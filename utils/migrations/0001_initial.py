# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-04-19 20:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('url_alias', models.CharField(max_length=150, null=True)),
                ('location_text', models.CharField(blank=True, max_length=100)),
                ('items_amount', models.IntegerField(default=0)),
                ('latitude', models.DecimalField(blank=True, decimal_places=15, max_digits=19, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=15, max_digits=19, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'Удалено'), (1, 'Активно')], default=1)),
            ],
        ),
    ]
