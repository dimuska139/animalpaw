# Фотографии
import uuid
from urllib.parse import urlparse

import os
import requests
import shutil
from django.apps import apps
from django.conf import settings
from django.core.files import File
from django.db.models.signals import post_save, pre_save
from imagekit.models import ImageSpecField
from model_utils import Choices

from django.db import models
from PIL import Image as PILimage
import hashlib
from pilkit.processors import ResizeToFill


class Image(models.Model):
    original = models.ImageField()  # Оригинал изображения
    preview = models.ImageField(null=True, blank=True)
    thumbnail = ImageSpecField(source='original',  # Автоматическая миниатюра
                               processors=[ResizeToFill(200, 200)],
                               format='JPEG',
                               options={'quality': 90})
    version = models.IntegerField(default=0, blank=True)
    # base64 = models.TextField(null=True)
    md5 = models.CharField(max_length=32, null=True, db_index=True)
    x_1 = models.IntegerField(null=True, blank=True)
    x_2 = models.IntegerField(null=True, blank=True)
    y_1 = models.IntegerField(null=True, blank=True)
    y_2 = models.IntegerField(null=True, blank=True)
    user = models.ForeignKey('extusers.ExtUser', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.original.name

    def make_logo(self, x1=0, y1=0, x2=100, y2=100):
        x1 = int(round(float(x1)))
        y1 = int(round(float(y1)))
        x2 = int(round(float(x2)))
        y2 = int(round(float(y2)))
        im = PILimage.open(settings.MEDIA_ROOT + '/' + self.original.name)
        im = im.crop((x1, y1, x2, y2))
        im = im.resize(getattr(settings, 'PREVIEW_SIZE', (200, 200)), PILimage.ANTIALIAS)

        preview_name = 'preview_' + os.path.splitext(self.original.name)[0] + '.png'
        im.save(settings.MEDIA_ROOT + '/' + preview_name)

        self.preview = preview_name
        self.x_1 = x1
        self.x_2 = x2
        self.y_1 = y1
        self.y_2 = y2
        self.save()

    def make_preview(self, maxsize=getattr(settings, 'PREVIEW_SIZE', (200, 200))):
        im = PILimage.open(settings.MEDIA_ROOT + '/' + self.original.name)
        im.thumbnail(maxsize)
        preview_name = 'preview_' + os.path.splitext(self.original.name)[0] + '.png'
        im.save(settings.MEDIA_ROOT + '/' + preview_name)
        self.preview = preview_name
        self.save()
        return self

    def calc_hash(self):
        img_path = settings.MEDIA_ROOT + '/' + self.original.name
        if os.path.isfile(img_path):
            self.md5 = hashlib.md5(open(img_path, 'rb').read()).hexdigest()
            self.save()
        else:
            self.delete()
        return self

    @staticmethod
    def from_url(url):
        link = urlparse(url)
        img_name = os.path.basename(link.path)  # Имя файла изображения из его URL
        filereq = requests.get(url, stream=True)
        img_path = settings.TEMP_DIR + img_name
        with open(img_path, "wb") as receive:
            shutil.copyfileobj(filereq.raw, receive)
        del filereq
        image = Image(
            version=1,
            user=None
        )
        image.original = File(open(img_path, 'rb'))
        image.save()
        os.remove(img_path)
        return image

    @staticmethod
    def remove_similar():
        images = Image.objects.filter(status=Image.STATUSES.active).all()
        for image in images:
            if not os.path.exists(settings.MEDIA_ROOT + '/' + image.original.name):
                image.delete()
            else:
                image.calc_hash()

        checked_images = []
        for image in images:
            checked_images.append(image.id)
            imgs = Image.objects.filter(status=Image.STATUSES.active).exclude(id__in=checked_images)
            for img in imgs:
                if img.md5 == image.md5 and img.original.name != image.original.name and os.path.exists(
                                        settings.MEDIA_ROOT + '/' + image.original.name):
                    os.remove(settings.MEDIA_ROOT + '/' + img.original.name)
                    print(img.original.name)
                    img.original = image.original
                    img.save()
                    checked_images.append(img.id)


def pre_save_image(sender, instance, **kwargs):
    if instance.pk is None:
        instance.original.name = str(uuid.uuid4()) + os.path.splitext(instance.original.name)[-1]


def post_save_image(sender, instance, **kwargs):
    post_save.disconnect(post_save_image, sender=sender)
    instance = instance.calc_hash()

    new_img_path = settings.MEDIA_ROOT + '/' + instance.original.name

    similar_image = Image.objects.filter(md5=instance.md5, status=Image.STATUSES.active).exclude(id=instance.id).first()

    if similar_image:
        similar_image_path = settings.MEDIA_ROOT + '/' + similar_image.original.name

        if os.path.isfile(similar_image_path):
            instance.original = similar_image.original
            instance.save()
        if similar_image_path != new_img_path:
            os.remove(new_img_path)

    post_save.connect(post_save_image, sender=sender)


pre_save.connect(pre_save_image, sender=Image)
post_save.connect(post_save_image, sender=Image)
