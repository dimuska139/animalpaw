from django.db import models
from model_utils import Choices


class Region(models.Model):
    name = models.CharField(max_length=100)
    url_alias = models.CharField(max_length=150, null=True)
    location_text = models.CharField(max_length=100, blank=True)
    items_amount = models.IntegerField(default=0)
    latitude = models.DecimalField(max_digits=19, decimal_places=15, null=True, blank=True)
    longitude = models.DecimalField(max_digits=19, decimal_places=15, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name
