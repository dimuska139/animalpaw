from django.contrib import admin
from .models import Region, Image


# Register your models here.
admin.site.register(
    (Region, Image))
