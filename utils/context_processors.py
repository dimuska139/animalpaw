from django.conf import settings
from django.contrib.sites.models import Site


def extra_context(request):
    current_site = Site.objects.first()
    user_id = 0
    if request.user.is_authenticated:
        user_id = request.user.id
    return {
        'base_url': settings.SCHEME + '://' + current_site.domain,
        'user_id': user_id
    }
