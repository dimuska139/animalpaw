import json

from django.contrib.contenttypes.models import ContentType
from django.http import JsonResponse
from django.http import HttpResponse
from channels import Channel, Group
from channels.auth import channel_session_user, channel_session_user_from_http
from django.template import loader
# Create your views here.
from jsonview.decorators import json_view

from utils.forms import get_errors, RatingForm
from utils.models.rating import Rating


def get_channel_name(content_type, object_id):
    return 'rating_' + str(content_type) + '_' + str(object_id)


def socket_push(content_type, object_id, data):
    ch_name = get_channel_name(content_type, object_id)
    Group(ch_name).send({
        'text': json.dumps(data)
    })


def ws_connect(message, content_type, object_id):
    ch_name = get_channel_name(content_type, object_id)
    message.reply_channel.send({'accept': True})
    Group(ch_name).add(message.reply_channel)


def ws_disconnect(message, content_type, object_id):
    ch_name = get_channel_name(content_type, object_id)
    Group(ch_name).discard(message.reply_channel)


def rate(request, content_type, object_id):
    ct = ContentType.objects.get(id=content_type)
    ct_class = ct.model_class()
    object = ct_class.objects.filter(id=object_id, status=ct_class.STATUSES.active).first()

    if not isinstance(object, ct_class):
        return JsonResponse({
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Not found'
                }
            ]
        })

    form = RatingForm(request.POST)
    if not form.is_valid():
        return JsonResponse({
            'success': False,
            'errors': get_errors(form)
        })
    response = JsonResponse({
        'success': True,
        'message': 'Спасибо, Ваш голос учтён!'
    })
    if request.user.is_authenticated():
        if object.rating.filter(user=request.user).exists():
            return JsonResponse({
                'success': False,
                'errors': [
                    {
                        'key': 'all',
                        'description': 'Вы уже голосовали'
                    }
                ]
            })
        object.rating.create(value=request.POST['value'], user=request.user)
    else:
        cookie_name = 'rating_' + str(content_type)
        if cookie_name in request.COOKIES:
            cookies = json.loads(request.COOKIES.get(cookie_name))
        else:
            cookies = []

        rating_ids = []
        cookie_list = []
        for cookie in cookies:
            rating_ids.append(int(cookie['id']))
            cookie_list.append(cookie)

        obj_ids = object.rating.filter(id__in=rating_ids).values_list('object_id', flat=True)

        if int(object_id) in obj_ids:
            return JsonResponse({
                'success': False,
                'errors': [
                    {
                        'key': 'all',
                        'description': 'Вы уже голосовали'
                    }
                ]
            })
        rate_object = object.rating.create(value=request.POST['value'])

        cookie_list.append({
            'id': int(rate_object.id),
            'content_type': content_type,
            'object_id': object_id
        })
        response.set_cookie(cookie_name, json.dumps(cookie_list))

    data = {
        'score': object.score(),
        'reviews_num': object.reviews_num()
    }

    socket_push(content_type, object_id, data)

    return response
