from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class MainTest(TestCase):
    fixtures = ['users.json']

    def setUp(self):
        self.url = reverse('main')

    def test_anon(self):
        response = self.client.get(self.url)
        self.assertNotContains(response, 'id="djDebugToolbar"', status_code=200)

    def test_not_admin(self):
        user = ExtUser.objects.get(pk=1)
        user.is_superuser = False
        user.save()
        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)
        self.assertNotContains(response, 'id="djDebugToolbar"', status_code=200)

    def test_admin(self):
        user = ExtUser.objects.get(pk=1)
        user.is_superuser = True
        user.save()
        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)
        self.assertContains(response, 'id="djDebugToolbar"', status_code=200)







