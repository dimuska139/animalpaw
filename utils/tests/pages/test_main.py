from django.contrib.contenttypes.models import ContentType
from django.http import SimpleCookie
from django.test import TestCase
from django.urls import reverse
import json

from articles.models import Article
from extusers.models import ExtUser
from utils.models.rating import Rating


class MainTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def setUp(self):
        self.url = reverse('main')

    def test_anon(self):
        response = self.client.get(self.url)
        self.assertContains(response, 'Потерялась собака? Хотите отдать животное в добрые руки?', status_code=200)
        self.assertContains(response, 'Забыли пароль?', status_code=200)

    def test_auth(self):
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)
        self.assertContains(response, 'Потерялась собака? Хотите отдать животное в добрые руки?', status_code=200)
        self.assertNotContains(response, 'Забыли пароль?', status_code=200)

    def test_move_rating(self):
        ct_id = ContentType.objects.get_for_model(Article).id
        article_1 = Article.objects.get(pk=1)
        article_2 = Article.objects.get(pk=2)

        cookies = []
        rate_ids = []

        r = article_1.rating.create(value=4)
        cookies.append({
            'id': r.id,
            'content_type': ct_id,
            'object_id': r.object_id
        })
        rate_ids.append(r.id)

        r = article_2.rating.create(value=1)
        cookies.append({
            'id': r.id,
            'content_type': ct_id,
            'object_id': r.object_id
        })
        rate_ids.append(r.id)

        rates = Rating.objects.filter(id__in=rate_ids).all()
        for rate in rates:
            self.assertIsNone(rate.user)

        ct_id = ContentType.objects.get_for_model(Article).id
        cookie_name = 'rating_' + str(ct_id)
        self.client.cookies = SimpleCookie({cookie_name: json.dumps(cookies)})

        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')

        response = self.client.get(self.url)

        rates = Rating.objects.filter(id__in=rate_ids).all()
        for rate in rates:
            self.assertEqual(rate.user, user)
