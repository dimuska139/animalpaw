# -*- coding: utf-8 -*-

from django.test import TestCase
from utils.models import Region


class RegionTest(TestCase):
    def test_creation(self):
        region = Region.objects.create(name='Регион', url_alias='reg', location_text='рег', latitude=30, longitude=30)
        self.assertTrue(isinstance(region, Region))
        self.assertEqual(region.__str__(), region.name)
        self.assertEqual(region.status, Region.STATUSES.active)
        self.assertEqual(region.items_amount, 0)
