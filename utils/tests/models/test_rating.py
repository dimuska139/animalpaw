# -*- coding: utf-8 -*-

from django.test import TestCase

from extusers.models import ExtUser
from utils.models import Rating
from articles.models import Article


class RatingTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_creation(self):
        user = ExtUser.objects.first()

        article = Article.objects.first()
        rating = article.rating.create(value=3, user=user)

        self.assertTrue(isinstance(rating, Rating))
        self.assertEqual(rating.__str__(), str(rating.value))
        self.assertEqual(rating.status, Rating.STATUSES.active)
