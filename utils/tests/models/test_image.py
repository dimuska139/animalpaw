# -*- coding: utf-8 -*-
import hashlib
import os
from django.conf import settings
from django.core.files import File
from django.test import TestCase

from utils.models import Image
from extusers.models import ExtUser


class ImageTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation_from_url(self):
        media_before = os.listdir(settings.MEDIA_ROOT)
        images_amount_before = Image.objects.count()
        img_url = 'https://www.google.ru/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
        image = Image.from_url(img_url)
        media_after = os.listdir(settings.MEDIA_ROOT)
        images_amount_after = Image.objects.count()

        self.assertEqual(1, images_amount_after - images_amount_before)
        self.assertEqual(1, len(media_after) - len(media_before))

        self.assertTrue(isinstance(image, Image))
        self.assertEqual(image.__str__(), image.original.name)
        self.assertEqual(image.status, Image.STATUSES.active)
        self.assertTrue(image.original.name in media_after)

    def test_creation(self):
        image = Image(
            version=1,
            x_1=0,
            y_1=0,
            x_2=30,
            y_2=30,
            user=ExtUser.objects.first()
        )
        image.original = File(open('./test_files/correct_img.jpg', 'rb'))
        image.preview = File(open('./test_files/correct_img.jpg', 'rb'))
        image.save()

        self.assertTrue(isinstance(image, Image))
        self.assertEqual(image.__str__(), image.original.name)
        self.assertEqual(image.status, Image.STATUSES.active)

    def test_updating(self):
        image = Image(
            version=1,
            x_1=0,
            y_1=0,
            x_2=30,
            y_2=30,
            user=ExtUser.objects.first()
        )
        image.original = File(open('./test_files/correct_img.jpg', 'rb'))
        image.preview = File(open('./test_files/correct_img.jpg', 'rb'))
        image.save()

        updated_image = Image.objects.get(id=image.id)
        self.assertEqual(image.original.name, updated_image.original.name)
        self.assertEqual(image.status, Image.STATUSES.active)

    def test_similarity(self):
        media_before = os.listdir(settings.MEDIA_ROOT)
        images_amount_before = Image.objects.count()
        image_1 = Image(
            version=1,
            x_1=0,
            y_1=0,
            x_2=30,
            y_2=30,
            user=ExtUser.objects.first()
        )
        image_1.original = File(open('./test_files/correct_img.jpg', 'rb'))
        image_1.save()

        first_image = Image.objects.get(id=image_1.id)

        image_2 = Image(
            version=1,
            x_1=0,
            y_1=0,
            x_2=30,
            y_2=30,
            user=ExtUser.objects.first()
        )
        image_2.original = File(open('./test_files/correct_img.jpg', 'rb'))
        image_2.save()

        second_image = Image.objects.get(id=image_2.id)

        self.assertEqual(first_image.original, second_image.original)
        media_after = os.listdir(settings.MEDIA_ROOT)
        images_amount_after = Image.objects.count()

        self.assertEqual(2, images_amount_after - images_amount_before)
        self.assertEqual(1, len(media_after) - len(media_before))

        media_before = os.listdir(settings.MEDIA_ROOT)
        image_3 = Image(
            version=1,
            x_1=0,
            y_1=0,
            x_2=30,
            y_2=30,
            user=ExtUser.objects.first()
        )
        image_3.original = File(open('./test_files/correct_img.jpg', 'rb'))
        image_3.save()

        third_image = Image.objects.get(id=image_3.id)

        self.assertEqual(second_image.original, third_image.original)

        media_after = os.listdir(settings.MEDIA_ROOT)

        self.assertEqual(0, len(media_after) - len(media_before))

        self.assertTrue(os.path.exists(settings.MEDIA_ROOT + '/' + image_1.original.name))

    def test_calc_hash(self):
        image = Image.objects.first()
        image.md5 = None
        image.save()

        image.calc_hash()
        img_path = settings.MEDIA_ROOT + '/' + image.original.name
        self.assertEqual(hashlib.md5(open(img_path, 'rb').read()).hexdigest(), image.md5)

        new_path = img_path + '_'
        os.rename(img_path, new_path)
        image.calc_hash()

        self.assertFalse(Image.objects.filter(id=image.id).exists())

        os.rename(new_path, img_path)

