# coding: utf-8
from django.test import TestCase
from comments.forms import CommentForm
from utils.forms import RatingForm


class CommentTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_valid_form(self):
        data = {
            'value': 3,
        }
        form = RatingForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'value': 7,
        }
        form = RatingForm(data=data)
        self.assertFalse(form.is_valid())
