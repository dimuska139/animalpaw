# -*- coding: utf-8 -*-

from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('utils.rate', args=[1, 1]), '/utils/1/1/rate')
