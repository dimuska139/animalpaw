# -*- coding: utf-8 -*-
import json

from django.contrib.contenttypes.models import ContentType
from django.http import SimpleCookie
from django.test import TestCase
from django.urls import reverse

from articles.models import Article
from extusers.models import ExtUser


class RatingCreationTest(TestCase):
    fixtures = ['users.json', 'initial.json', 'articles.json']

    def test_action(self):
        ct_id = ContentType.objects.get_for_model(Article).id
        article = Article.objects.first()
        url = reverse('utils.rate', args=[
            ct_id,
            article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_save(self):
        user = ExtUser.objects.get(id=1)

        article = Article.objects.first()
        ct_id = ContentType.objects.get_for_model(Article).id

        self.client.login(username=user.email, password='12345')
        url = reverse('utils.rate', args=[
            ct_id,
            article.id
        ])
        rates_before = article.rating.count()
        response = self.client.post(url, {
            'value': 2,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        rates_after = Article.objects.get(id=article.id).rating.count()

        self.assertEqual(1, rates_after - rates_before)

    def test_double_rating(self):
        user = ExtUser.objects.get(id=1)

        article = Article.objects.first()
        ct_id = ContentType.objects.get_for_model(Article).id

        self.client.login(username=user.email, password='12345')
        url = reverse('utils.rate', args=[
            ct_id,
            article.id
        ])

        rates_before = article.rating.count()
        response = self.client.post(url, {
            'value': 2,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        rates_after = Article.objects.get(id=article.id).rating.count()
        self.assertEqual(1, rates_after - rates_before)

        response = self.client.post(url, {
            'value': 2,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        rates_after = Article.objects.get(id=article.id).rating.count()
        self.assertEqual(1, rates_after - rates_before)

    def test_double_rating_cookie(self):
        article = Article.objects.first()
        ct_id = ContentType.objects.get_for_model(Article).id

        url = reverse('utils.rate', args=[
            ct_id,
            article.id
        ])

        rates_before = article.rating.count()
        r = article.rating.create(value=3)

        # Повторная попытка проголосовать
        cookie_name = 'rating_' + str(ct_id)
        self.client.cookies = SimpleCookie({cookie_name: json.dumps([
            {
                'id': r.id,
                'content_type': ct_id,
                'object_id': article.id
            }
        ])})
        response = self.client.post(url, {
            'value': 2,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        rates_after = Article.objects.get(id=article.id).rating.count()
        self.assertEqual(1, rates_after - rates_before)

    def test_not_exists(self):
        article = Article.objects.last()
        ct_id = ContentType.objects.get_for_model(Article).id

        url = reverse('utils.rate', args=[
            ct_id,
            article.id + 1  # Не существующий id
        ])

        response = self.client.post(url, {
            'value': 2,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
