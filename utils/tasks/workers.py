from celery.task import task
from django.db import connection


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
# Удаление OFFLINE воркеров djcelery
def clean():
    cursor = connection.cursor()
    cursor.execute("DELETE FROM public.djcelery_workerstate WHERE last_heartbeat < now() - interval '1 days' AND id NOT IN (SELECT worker_id FROM djcelery_taskstate)")
