from django.core.management import call_command
from celery.task import task


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def make_backup():
    call_command('dbbackup', clean=True, compress=True, interactive=False)
    call_command('mediabackup')
