from django.forms import ModelForm

from utils.models.rating import Rating


def get_errors(form):
    errors = []
    if not form.is_valid():
        for k, v in form._errors.items():
            text = {
                'description': ', '.join(v),
            }
            if k == '__all__':
                text['key'] = 'all'
            else:
                text['key'] = '#id_%s' % k
            errors.append(text)
    return errors


class RatingForm(ModelForm):
    class Meta:
        model = Rating
        exclude = (
            'status',
            'user',
            'content_type',
            'object_id',
            'content_object'
        )

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['value'].label = 'Рейтинг'
