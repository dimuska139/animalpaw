CKEDITOR.dialog.add(
        "simple-image-browser-dialog",
        function() {
            return {
                title: "Simple Image Browser",
                minWidth: 800,
                minHeight: 400,
                maxWidth: 800,
                maxHeight: 400,
                contents: [{
                    id: "tab-step1",
                    label: "Browse for images",
                    elements: [{
                        type: "html",
                        align: "left",
                        id: "titleid",
                        style: "font-size: 20px; font-weight: bold;",
                        html: "Browse for pictures"
                    }, {
                        type: "html",
                        align: "left",
                        id: "msg",
                        style: "",
                        html: '<div id="imageBrowser"></div>'
                    }]
                }]
            }
        }),
    CKEDITOR.plugins.add("simple-image-browser", {
        init: function(a) {
            "undefined" == typeof CKEDITOR.config.simpleImageBrowserListType && (CKEDITOR.config.simpleImageBrowserListType = "thumbnails"),
                a.on("dialogShow", function(a) {
                    var b;
                    b = a.data,
                        "simple-image-browser-dialog" === b.getName() && $.get({
                            url: CKEDITOR.config.simpleImageBrowserURL,
                            success: function(urls_list) {
                                return html = "", $.each(urls_list, function(ind, url) {
                                    html = "thumbnails" === CKEDITOR.config.simpleImageBrowserListType ? html + "<div onclick=\"CKEDITOR.tools.simpleimagebrowserinsertpicture('" + url + "');\" style=\"position:relative;width:75px;height:75px;margin:5px;background-image:url('" + url + "');background-repeat:no-repeat;background-size:125%;background-position:center center;float:left;\"></div>" : "link"
                                }), $("#imageBrowser").html(html)
                            },
                            dataType: 'json'
                        })
                }), a.addCommand("simple-image-browser-start", new CKEDITOR.dialogCommand("simple-image-browser-dialog")), CKEDITOR.tools.simpleimagebrowserinsertpicture = function(b) {
                    var c, d;
                    a = CKEDITOR.currentInstance, c = CKEDITOR.dialog.getCurrent(), d = '<img src="' + b + '" data-cke-saved-src="' + b + '" alt="' + b + '" />', a.config.allowedContent = !0, a.insertHtml(d.trim()), c.hide()
                }, a.ui.addButton("Simple Image Browser", {
                    label: "Simple Image Browser ",
                    command: "simple-image-browser-start",
                    icon: this.path + "images/icon.png"
                })
        }
    });