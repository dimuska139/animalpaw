/**
 * Created by dimuska139 on 25.04.17.
 */


var cropper = false;


function initCrop() {
    var image = $('.image-place img').get(0);
    if (typeof(image) == 'undefined')
        return;
    if (cropper)
        cropper.destroy();
    cropper = new Cropper(image, {
        movable: false,
        zoomable: false,
        rotatable: false,
        scalable: false,
        aspectRatio: 1,
        guides: false,
        cropend: function () {
            var cropper = this.cropper;
            var cropData = cropper.getData();
            var image = {};
            image.x_1 = cropData.x;
            image.y_1 = cropData.y;
            image.x_2 = cropData.x + cropData.width;
            image.y_2 = cropData.y + cropData.height;

            if (cropData.x < 0) {
                image.x_2 += cropData.x;
                image.x_1 = 0;
            }

            if (cropData.y < 0) {
                image.y_2 += cropData.y;
                image.y_1 = 0;
            }
            $('#logo_x1').val(image.x_1);
            $('#logo_y1').val(image.y_1);
            $('#logo_x2').val(image.x_2);
            $('#logo_y2').val(image.y_2);
        },
        ready: function () {
            if ($('#logo_x1').val().length != 0) {
                cropper.setData({
                    'x': parseInt($('#logo_x1').val()),
                    'y': parseInt($('#logo_y1').val()),
                    'width': parseInt($('#logo_x2').val()) - parseInt($('#logo_x1').val()),
                    'height': parseInt($('#logo_y2').val()) - parseInt($('#logo_y1').val()),
                    'rotate': 0,
                    'scaleX': 1,
                    'scaleY': 1
                });
            }

            var cropData = cropper.getData();
            $('#logo_x1').val(cropData.x);
            $('#logo_y1').val(cropData.y);
            $('#logo_x2').val(cropData.x + cropData.width);
            $('#logo_y2').val(cropData.y + cropData.height);
        }
    });
}

function initUploader() {
    $('.logo-upload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        drop: function (e, data) {
        },
        start: function(e) {
            $('.crop-widget .btn-upload').hide();
            $('.crop-widget .image-place').hide();
            $('.crop-widget .error').hide();
            $('.crop-widget .progress').show();
        },
        done: function (e, data) {
            $('.crop-widget .progress').hide();
            $('.crop-widget .btn-upload').show();

            var data = data.result;
            if (!data.success) {
                $('.crop-widget .image-place').show();
                $('.crop-widget .error').html(data.message).show();
                setTimeout(function() {
                    $('.crop-widget .error').slideUp();
                }, 5000);
                return;
            }
            $('#logo_id').val(data.id);
            $('.crop-widget .image-place').html('<img class="img-thumbnail" src="' + data.url + '"/>').show();

            initCrop();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.crop-widget .progress').val(progress);
        }
    });
}

function init() {
    initCrop();
    initUploader();
}


$(document).ready(function() {
    $('#breed-master-form').bind('error', function() {
        init();
    });
    init();
});