/**
 * Created by dimuska139 on 18.04.17.
 */
$(document).ready(function() {
    $('#whatdog-form').bind('success', function(ev, data) {
        $(this).hide();
        $('#whatdog-results .result').html(data.html);
        $('#whatdog-results').slideDown();
    });

    $('#whatdog-repeat').on('click', function(){
        $('#whatdog-results').hide();
        $('#whatdog-results .result').html('');
        //$('#whatdog-form select').val(0);
        $('#whatdog-form').slideDown();
    });
});