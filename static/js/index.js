/**
 * Created by dimuska139 on 22.01.17.
 */
function init() {
    $('#feedback-form').bind('success', function() {
        $(this).find('input,textarea').val('');
    });
}

$(document).ready(function() {
    init();
});