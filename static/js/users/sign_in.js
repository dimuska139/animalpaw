/*var checkEmailtimerId = false;
var checkEmailTimeout = 1000;
var passwordsSuccess = false;*/

$(document).ready(function() {
    var link = window.location.hash.replace("#","");
  //  console.dir(window.location.hash);

    /*function activeSubmitButton(form) {
        var fieldsAmount = $(form).find('.form-group').length;
        var successFieldsAmount = $(form).find('.form-group.has-success').length;
        if (fieldsAmount==successFieldsAmount) {
            $(form).find('.submit-button').prop('disabled', false);
        } else {
            $(form).find('.submit-button').prop('disabled', false);
        }
    }

    function checkTextField(field) {
        if ($(field).val().length != 0) {
            $(field).parent('.form-group').removeClass('has-danger').addClass('has-success');
        } else {
            $(field).parent('.form-group').removeClass('has-success');
        }
    }

    function checkEmailField(field) {
        var r = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;
        if (!r.test($(field).val())) {
            $(field).parent('.form-group').removeClass('has-success');
            if ($(field).val().length > 0)
                $(field).parent('.form-group').addClass('has-danger');
        } else {
            $(field).parent('.form-group').removeClass('has-danger').addClass('has-success');
        }
    }

    $('.login-form .username').on('change keyup input click', function (event) {
        checkEmailField($(this));
        activeSubmitButton($('.login-form'));
    });

    $('.login-form .password').on('change keyup input click', function (event) {
        checkTextField($(this));
        activeSubmitButton($('.login-form'));
    });*/

    /*$('.btn-social').on('click', function(e) {
        var url = $(this).data('href');
        location.href = url
    });

    $('.reset-password-form .email').on('change keyup input click', function (event) {
        var r = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;
        if (!r.test($(this).val())) {
            $('.email').parent('.form-line').find('.indicator').hide();
            $('.reset-password-button').addClass('disabled');
        } else {
            $('.email').parent('.form-line').find('.indicator').show();
            $('.reset-password-button').removeClass('disabled');
        }
    });

    $('.login-form .remember-password-button').on('click', function(){
        if ($('.reset-password-form').is(':visible'))
            $('.reset-password-form').hide();
        else {
            $('.reset-password-form').show();
            location.href='#reset-password-form';
        }
    });

    $('.reset-password-form').on('click', '.reset-password-button:not(.disabled)', function () {
        var params = {}
        params.email = $('.reset-password-form .email').val();
        var me = this;
        $('.reset-password-form').find('.form-line .loading-indicator').fadeIn();
        $('.reset-password-form .error').html('').hide();
        $.ajax({
            type: "post",
            url: "/user/reset_password/",
            dataType: "json",
            data: params,
            complete: function () {
                $('.reset-password-form').find('.form-line .loading-indicator').fadeOut();
            },
            success: function (res) {
                if (res.success) {
                    $('.reset-password-form .initial').hide();
                    $('.reset-password-form .initial .email').val('');
                    $('.reset-password-form .success').show();
                } else {
                    $('.reset-password-form .error').html(res.message).show();
                }
            }
        });
    });

    function activeLoginButt() {
        var fieldsAmount = $('.login-form .form-line .indicator').length;
        var successFieldsAmount = $('.login-form .form-line .indicator:visible').length;
        if (fieldsAmount==successFieldsAmount) {
            $('.login-form').find('.login-button').removeClass('disabled');
        } else {
            $('.login-form').find('.login-button').addClass('disabled');
        }
    }

    $('#id_username').on('change keyup input click', function(event) {
        var r = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;

        if (!r.test($(this).val())) {
            $(this).parent(".form-line").find(".indicator").hide();
        } else {
            $(this).parent(".form-line").find(".indicator").show();
        }
        activeLoginButt();
    });

    $('#id_password').on('change keyup input click', function(event) {
        if ($(this).val().length != 0) {
            $(this).parent(".form-line").find(".indicator").show();
        } else {
            $(this).parent(".form-line").find(".indicator").hide();
        }
        activeLoginButt();
    });

    $('form').on('click', '.submit-button:not(.disabled)', function() {
        $(this).submit();
    });


    // Активировать или деакт ивировать кнопку "Зарегистрироваться"
    function activeRegistrationButt(){
        var fieldsAmount = $('.registration-form .form-line .indicator').length;
        var successFieldsAmount = $('.registration-form .form-line .indicator:visible').length;
        if (fieldsAmount==successFieldsAmount && passwordsSuccess) {
            $('.registration-form').find(".registration-button").removeClass("disabled");
        } else {
            $('registration-form').find(".registration-button").addClass("disabled");
        }
    }

    $('#id_email').on('change keyup input click', function(event) {
        var r = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;

        if (!r.test($(this).val())) {
            $(this).parent(".form-line").find(".indicator").hide();
        } else {
            if (checkEmailtimerId != false)
                clearTimeout(checkEmailtimerId);
            checkEmailtimerId = setTimeout( checkEmail, checkEmailTimeout);
        }
    });

    function checkEmail() {
        $('#id_email').parent(".form-line").find(".loading-indicator").fadeIn();
        var params = {
            "email": $('#id_email').val()
        }
        clearTimeout(checkEmailtimerId);

        $.ajax({
            type: "post",
            url: check_email_url,
            dataType: "json",
            data: params,
            complete: function(){
                $('#id_email').parent(".form-line").find(".loading-indicator").fadeOut();
            },
            success: function(res){
                if (res.success) {
                    $('#id_email').parent(".form-line").find(".indicator").show();
                    $('#id_email').parent(".form-line").find(".field-error").html("");
                } else {
                    $('#id_email').parent(".form-line").find(".indicator").hide();
                    $('#id_email').parent(".form-line").find(".field-error").html(res.message);
                }
                activeRegistrationButt();
            }
        });
    }

    // Проверка паролей в форме регистрации
    function checkPasswords() {
        var p1_success = false;
        var p2_success = false;

        if ($('#id_password1').val().length >= MIN_PASSWORD_LENGTH) {
            p1_success = true;
            $('#id_password1').parent(".form-line").find(".indicator").show();
        }
        else {
            $('#id_password1').parent(".form-line").find(".indicator").hide();
        }

        if ($('#id_password2').val().length >= MIN_PASSWORD_LENGTH) {
            p2_success = true;
            $('#id_password2').parent(".form-line").find(".indicator").show();
        }
        else {
            $('#id_password2').parent(".form-line").find(".indicator").hide();
        }

        if (p1_success && p2_success)
        {
            if ($('#id_password1').val() == $('#id_password2').val()) {
                $('.passwords-error').html('').hide();
                passwordsSuccess = true;
            } else {
                $('.passwords-error').html('Пароли не совпадают').show();
                passwordsSuccess = false;
            }
        } else {
            passwordsSuccess = false;
        }
    }

    $('#id_password1,#id_password2').on('change keyup input click', function(event) {
        checkPasswords();
        activeRegistrationButt();
    });


    $('.registration-form').on('click', '.submit-button:not(.disabled)', function(){
        $('.registration-form').submit();
    });


    $('.login-form input').trigger('click');*/
});