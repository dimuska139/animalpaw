/**
 * Created by dimuska139 on 02.09.16.
 */
$(document).ready(function() {
    $('#id_password1,#id_password2').trigger('click');

    // Активировать или деактивировать кнопку "Зарегистрироваться"
    function activeRegistrationButt(){
        var fieldsAmount = $('.form-line .indicator').length;
        var successFieldsAmount = $('.form-line .indicator:visible').length;
        if (fieldsAmount==successFieldsAmount) {
            $('form').find(".submit-button").removeClass("disabled");
        } else {
            $('form').find(".submit-button").addClass("disabled");
        }
    }

    function checkPasswords() {
        if ($('#id_password1').val().length<MIN_PASSWORD_LENGTH || $('#id_password2').val().length<MIN_PASSWORD_LENGTH) {
            $('#id_password1').parent(".form-line").find(".indicator").hide();
            $('#id_password2').parent(".form-line").find(".indicator").hide();
            return;
        }

    /*    if ($('#id_password1').val().length>=MIN_PASSWORD_LENGTH && $('#id_password2').val().length == 0)
            $('#id_password1').parent(".form-line").find(".indicator").show();

        if ($('#id_password2').val().length>=MIN_PASSWORD_LENGTH && $('#id_password1').val().length == 0)
            $('#id_password2').parent(".form-line").find(".indicator").show();*/

        if ($('#id_password1').val().length>=MIN_PASSWORD_LENGTH && $('#id_password2').val().length>=MIN_PASSWORD_LENGTH) {
            if ($('#id_password1').val() == $('#id_password2').val()) {
                $('#id_password1').parent(".form-line").find(".indicator").show();
                $('#id_password2').parent(".form-line").find(".indicator").show();
            } else {
                $('#id_password1').parent(".form-line").find(".indicator").hide();
                $('#id_password2').parent(".form-line").find(".indicator").hide();
            }
        }
    }

    $('#id_password1,#id_password2').on('change keyup input click', function(event) {
        checkPasswords();
        activeRegistrationButt();
    });

    $('form').on('click', '.submit-button:not(.disabled)', function(){
        $('.new-password-form').submit();
    });
});