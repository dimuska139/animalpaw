/**
 * Created by dimuska139 on 04.10.16.
 */
var cropper = false;


function initCrop() {
    var image = $('.image-place img').get(0);
    if (typeof(image) == 'undefined')
        return;
    if (cropper)
        cropper.destroy();
    cropper = new Cropper(image, {
        movable: false,
        zoomable: false,
        rotatable: false,
        scalable: false,
        aspectRatio: 1,
        guides: false,
        cropend: function () {
            var cropper = this.cropper;
            var cropData = cropper.getData();
            var avatar = {};
            avatar.x_1 = cropData.x;
            avatar.y_1 = cropData.y;
            avatar.x_2 = cropData.x + cropData.width;
            avatar.y_2 = cropData.y + cropData.height;

            if (cropData.x < 0) {
                avatar.x_2 += cropData.x;
                avatar.x_1 = 0;
            }

            if (cropData.y < 0) {
                avatar.y_2 += cropData.y;
                avatar.y_1 = 0;
            }
            $('#avatar_x1').val(avatar.x_1);
            $('#avatar_y1').val(avatar.y_1);
            $('#avatar_x2').val(avatar.x_2);
            $('#avatar_y2').val(avatar.y_2);
        },
        ready: function () {
            if ($('#avatar_x1').val().length != 0) {
                cropper.setData({
                    'x': parseInt($('#avatar_x1').val()),
                    'y': parseInt($('#avatar_y1').val()),
                    'width': parseInt($('#avatar_x2').val()) - parseInt($('#avatar_x1').val()),
                    'height': parseInt($('#avatar_y2').val()) - parseInt($('#avatar_y1').val()),
                    'rotate': 0,
                    'scaleX': 1,
                    'scaleY': 1
                });
            }

            var cropData = cropper.getData();
            $('#avatar_x1').val(cropData.x);
            $('#avatar_y1').val(cropData.y);
            $('#avatar_x2').val(cropData.x + cropData.width);
            $('#avatar_y2').val(cropData.y + cropData.height);
        }
    });
}

function initUploader() {
    $('.avatar-upload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        drop: function (e, data) {
        },
        start: function(e) {
            $('.crop-widget .btn-upload').hide();
            $('.crop-widget .image-place').hide();
            $('.crop-widget .error').hide();
            $('.crop-widget .progress').show();
        },
        done: function (e, data) {
            $('.crop-widget .progress').hide();
            $('.crop-widget .btn-upload').show();

            $(this).parent('.upload-button').parent('.image-upload-crop-widget').parent('.form-line').find('.indicator').show();
            $(this).parent('.upload-button').parent('.image-upload-crop-widget').find('.upload-progress-step').hide();

            var data = data.result;
            if (!data.success) {
                $('.crop-widget .image-place').show();
                $('.crop-widget .error').html(data.message).show();
                var me = this;
                setTimeout(function() {
                    $('.crop-widget .error').slideUp();
                }, 5000);
                return;
            }
            $('#avatar_id').val(data.id);
            $('.crop-widget .image-place').html('<img class="img-thumbnail" src="' + data.url + '"/>').show();

            initCrop();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.crop-widget .progress').val(progress);
        }
    });
}

function init() {
    initCrop();
    initUploader();
    $('#profile-form').bind('error', function() {
        initCrop();
        initUploader();
    });

    $('#password-form').bind('complete', function() {
        $(this).find('input[type="password"]').val('');
    });
}

$(document).ready(function(){
    $('input[type="phone"]').inputmask('mask', {
        'mask': '+7 (999) 999-9999'
    });
    init();
});