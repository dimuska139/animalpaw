/**
 * Created by dimuska139 on 31.03.17.
 */
$(document).ready(function() {
    $('form').on('submit', function(e) {
        e.preventDefault();
        $(this).find('.loading-indicator').fadeIn();
        $(this).find('.non-field-errors').hide().html('');

        $(this).find('.form-group').removeClass('has-danger');
        $(this).find('.form-group .form-control-feedback').html('');

        var me = this;
        var params = $(this).serialize();

        $.ajax({
            type: 'post',
            url: $(me).attr('action'),
            dataType: 'json',
            data: params,
            complete: function(xhr){
                $(me).find('.loading-indicator').fadeOut();
                if (xhr.status != 200) {
                    $(me).find('.non-field-errors').html('Непредвиденная ошибка').show();
                    $(me).trigger('error');
                } else
                    $(me).trigger('complete');
            },
            success: function(res){
                if (res.url) {
                    window.location = res.url;
                    return;
                }

                if (res.success) {
                    if (res.message) {
                        $(me).find('.success').html(res.message).show();
                        $(me).trigger('success', res);
                        setTimeout(function () {
                            $(me).find('.success').fadeOut();
                        }, 3000);
                    } else {
                        $(me).trigger('success', res);
                    }

                } else {
                    if (!res.errors)
                        $(me).html(res.html);
                    else {
                        for (var i=0;i<res.errors.length;i++) {
                            $(me).find(res.errors[i].key).parents('.form-group').addClass('has-danger').find('.form-control-feedback').html(res.errors[i].description);
                            if (res.errors[i].key == 'all') {
                                $(me).find('.non-field-errors').html(res.errors[i].description).show();
                            }
                        }

                        if (res.errors.length > 0) {
                        //    location.hash = res.errors[0].key;
                        }
                    }
                    $(me).trigger('error');
                }
                $(me).trigger('after');
            }
        });
    });
});