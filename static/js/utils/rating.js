/**
 * Created by dimuska139 on 24.05.17.
 */

function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true;
  }

  return Object.keys(obj);
}

(function( $ ){
    var settings = {
        'socket': false,
        'content_type': 0,
        'object_id': 0,
        'rating': 0,
        'is_disabled': false,
        'set_rating_lock': false
    };

    var options = {};

    var methods = {
        disable:function () {
            settings.is_disabled = true;
            $(this).find('.rate').addClass('disabled');
        },
        setValue:function (rating) {
            var me = this;
            $(me).find('.rate').removeClass('fa-star').removeClass('fa-star-o').removeClass('fa-star-half-empty');
            $(me).find('.rate').each(function(index) {
                if (rating >= parseInt($(this).data('value'))) {
                    $(this).addClass('fa-star');
                }
                else if (rating < parseInt($(this).data('value'))) {
                    $(this).addClass('fa-star-o');
                }
            });
        },
        setRating:function (rating) {
            if (settings.set_rating_lock)
                return;
            var me = this;
            $(me).find('.rate').each(function(index) {
                if (rating < parseInt($(this).data('value')) && rating > parseInt($(this).data('value')) - 1) {
                    $(this).removeClass('fa-star').removeClass('fa-star-o').removeClass('fa-star-half-empty');
                    $(this).addClass('fa-star-half-empty');
                }
                else if (rating >= parseInt($(this).data('value'))) {
                    $(this).removeClass('fa-star').removeClass('fa-star-o').removeClass('fa-star-half-empty');
                    $(this).addClass('fa-star');
                }
                else if (rating < parseInt($(this).data('value'))) {
                    $(this).removeClass('fa-star').removeClass('fa-star-o').removeClass('fa-star-half-empty');
                    $(this).addClass('fa-star-o');
                }
            });
        },

        init:function(params) {
            var me = this;

            settings = $.extend(settings, params);
            settings.url = $(this).data('url');
            settings.rating = $(this).data('value');

            methods['setRating'].apply(me, [settings.rating]);

            if (settings.is_disabled == true) {
                methods['disable'].apply(me);
                return;
            }



            var protocol = 'ws:';
            if ('https:' == document.location.protocol)
                protocol = 'wss:';

            settings.socket = new WebSocket(protocol + '//' + window.location.host + '/ws/rating/' + settings.content_type + '/' + settings.object_id + '/');

            settings.socket.onopen = function() {
                console.dir("Соединение установлено");
            };

            settings.socket.onclose = function (event) {
                if (event.wasClean) {
                    console.dir('Соединение закрыто');
                } else {
                    console.dir('Обрыв соединения');
                }
            };

            settings.socket.onerror = function (error) {
                console.dir("Ошибка " + error.message);
            };


            settings.socket.onmessage = function (message) {
                var data = JSON.parse(message.data);
                methods['setRating'].apply(me, [data.score.toFixed(2)]);
                settings.rating = data.score.toFixed(2);
                $(me).find('.score').html(data.score.toFixed(2));
                $(me).find('.reviews_num').html(data.reviews_num);
            };

            $(me).on('click', '.rate', function(event){
                if (settings.is_disabled)
                    return;

                $(me).find('.message').hide();
                $(me).find('.rate-value').hide();
                $(me).find('.loading').show();

                var data = {};
                data.value = $(this).data('value');
                $.ajax({
                    type: 'post',
                    url: settings.url,
                    dataType: 'json',
                    data: data,
                    complete: function (xhr) {
                        $(me).find('.loading').hide();
                        $(me).find('.rate-value').show();
                        if (xhr.status != 200) {
                            $(me).find('.message.error-text').html('Непредвиденная ошибка').show();
                        }
                    },
                    success: function (res) {
                        if (res.success) {
                            $(me).find('.message.success-text').html(res.message);
                            $(me).find('.message.success-text').show();
                            setTimeout(function () {
                                $(me).find('.message.success-text').fadeOut();
                            }, 3000);
                        } else {
                            $(me).find('.message.error-text').html(res.message);
                            $(me).find('.message.error-text').show();
                            methods['setRating'].apply(me, [settings.rating]);
                        }
                        methods['disable'].apply(me);
                    }
                });
            });

            $(me).on('mouseenter', function(event) {
                settings.set_rating_lock = true;
            });

            $(me).on('mouseleave', function(event) {
                settings.set_rating_lock = false;
                methods['setRating'].apply(me, [settings.rating]);
            });

            $(me).on('mouseenter', '.rate', function(event){
                if (settings.is_disabled)
                    return;
                var rate = $(this).data('value');
                methods['setValue'].apply(me, [rate]);
            });

            $(me).on('mouseleave', '.rate', function(event){
                if (settings.is_disabled)
                    return;
                var rate = settings.rating;
                methods['setRating'].apply(me, [rate]);
            });
        },
        setParam:function(key, value) {
            var newOption = {};
            newOption[key] = value;
            options = $.extend(options, newOption);
        },
        setParams:function(object) {
            options = $.extend(options, object);
        }
    };

    $.fn.rating = function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Function ' +  method + '" not found' );
        }
    };
})( jQuery );


