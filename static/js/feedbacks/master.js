/**
 * Created by dimuska139 on 12.01.17.
 */
/**
 * Created by dimuska139 on 13.12.16.
 */

$(document).ready(function() {
    function checkSuccess() {
        var fieldsAmount = $('.form-line .indicator').length;
        var successFieldsAmount = $('.form-line .indicator:visible').length;
        if (fieldsAmount==successFieldsAmount) {
            $('#save-button').removeClass('disabled');
        } else {
            $('#save-button').addClass('disabled');
        }
    }


    $('#id_text,#id_name').on('change keyup input click', function(event) {
        var maxLength = $(this).attr("maxlength")!=undefined?parseInt($(this).attr("maxlength")):false;
        if (this.value.charAt(0)==' ')
            this.value = this.value.substr(1);
        if (maxLength && this.value.length>maxLength)
            this.value = this.value.substr(0, maxLength);
        if (this.value.length>0) {
            $(this).parent(".form-line").find(".indicator").show();
        } else {
            $(this).parent(".form-line").find(".indicator").hide();
        }
        checkSuccess();
    });

    $('#id_email').on('change keyup input click', function(event) {
        var r = /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/i;

        if (!r.test($(this).val())) {
            $(this).parent(".form-line").find(".indicator").hide();
        } else
            $(this).parent(".form-line").find(".indicator").show();
        checkSuccess();
    });

    $('.photoupload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        drop: function (e, data) {
        },
        start: function(e) {
            $(this).parent('.upload-button').parent('.image-upload-widget').parent('.form-line').find('.indicator').hide();
            $(this).parent('.upload-button').hide();
            $(this).parent('.upload-button').parent('.image-upload-widget').find('.upload-progress-step .progress .bar').css('width', '0%');
            $(this).parent('.upload-button').parent('.image-upload-widget').find('.upload-progress-step').show();
        },
        done: function (e, data) {
            $(this).parent('.upload-button').parent('.image-upload-widget').parent('.form-line').find('.indicator').show();
            if (checkSuccess()) {
                $('#save-button').removeClass('disabled');
            } else {
                $('#save-button').addClass('disabled');
            }
            var data = data.result;
            $(this).parent('.upload-button').parent('.image-upload-widget').find('.upload-progress-step').hide();
            if (!data.success) {
                $(this).parent('.upload-button').parent('.image-upload-widget').find('.upload-error-step').show().find('.text').html(data.message);
                var me = this;
                setTimeout(function() {
                    $(me).parent('.upload-button').parent('.image-upload-widget').find('.upload-error-step').hide();
                    $(me).parent('.upload-button').parent('.image-upload-widget').find('.upload-button').show();
                }, 2000);
                return;
            }
            $('#image_id').val(data.id);
            $(this).parent('.upload-button').show();
            $('.image').html('<img src="' + data.url + '"/>');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $(this).parent('.upload-button').parent('.image-upload-widget').find('.upload-progress-step .progress .bar').css('width', progress+'%');
        }
    });

    $('.add-feedback-form').on('click', '.save-button:not(.disabled)', function(){
        $('.add-feedback-form').submit();
    });


    $('.form-line > input, .form-line > textarea').trigger('click');

    function init() {
        $('.photos-widget').parent('.form-line').find('.indicator').show();
        if (typeof(number) != 'undefined') {
            $('.image-upload-widget').parent('.form-line').find('.indicator').show();
            checkSuccess();
        }
    }

    init();
});