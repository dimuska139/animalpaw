/**
 * Created by dimuska139 on 21.07.16.
 */

var map, marker, geocoder;

var custom_address = false; // Юзер ввёл адрес вручную

var cropper = false;


function initCrop() {
    var image = $('.image-place img').get(0);
    if (typeof(image) == 'undefined')
        return;
    if (cropper)
        cropper.destroy();
    cropper = new Cropper(image, {
        movable: false,
        zoomable: false,
        rotatable: false,
        scalable: false,
        aspectRatio: 1,
        guides: false,
        cropend: function () {
            var cropper = this.cropper;
            var cropData = cropper.getData();
            var image = {};
            image.x_1 = cropData.x;
            image.y_1 = cropData.y;
            image.x_2 = cropData.x + cropData.width;
            image.y_2 = cropData.y + cropData.height;

            if (cropData.x < 0) {
                image.x_2 += cropData.x;
                image.x_1 = 0;
            }

            if (cropData.y < 0) {
                image.y_2 += cropData.y;
                image.y_1 = 0;
            }
            $('#logo_x1').val(image.x_1);
            $('#logo_y1').val(image.y_1);
            $('#logo_x2').val(image.x_2);
            $('#logo_y2').val(image.y_2);
        },
        ready: function () {
            if ($('#logo_x1').val().length != 0) {
                cropper.setData({
                    'x': parseInt($('#logo_x1').val()),
                    'y': parseInt($('#logo_y1').val()),
                    'width': parseInt($('#logo_x2').val()) - parseInt($('#logo_x1').val()),
                    'height': parseInt($('#logo_y2').val()) - parseInt($('#logo_y1').val()),
                    'rotate': 0,
                    'scaleX': 1,
                    'scaleY': 1
                });
            }

            var cropData = cropper.getData();
            $('#logo_x1').val(cropData.x);
            $('#logo_y1').val(cropData.y);
            $('#logo_x2').val(cropData.x + cropData.width);
            $('#logo_y2').val(cropData.y + cropData.height);
        }
    });
}

function initLogoUploader() {
    $('.logo-upload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        drop: function (e, data) {
        },
        start: function(e) {
            $('.crop-widget .btn-upload').hide();
            $('.crop-widget .image-place').hide();
            $('.crop-widget .error').hide();
            $('.crop-widget .progress').show();
        },
        done: function (e, data) {
            $('.crop-widget .progress').hide();
            $('.crop-widget .btn-upload').show();

            var data = data.result;
            if (!data.success) {
                $('.crop-widget .image-place').show();
                $('.crop-widget .error').html(data.message).show();
                setTimeout(function() {
                    $('.crop-widget .error').slideUp();
                }, 5000);
                return;
            }
            $('#logo_id').val(data.id);
            $('.crop-widget .image-place').html(data.html).show();

            initCrop();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.crop-widget .progress').val(progress);
        }
    });
}

function checkPhotos() {
    if ($('.master-form .photo-preview').length >= MAX_PHOTOS_AMOUNT) {
        $('.image-widget .btn-upload').hide();
    } else
        $('.image-widget .btn-upload').show();
}

function initUploader() {
    checkPhotos();
    $('.photo-upload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        drop: function (e, data) {
        },
        start: function(e) {
            $('.image-widget .btn-upload').hide();
            $('.image-widget .image-place').hide();
            $('.image-widget .error').hide();
            $('.image-widget .progress').show();
        },
        done: function (e, data) {
            $('.image-widget .progress').hide();
            $('.image-widget .btn-upload').show();

            var data = data.result;
            if (!data.success) {
                $('.image-widget .photos-container').show();
                $('.image-widget .error').html(data.message).show();
                setTimeout(function() {
                    $('.crop-widget .error').slideUp();
                }, 5000);
                return;
            }
            $('.image-widget .photos-container').append(data.html).show();
            checkPhotos();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.image-widget .progress').val(progress);
        }
    });
}


function initKind() {
    var kind = $('.master-form .kind:checked').val();

    if (kind === undefined)
        return;

    $('#breed-selector').html($('<option value="" selected="selected">Неизвестно</option>'));
    switch (parseInt(kind)) {
        case 1:
            for (var i=0;i<dog_breeds.length;i++)
                $('#breed-selector').append( $('<option value="'+dog_breeds[i].id+'">'+dog_breeds[i].name+'</option>'));
            break;
        case 2:
            for (var i=0;i<cat_breeds.length;i++)
                $('#breed-selector').append( $('<option value="'+cat_breeds[i].id+'">'+cat_breeds[i].name+'</option>'));
            break;
        case 3:
            for (var i=0;i<bird_breeds.length;i++)
                $('#breed-selector').append( $('<option value="'+bird_breeds[i].id+'">'+bird_breeds[i].name+'</option>'));
            break;
    }

    if ($('#id_breed').val().length != 0) {
        $('#breed-selector').val($('#id_breed').val());
    } else
        $('#breed-selector').val('').trigger('chosen:updated')

    $('.breed-place').removeClass('has-danger');
    $('.breed-place .errorlist').remove();
    $('.breed-place').css('display', 'flex');
}

function initRegion() {
    if ($('#id_region').val().length != 0) {
        $('#region-selector').val($('#id_region').val());
    } else
        $('#region-selector').val('').trigger('chosen:updated')
}

// Инициализация маркера
function initMarker(latitude, longitude) {
    var image = '/static/img/dog.png';
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        icon: image
    });
}

// Инициализация карты
function initMap() {
    var input = (document.getElementById('id_address'));
    if (input == null)
        return;

    if ($('#latitude').val().length !=0 && $('#longitude').val().length !=0) {
        var latitude = parseFloat($('#latitude').val());
        var longitude = parseFloat($('#longitude').val());
    } else {
        var latitude = 59.955727;
        var longitude = 30.302690;
    }

    geocoder = new google.maps.Geocoder;
    map = new google.maps.Map($('.map')[0], {
        disableDefaultUI: true,
        zoom: 13,
        center: {
            lat: latitude,
            lng: longitude
        },
        zoomControl: true,
        scaleControl: true
    });

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    $('#latitude').val(latitude);
    $('#longitude').val(longitude);

    initMarker(latitude, longitude);


    // Обработчик события передвинутого маркера
    google.maps.event.addListener(marker, 'dragend', function(e) {
        //$.cookie('latitude', e.latLng.lat(), { path: '/' });
        //$.cookie('longitude', e.latLng.lng(), { path: '/' });
        $('#latitude').val(e.latLng.lat());
        $('#longitude').val(e.latLng.lng());
        var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
        geocoder.geocode({'location': latlng }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if ((!custom_address || $('#id_address').val().length == 0) && results[0]) {
                    $('#id_address').val(results[0].formatted_address);
                }
            }
        });
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(10);
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
        custom_address = true;
    });
}


function scrollToError() {
    if ($('.has-danger:first').length == 0)
        return;
    $('html,body').animate({
        scrollTop: $('.has-danger:first').offset().top - $('.fixed-top').height()
    }, 1000);
}

function init() {
    initCrop();
    initLogoUploader();
    initUploader();
    initKind();
    initRegion();
    initMap();
}

$(document).ready(function(){
    init();

    $('.master-form').bind('error', function() {
        init();
        scrollToError();
    });

    $('.master-form').on('change', '.kind', function() {
        initKind();
        $('#breed-selector').val('').trigger('chosen:updated')
    });

    $('.master-form').on('change', '.type', function() {
        if (parseInt($(this).val()) == 4) {
            $('.price-line').val('').css('display', 'flex');
        } else {
            $('.price-line').hide().val('');
        }
    });

    $('.master-form').on('change', '#breed-selector', function() {
        $('#id_breed').val($('#breed-selector').val());
    });


    $('.master-form').on('click', '.delete-photo', function() {
        $(this).parent('.photo-preview').slideUp('fast', function() {
            $(this).remove();
            checkPhotos();
        });
    });

    $('.master-form').on('change', '#region-selector', function() {
        $('#id_region').val($('#region-selector').val());
        if ($(this).val().length!=0) {
            if ($('#id_address').val().length == 0) {
                var lat = $("#region-selector option:selected").data('latitude');
                var lon = $("#region-selector option:selected").data('longitude');
                if (lat.length == 0 || lon.length == 0)
                    return;
                var latlng = new google.maps.LatLng(lat, lon);
                marker.setPosition(latlng);
                map.panTo(latlng);
            }
        }
    });
});