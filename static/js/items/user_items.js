/**
 * Created by dimuska139 on 14.08.16.
 */

$(document).ready(function() {
    $('.load-more').loadMore({
        'max_page': max_page,
        'container': $('.items'),
        'url': items_url
    });

    $('.load-more').bind('success', function() {
        $('#search').removeClass('disabled');
    });

    $('.items').on('click', '.item-delete', function(e) {
        var link = $(this).attr('href');
        if (confirm('Вы уверены, что хотите удалить объявление навсегда?')) {
            $.ajax({
                type: 'post',
                url: link,
                dataType: 'json',
                complete: function (xhr) {
                    $('.load-more').loadMore('setParam', 'page', 0);
                    $('.items').html('');
                    $('.load-more').loadMore('load');
                }
            });
        }
        e.preventDefault();
    });
});