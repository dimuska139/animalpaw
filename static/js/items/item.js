/**
 * Created by dimuska139 on 18.08.16.
 */



$(document).ready(function() {
    function initMap(lat, lon) {
        var latLng = new google.maps.LatLng(lat, lon);

        var map = new google.maps.Map($('#map')[0], {
            disableDefaultUI: true,
            scrollwheel: false,
            zoom: 16,
            center: {
                lat: lat,
                lng: lon
            },
            zoomControl: true,
            scaleControl: true
        });
        var marker = new google.maps.Marker({
            'map': map,
            'position': latLng
        });
    }

    var lat = parseFloat($('#map').data('lat'));
    var lon = parseFloat($('#map').data('lon'));
    if ($('#map').html().length == 0)
        initMap(lat, lon);

    $('.show-phone-button').on('click', function(){
        var me = this;
        $(this).hide();
        $(this).parent('.phone').find('.loading-indicator').show();
        $.ajax({
            type: "post",
            url: phone_img_url,
            dataType: "json",
            complete: function(){
            },
            success: function(res){
                $(me).parent('.phone').find('.loading-indicator').hide();
                if (res.success) {
                    $('.phone-place').html($(res.html));
                } else {
                    $('.phone-place').find('.text.error').html(res.error);
                }
            }
        });
    });

    $('.write-message-button').on('click', function(){
        var me = this;
        $(this).hide();
        $('#feedback-form').show();
    });

    $('#feedback-form').bind('success', function() {
        $(this).find('.row').hide();
    });

});