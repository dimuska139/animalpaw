/**
 * Created by dimuska139 on 14.08.16.
 */
function getFilterVals() {
    var filters = {};
    filters.region = $('#id_region').val();
    filters.type = $('#id_type').val();
    filters.kind = $('#id_kind').val();
    return filters;
}

function changeUrl(options) {
    if (typeof (history.replaceState) != "undefined") {
        var urlParts = [];
        var state = {};
        for (var key in options) {
            if (options[key].length > 0) {
                urlParts.push(key + '=' + options[key]);
                state.key = options[key]
            }
        }
        if (urlParts.length != 0)
            var url = '?' + urlParts.join('&');
        else
            var url = './';
        history.replaceState(state, '', url);
    }
}


$(document).ready(function() {
    $('.load-more').loadMore({
        'max_page': max_page,
        'container': $('.items'),
        'url': items_url
    });

    $('.load-more').bind('success', function() {
        changeUrl(getFilterVals());
        $('#search').removeClass('disabled');
    });

    $('#search').on('click', function() {
        if ($(this).hasClass('disabled'))
            return;

        $('.load-more').loadMore('setParam', 'page', 0);
        $('.load-more').loadMore('setParams', getFilterVals());
        $('.items').html('');
        $(this).addClass('disabled');
        $('.load-more').loadMore('load');
    });
});