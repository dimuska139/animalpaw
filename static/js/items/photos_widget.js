/**
 * Created by dimuska139 on 07.11.16.
 */
$(document).ready(function() {
    $('.photos-widget .previews').on('click', '.preview:not(.active)', function () {
        var newSrc = $(this).data('url');
        $(this).parent('.previews').parent('.photos-widget').find('.main').css({'background-image': 'url(' + newSrc + ')'});
        $(this).parent('.previews').find('.active').removeClass('active');
        $(this).addClass('active');
    });
});