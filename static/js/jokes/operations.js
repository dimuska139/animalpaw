/**
 * Created by dimuska139 on 11.04.17.
 */
$(document).ready(function() {
    $('.jokes').on('click', '.delete-joke,.censor-joke', function(){
        var me = this;
        $.ajax({
            type: "get",
            url: $(me).data('href'),
            dataType: "json",
            complete: function(){
            },
            success: function(res){
                if (res.success) {
                    if ($(me).hasClass('delete-joke')) {
                        $(me).parents('.joke').slideUp();
                    } else {
                        if ($('#censored input').is(':checked')) {
                            $(me).parents('.joke').slideUp();
                        } else {
                            $(me).fadeOut();
                        }
                    }
                } else {
                    alert('Что-то пошло не так')
                }
            }
        });
    });
});