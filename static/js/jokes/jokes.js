/**
 * Created by dimuska139 on 11.04.17.
 */

$(document).ready(function() {
    function reload() {
        var searchValue = $('#search-value').val();
        var show_censored = $('#censored input').is(':checked')?0:1;
        $('.load-more').loadMore('setParam', 'page', 0);
        $('.load-more').loadMore('setParam', 'search', searchValue);
        $('.load-more').loadMore('setParam', 'show_censored', show_censored);

        $.cookie('show_censored', show_censored);
        $('.jokes').html('');
        $(this).addClass('disabled');
        $('.load-more').loadMore('load');
    }

    $('.load-more').loadMore({
        'max_page': max_page,
        'container': $('.jokes'),
    });

    $('.load-more').bind('success', function() {
        var searchValue = $('#search-value').val();
        changeUrl({
            'search': searchValue,
            'show_censored': $('#censored input').is(':checked')?0:1
        });
        $('#search').removeClass('disabled');
    });

    $('#search').on('click', function() {
        if ($(this).hasClass('disabled'))
            return;
        reload();
    });

    $('#censored input').on('change', function(){
        reload();
    });

    $("#search-value").keyup(function(event){
        if (event.keyCode == 13){
            event.preventDefault();
            reload();
        }
    });
});