/**
 * Created by dimuska139 on 13.04.17.
 */
function initEditor() {
    CKEDITOR.replace('id_text', {
        extraPlugins: 'table,tabletools,wordcount,notification',
        height: 350,
        // Remove unused plugins.
        removePlugins: 'about,image',
        // Remove unused buttons.
        removeButtons: 'BGColor,Font,Strike,Subscript,Superscript',
        // Width and height are not supported in the BBCode format, so object resizing is disabled.
        disableObjectResizing: true,
        // Define font sizes in percent values.
        fontSize_sizes: "30/30%;50/50%;100/100%;120/120%;150/150%;200/200%;300/300%"
    });
    CKEDITOR.config.wordcount = {

        // Whether or not you want to show the Paragraphs Count
        showParagraphs: true,

        // Whether or not you want to show the Word Count
        showWordCount: true,

        // Whether or not you want to show the Char Count
        showCharCount: true,

        // Whether or not you want to count Spaces as Chars
        countSpacesAsChars: false,

        // Whether or not to include Html chars in the Char Count
        countHTML: false,

        // Maximum allowed Word Count, -1 is default for unlimited
        maxWordCount: -1,

        // Maximum allowed Char Count, -1 is default for unlimited
        maxCharCount: -1,

        // Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
        filter: new CKEDITOR.htmlParser.filter({
            elements: {
                div: function( element ) {
                    if(element.attributes.class == 'mediaembed') {
                        return false;
                    }
                }
            }
        })
    };

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        if (dialogName == 'link') {
            dialogDefinition.removeContents('upload');
        }
        var editor = ev.editor;
        editor.dataProcessor.htmlFilter.addRules({
            elements: {
                a: function (element) {
                    if (!element.attributes.rel)
                        element.attributes.rel = 'nofollow';
                }
            }
        });
    });

    CKEDITOR.instances['id_text'].on('change', function () {
        var data = this.getData();
        $('#id_text').val(data);
    });
}

function initDatepicker() {
    $.datetimepicker.setLocale('ru');
    var date = $('#id_published_at').val();
    if (date.length == 0)
        date = new Date();
    else
        date = new Date(date);
    $('#id_published_at').datetimepicker({
        format: 'Y-m-d H:i',
        value: date,
        mask: '9999-19-39 29:59'
    });
}

function init() {
    initDatepicker();
}

$(document).ready(function() {
    $('#joke-master-form').bind('error', function() {
        init();
    });
    initEditor();
    init();
});