/**
 * Created by dimuska139 on 04.04.17.
 */
$(document).ready(function() {
    function initLightbox() {
        $('.article-image').each(function (i, elem) {
            var src = $(this).attr('src');
            $(this).wrap('<a class="article-image-lightbox" href="' + src + '" data-toggle="lightbox"></a>');
        });

        $('.article-image-lightbox').on('click', function(event){
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    }

    initLightbox();

    $('.comments').comments({
        'content_type': content_type,
        'object_id': object_id
    });

    $('.rating').rating({
        'content_type': content_type,
        'object_id': object_id
    });

});