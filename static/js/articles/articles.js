$(document).ready(function() {
    $('.load-more').loadMore({
        'max_page': max_page,
        'container': $('.articles'),
    });

    if ($('#tag').length !=0 && $('#tag').val().length > 0) {
        $('.load-more').loadMore('setParam', 'tag', $('#tag').val());
    }

    $('.load-more').bind('success', function() {
        var searchValue = $('#search-value').val();
        changeUrl({
            'search': searchValue
        });
        $('#search').removeClass('disabled');
    });

    $('#search').on('click', function() {
        if ($(this).hasClass('disabled'))
            return;

        var searchValue = $('#search-value').val();
        $('.load-more').loadMore('setParam', 'page', 0);
        $('.load-more').loadMore('setParam', 'search', searchValue);
        $('.articles').html('');
        $(this).addClass('disabled');
        $('.load-more').loadMore('load');
    });

    $("#search-value").keyup(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $('#search').trigger('click');
        }
    });
});