/**
 * Created by dimuska139 on 13.12.16.
 */


var cropper = false;


function initCrop() {
    var image = $('.image-place img').get(0);
    if (typeof(image) == 'undefined')
        return;
    if (cropper)
        cropper.destroy();
    cropper = new Cropper(image, {
        movable: false,
        zoomable: false,
        rotatable: false,
        scalable: false,
        aspectRatio: 1,
        guides: false,
        cropend: function () {
            var cropper = this.cropper;
            var cropData = cropper.getData();
            var image = {};
            image.x_1 = cropData.x;
            image.y_1 = cropData.y;
            image.x_2 = cropData.x + cropData.width;
            image.y_2 = cropData.y + cropData.height;

            if (cropData.x < 0) {
                image.x_2 += cropData.x;
                image.x_1 = 0;
            }

            if (cropData.y < 0) {
                image.y_2 += cropData.y;
                image.y_1 = 0;
            }
            $('#logo_x1').val(image.x_1);
            $('#logo_y1').val(image.y_1);
            $('#logo_x2').val(image.x_2);
            $('#logo_y2').val(image.y_2);
        },
        ready: function () {
            if ($('#logo_x1').val().length != 0) {
                cropper.setData({
                    'x': parseInt($('#logo_x1').val()),
                    'y': parseInt($('#logo_y1').val()),
                    'width': parseInt($('#logo_x2').val()) - parseInt($('#logo_x1').val()),
                    'height': parseInt($('#logo_y2').val()) - parseInt($('#logo_y1').val()),
                    'rotate': 0,
                    'scaleX': 1,
                    'scaleY': 1
                });
            }

            var cropData = cropper.getData();
            $('#logo_x1').val(cropData.x);
            $('#logo_y1').val(cropData.y);
            $('#logo_x2').val(cropData.x + cropData.width);
            $('#logo_y2').val(cropData.y + cropData.height);
        }
    });
}

function initUploader() {
    $('.logo-upload').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        drop: function (e, data) {
        },
        start: function(e) {
            $('.crop-widget .btn-upload').hide();
            $('.crop-widget .image-place').hide();
            $('.crop-widget .error').hide();
            $('.crop-widget .progress').show();
        },
        done: function (e, data) {
            $('.crop-widget .progress').hide();
            $('.crop-widget .btn-upload').show();

            var data = data.result;
            if (!data.success) {
                $('.crop-widget .image-place').show();
                $('.crop-widget .error').html(data.message).show();
                setTimeout(function() {
                    $('.crop-widget .error').slideUp();
                }, 5000);
                return;
            }
            $('#logo_id').val(data.id);
            $('.crop-widget .image-place').html('<img class="img-thumbnail" src="' + data.url + '"/>').show();

            initCrop();
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.crop-widget .progress').val(progress);
        }
    });
}

function initEditor() {
    var plugins = [
        'simple-image-browser',
        'table',
        'tabletools',
        'tableresize',
        'wordcount',
        'notification',
        'undo',
        'indent',
        'clipboard',
        'justify',
        'autocorrect',
        'colorbutton',
        'font',
    ];
    CKEDITOR.replace('id_text', {
        extraPlugins: plugins.join(','),
        height: 450,
        // Remove unused plugins.
        removePlugins: 'about',
        // Remove unused buttons.
        removeButtons: 'Subscript,Superscript',
        // Define font sizes in percent values.
        fontSize_sizes: "30/30%;50/50%;100/100%;120/120%;150/150%;200/200%;300/300%",
        on: {
            instanceReady: function() {
                this.dataProcessor.htmlFilter.addRules( {
                    elements: {
                        img: function( el ) {
                            // Add an attribute.
                            if ( !el.attributes.alt )
                                el.attributes.alt = $('#id_title').val();

                            // Add some class.
                            el.addClass( 'article-image' );
                            el.addClass( 'col-md-5' );
                            el.addClass( 'col-sm-6' );
                            el.addClass( 'col-xsm-12' );
                            el.addClass( 'col-12' );
                        }
                    }
                } );
            }
        }
    });
    CKEDITOR.config.filebrowserUploadUrl = upload_article_image_url;
    CKEDITOR.config.simpleImageBrowserURL = browse_article_image_url;
    CKEDITOR.config.contentsCss = cke_styles_url;
    CKEDITOR.config.wordcount = {

        // Whether or not you want to show the Paragraphs Count
        showParagraphs: true,

        // Whether or not you want to show the Word Count
        showWordCount: true,

        // Whether or not you want to show the Char Count
        showCharCount: true,

        // Whether or not you want to count Spaces as Chars
        countSpacesAsChars: false,

        // Whether or not to include Html chars in the Char Count
        countHTML: false,

        // Maximum allowed Word Count, -1 is default for unlimited
        maxWordCount: -1,

        // Maximum allowed Char Count, -1 is default for unlimited
        maxCharCount: -1,

        // Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
        filter: new CKEDITOR.htmlParser.filter({
            elements: {
                div: function( element ) {
                    if(element.attributes.class == 'mediaembed') {
                        return false;
                    }
                }
            }
        })
    };

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        if (dialogName == 'link') {
            dialogDefinition.removeContents('upload');
        }
        var editor = ev.editor;
        editor.dataProcessor.htmlFilter.addRules(
            {
                elements: {
                    a: function (element) {
                        if (!element.attributes.rel && element.attributes.href.indexOf('animalpaw') == -1)
                            element.attributes.rel = 'nofollow';
                    }
                }
            });
    });

    CKEDITOR.instances['id_text'].on('change', function () {
        var data = this.getData();
        $('#id_text').val(data);
    });
}

function initDatepicker() {
    $.datetimepicker.setLocale('ru');
    var date = $('#id_published_at').val();
    if (date.length == 0)
        date = new Date();
    else
        date = new Date(date);
    $('#id_published_at').datetimepicker({
        format: 'Y-m-d H:i',
        value: date,
        mask: '9999-19-39 29:59'
    });
}

function init() {
    initCrop();
    initUploader();
    initDatepicker();
}

$(document).ready(function() {
    $('#article-master-form').bind('error', function() {
        init();
    });
    initEditor();
    init();
});