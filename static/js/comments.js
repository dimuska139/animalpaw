/**
 * Created by dimuska139 on 04.05.17.
 */

(function( $ ){
    var settings = {
        'socket': false,
        'content_type': 0,
        'object_id': 0
    };

    var options = {};

    var methods = {
        init:function(params) {
            settings = $.extend(settings, params);
            var me = this;
            if ($(me).find('.comments-list .comment').length == 0)
                $(me).find('.message-no-comments').show();

            var protocol = 'ws:';
            if ('https:' == document.location.protocol)
                protocol = 'wss:';

            settings.socket = new WebSocket(protocol + '//' + window.location.host + '/ws/comments/' + settings.content_type + '/' + settings.object_id + '/');

            settings.socket.onopen = function() {
                console.dir("Соединение установлено");
            };

            settings.socket.onclose = function (event) {
                if (event.wasClean) {
                    console.dir('Соединение закрыто');
                } else {
                    console.dir('Обрыв соединения');
                }
            };

            settings.socket.onerror = function (error) {
                console.dir("Ошибка " + error.message);
            };

            //if (settings.socket.readyState == WebSocket.OPEN) settings.socket.onopen();

            settings.socket.onmessage = function (message) {
                var data = JSON.parse(message.data);
                switch (data.action) {
                    case 'new':
                        $(me).find('.message-no-comments').hide();
                        $(me).find('.comments-list').prepend(data.html);
                        if (data.user_id == user_id) {
                            $('.comment_' + data.id + ' .comment-delete').show();
                            $('.comment_' + data.id + ' .comment-answer').hide();
                        } else {
                            $('.comment_' + data.id + ' .comment-delete').hide();
                            $('.comment_' + data.id + ' .comment-answer').show();
                        }
                        break;
                    case 'delete':
                        $(me).find('.comments-list .comment_' + data.id).slideUp('fast', function() {
                            $(this).remove();
                            if ($(me).find('.comments-list .comment').length == 0)
                                $(me).find('.message-no-comments').show();
                        });
                }
                $(me).find('.comments-amount').html(data.total);
            };

            $(me).find('.new-comment-form form').bind('success', function() {
                $(this).find('textarea').val('');
                $(this).find('.photo-preview').remove();
                $(me).find('.new-comment-form .upload-btn').show();
            });

            $(me).on('click', '.comment-image-lightbox', function(event){
                event.preventDefault();
                $(this).ekkoLightbox();
            });

            $(me).find('.new-comment-form .upload-btn .input-upload').fileupload({
                dataType: 'json',
                sequentialUploads: true,
                drop: function (e, data) {
                },
                start: function(e) {
                    $(me).find('.new-comment-form .upload-btn .icon-photo').hide();
                    $(me).find('.new-comment-form .upload-btn .file-upload-progress').html('').show();
                },
                done: function (e, data) {
                    $(me).find('.new-comment-form .upload-btn .file-upload-progress').html('').hide();

                    var data = data.result;
                    if (!data.success) {
                        $(me).find('.new-comment-form .upload-btn .icon-photo').show();
                        $(me).find('.new-comment-form .non-field-errors').html(data.message).show();
                        setTimeout(function() {
                            $(me).find('.new-comment-form .non-field-errors').html('').hide();
                        }, 5000);
                        return;
                    }
                    $(me).find('.new-comment-form .upload-btn').hide();
                    $(me).find('.new-comment-form .upload-btn .icon-photo').show();
                    $(me).find('.new-comment-form .photo-place').html(data.html).show();
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10) + '%';
                    $(me).find('.new-comment-form .upload-btn .file-upload-progress').html(progress);
                }
            });

            $('.new-comment-form').on('click', '.delete-photo', function() {
                $(this).parent('.photo-preview').slideUp('fast', function() {
                    $(this).parent('.photo-preview');
                    $(me).find('.new-comment-form .upload-btn').show();
                });
            });

            $('.comments-list').on('click', '.comment-delete', function(){
                var delete_url = $(this).data('url');
                $.ajax({
                    type: "get",
                    url: delete_url,
                    dataType: "json",
                    complete: function(){
                    },
                    success: function(res){
                        if (!res.success) {
                            alert('Что-то пошло не так')
                        }
                    }
                });
            });
        },
        setParam:function(key, value) {
            var newOption = {};
            newOption[key] = value;
            options = $.extend(options, newOption);
        },
        setParams:function(object) {
            options = $.extend(options, object);
        }
    };

    $.fn.comments = function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Function ' +  method + '" not found' );
        }
    };
})( jQuery );


