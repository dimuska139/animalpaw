function changeUrl(options) {
    if (typeof (history.replaceState) != "undefined") {
        var urlParts = [];
        var state = {};
        for (var key in options) {
            if (options[key].length > 0) {
                urlParts.push(key + '=' + options[key]);
                state.key = options[key];
            }
        }
        if (urlParts.length != 0)
            var url = '?' + urlParts.join('&');
        else
            var url = './';
        history.replaceState(state, '', url);
    }
}

$(document).ready(function() {
    $('#sign-in').on('click', function() {
        if ($('#sign-in').parent().find('.dropdown-menu').is(':visible'))
            $(this).parent().find('.dropdown-menu').hide();
        else
            $(this).parent().find('.dropdown-menu').show();
    });

    $('body').on('click', function (e) {
        if (!$('#sign-in').is(e.target)
            && !$('.nav-login.dropdown-menu').is(e.target)
            && $('.nav-login.dropdown-menu').has(e.target).length === 0
            && $('#sign-in').parent().find('.dropdown-menu').is(':visible')
        ) {
            $('#sign-in').parent().find('.dropdown-menu').hide();
        }
    });
});