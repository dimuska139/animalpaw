/**
 * Created by dimuska139 on 24.01.17.
 */
(function( $ ){
    var settings = {
        'max_page': 1,
        'method': 'get',
        'url': './'
    };

    var options = {
        'page': 1
    };

    var methods = {
        init:function(params) {
            settings = $.extend(settings, params);
            var me = this;

            if (options.page >= settings.max_page)
                $(me).find('.load-main').hide();

            $(me).on('click', '.load,.load-repeat', function () {
                $(me).find('.load-main').hide();
                $(me).find('.loading').show();
                $(me).find('.error').hide();
                var data = options;
                data.page += 1;
                $.ajax({
                    type: settings.method,
                    url: settings.url,
                    dataType: 'json',
                    data: data,
                    complete: function (xhr) {
                        $(me).find('.loading').hide();
                        if (xhr.status != 200) {
                            $(me).find('.error .status-code').html(xhr.status);
                            $(me).find('.load-main').hide();
                            $(me).find('.error').show();
                        }
                    },
                    success: function (res) {
                        if (res.success) {
                            settings.max_page = res.max_page;
                            $(settings.container).append(res.html);
                            options.page = data.page;
                            $(me).trigger('success');
                            if (options.page < settings.max_page)
                                $(me).find('.load-main').show();
                        }
                    }
                });
            });
        },
        load:function() {
            $(this).find('.load').trigger('click');
        },
        setParam:function(key, value) {
            var newOption = {};
            newOption[key] = value;
            options = $.extend(options, newOption);
        },
        setParams:function(object) {
            options = $.extend(options, object);
        }
    };

    $.fn.loadMore = function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Function ' +  method + '" not found' );
        }
    };
})( jQuery );

