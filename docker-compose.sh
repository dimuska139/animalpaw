#!/usr/bin/env bash
sudo service memcached stop
sudo service apache2 stop
sudo docker pull registry.gitlab.com/dimuska139-images/animalpaw-http
sudo docker-compose kill
sudo docker-compose -f ./docker-compose-http.yml up -d --remove-orphans