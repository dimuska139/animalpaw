from channels import Group
from channels.auth import channel_session_user_from_http, channel_session_user
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.template import loader
from django.views.decorators.csrf import csrf_protect
from jsonview.decorators import json_view, json

from comments.forms import CommentForm
from comments.models import Comment
from extusers.decorators import login_required_json
from utils.forms import get_errors
from PIL import Image as PILImage

from utils.models import Image


def get_channel_name(content_type, object_id):
    ct = ContentType.objects.get(id=content_type)
    return ct.app_label + '_' + str(object_id)


def socket_push(content_type, object_id, data):
    ch_name = get_channel_name(content_type, object_id)
    Group(ch_name).send({
        'text': json.dumps(data)
    })


@channel_session_user_from_http
def ws_connect(message, content_type, object_id):
    ch_name = get_channel_name(content_type, object_id)
    message.reply_channel.send({'accept': True})
    Group(ch_name).add(message.reply_channel)


"""@channel_session_user
def ws_message(message, id):
    data = json.loads(message.content['text'])
    if message.user.is_authenticated():
        Group('article_'+str(id)).send({'text': json.dumps({'message': data['message'], 'sender': message.user.name})})
    else:
        Group('article_'+str(id)).send({'text': json.dumps({'message': data['message'], 'sender': "New user " + str(id)})})"""


@channel_session_user
def ws_disconnect(message, content_type, object_id):
    ch_name = get_channel_name(content_type, object_id)
    Group(ch_name).discard(message.reply_channel)


@login_required_json
@csrf_protect
@json_view
def create(request, content_type, object_id):
    ct = ContentType.objects.get(id=content_type)
    ct_class = ct.model_class()

    object = ct_class.objects.filter(id=object_id, status=ct_class.STATUSES.active).first()

    if not isinstance(object, ct_class):
        return {
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Not found'
                }
            ]
        }

    form = CommentForm(request.POST)
    if not form.is_valid():
        return {
            'success': False,
            'errors': get_errors(form)
        }
    if 'parent_id' in request.POST and len(request.POST['parent_id']) > 0:
        parent_comment = Comment.objects.get(id=request.POST['parent_id'])
        if not isinstance(parent_comment, Comment):
            return {
                'success': False,
                'errors': [
                    {
                        'key': 'all',
                        'description': 'Комментарий, на который Вы хотите ответить, удалён'
                    }
                ]
            }

        comment = object.comments.create(text=request.POST['text'], user=request.user, parent=parent_comment)
    else:
        comment = object.comments.create(text=request.POST['text'], user=request.user)

    # Связать загруженные фотографии с созданным объявлением
    photos_ids = request.POST.getlist('photos')
    photos = Image.objects.filter(id__in=photos_ids).all()
    for photo in photos:
        comment.photos.add(photo)

    template = loader.get_template('comments/comment.html')
    context = {
        'hide_operations': True,
        'comment': comment,
        'object': object
    }
    html = template.render(context, request)

    total = object.comments.filter(status=Comment.STATUSES.active, parent=None).count()

    data = {
        'action': 'new',
        'total': total,
        'id': comment.id,
        'user_id': request.user.id,
        'comment': request.POST['text'],
        'parent_id': comment.parent_id,
        'html': html
    }
    socket_push(content_type, object_id, data)
    return {
        'success': True,
    }


@login_required_json
@csrf_protect
@json_view
def delete(request, content_type, object_id, comment_id):
    comment = Comment.objects.filter(id=comment_id, status=Comment.STATUSES.active, user=request.user).first()

    if not isinstance(comment, Comment):
        return {
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Комментарий не найден'
                }
            ]
        }
    object_id = comment.object_id
    content_type = comment.content_type_id
    comment.status = Comment.STATUSES.deleted
    comment.save()

    total = Comment.objects.filter(content_type=content_type, object_id=object_id, status=Comment.STATUSES.active,
                                   parent=None).count()

    data = {
        'action': 'delete',
        'id': comment.id,
        'total': total
    }
    socket_push(content_type, object_id, data)

    return {
        'success': True,
    }


@json_view
@login_required_json
def photo_upload(request):
    if 'photo' not in request.FILES:
        response = {
            'success': False,
            'message': 'Файл не обнаружен',
            'html': ''
        }
        return response

    response = {
        'success': True,
        'message': '',
        'html': ''
    }

    if request.FILES['photo'].size > settings.MAX_ITEM_PHOTOS_SIZE:
        response['success'] = False
        response['message'] = 'Максимальный размер загружаемого файла - ' + str(
            settings.MAX_COMMENT_PHOTOS_SIZE / 1024) + ' кБ'
        return response

    # Check if uploaded file is image
    try:
        PILImage.open(request.FILES['photo'])
    except IOError as e:
        response['success'] = False
        response['message'] = "Недопустимый формат файла"
        return response
    photo = Image(
        original=request.FILES['photo'],
        user=request.user
    )
    photo.save()

    photo.make_preview(maxsize=(400, 400))

    context = {
        'photo': photo
    }

    template = loader.get_template('comments/uploaded_photo.html')
    rendered_template = template.render(context, request)
    response['id'] = photo.pk
    response['message'] = 'OK'
    response['html'] = rendered_template

    return response
