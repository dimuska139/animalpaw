# -*- coding: utf-8 -*-
__author__ = 'dimuska139'

from django.forms import ModelForm
from .models import Comment


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        exclude = (
            'status',
            'user',
            'parent',
            'content_type',
            'object_id',
            'content_object'
        )

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['text'].label = 'Комментарий'
