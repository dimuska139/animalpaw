# coding: utf-8
from django.test import TestCase
from comments.forms import CommentForm


class CommentTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_valid_form(self):
        data = {
            'text': 'Комментарий',
        }
        form = CommentForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'text': '',
        }
        form = CommentForm(data=data)
        self.assertFalse(form.is_valid())
