# -*- coding: utf-8 -*-
import json

from django.test import TestCase
from django.urls import reverse

from articles.models import Article
from comments.models import Comment
from extusers.models import ExtUser


class CommentDeleteTest(TestCase):
    fixtures = ['users.json', 'initial.json', 'articles.json']

    def test_action(self):
        article = Article.objects.first()
        user = ExtUser.objects.get(pk=1)
        article.comments.create(text='Комментарий', user=user)
        comment = article.comments.first()
        url = reverse('comments.delete', args=[
            article.get_content_type(),
            article.id,
            comment.id
        ])

        # Не авторизован
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        # Чужой комментарий
        self.client.login(username=user.email, password='12345')
        comment.user = ExtUser.objects.get(pk=2)
        comment.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_delete(self):
        user = ExtUser.objects.get(id=1)

        article = Article.objects.first()
        article.comments.create(text='Комментарий', user=user)

        self.client.login(username=user.email, password='12345')
        comments_before = article.comments.filter(status=Comment.STATUSES.active).count()
        comment = article.comments.first()
        url = reverse('comments.delete', args=[
            article.get_content_type(),
            article.id,
            comment.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        comments_after = Article.objects.get(id=article.id).comments.filter(status=Comment.STATUSES.active).count()
        self.assertEqual(1, comments_before - comments_after)
