# -*- coding: utf-8 -*-
import json

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.urls import reverse

from articles.models import Article
from extusers.models import ExtUser


class CommentCreationTest(TestCase):
    fixtures = ['users.json', 'initial.json', 'articles.json']

    def test_action(self):
        ct_id = ContentType.objects.get_for_model(Article).id
        article = Article.objects.first()
        url = reverse('comments.create', args=[
            ct_id,
            article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_save(self):
        user = ExtUser.objects.get(id=1)

        article = Article.objects.first()
        ct_id = ContentType.objects.get_for_model(Article).id

        self.client.login(username=user.email, password='12345')
        url = reverse('comments.create', args=[
            ct_id,
            article.id
        ])
        comments_before = article.comments.count()
        response = self.client.post(url, {
            'text': "Новый комментарий",
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        comments_after = Article.objects.get(id=article.id).comments.count()

        self.assertEqual(1, comments_after - comments_before)
