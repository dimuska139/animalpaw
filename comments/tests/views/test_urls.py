# -*- coding: utf-8 -*-

from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('comments.photo.upload'), '/comments/photo/upload')
        self.assertEqual(reverse('comments.create', args=[1, 1]), '/comments/1/1/create')
        self.assertEqual(reverse('comments.delete', args=[1, 1, 1]), '/comments/1/1/1/delete')
