from django.conf.urls import url
from comments import views

urlpatterns = [
    url(r'^(?P<content_type>\d+)/(?P<object_id>\d+)/create', views.create, name='comments.create'),
    url(r'^(?P<content_type>\d+)/(?P<object_id>\d+)/(?P<comment_id>\d+)/delete', views.delete, name='comments.delete'),
    url(r'^photo/upload', views.photo_upload, name='comments.photo.upload'),
]
