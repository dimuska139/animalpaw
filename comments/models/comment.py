from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from model_utils import Choices

from utils.models import Image


class Comment(models.Model):
    user = models.ForeignKey('extusers.Extuser', on_delete=models.SET_NULL, null=True)
    text = models.CharField(max_length=1000)
    parent = models.ForeignKey('comments.Comment', on_delete=models.CASCADE, null=True, blank=True)
    photos = models.ManyToManyField(Image, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()  # https://evileg.com/ru/post/246/

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.text[0:100]

