from celery.task import task
from articles.models import Article


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def sync(article_id):
    article = Article.objects.get(pk=article_id)
    article.vk_sync()

