from django.utils import timezone
from celery.task import task

from articles.models import Article


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def publisher():
    articles = Article.objects.filter(status=Article.STATUSES.active, is_visible=False)
    for article in articles:
        if article.published_at < timezone.now():
            article.is_visible = True
            article.save()
            article.vk_sync()


