# -*- coding: utf-8 -*-
__author__ = 'dimuska139'

from django.forms import ModelForm
from django import forms
from .models import Article
from django.db.models import Q
import re


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        exclude = (
            'views',
            'status',
            'is_history',
            'user',
            'vk_post_id'
        )

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['title'].label = 'Заголовок'
        self.fields['annotation'].label = 'О чём статья'
        self.fields['logo'].label = 'Логотип'
        self.fields['published_at'].label = 'Дата и время публикации'
        self.fields['text'].label = 'Текст статьи'
        self.fields['tags'].label = 'Теги (через запятую)'
