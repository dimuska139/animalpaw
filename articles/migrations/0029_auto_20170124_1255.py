# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-01-24 12:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0028_auto_20170124_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='number',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
