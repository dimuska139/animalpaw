# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-12-12 12:40
from __future__ import unicode_literals

from django.db import migrations, models
import precise_bbcode.fields


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_auto_20161212_1125'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='_text_rendered',
            field=models.TextField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='text',
            field=precise_bbcode.fields.BBCodeTextField(no_rendered_field=True),
        ),
    ]
