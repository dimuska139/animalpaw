# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-12-12 13:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_auto_20161212_1240'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='is_history',
            field=models.BooleanField(default=False),
        ),
    ]
