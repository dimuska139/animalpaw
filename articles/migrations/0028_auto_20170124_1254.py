# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-01-24 12:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0027_auto_20170124_1241'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='number',
            field=models.IntegerField(blank=True, db_index=True, default=0, null=True),
        ),
    ]
