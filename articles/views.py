# coding: utf-8
import os
from PIL import Image as PILimage
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Count
from django.db import connection
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

# Create your views here.
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from jsonview.decorators import json_view
import json

from articles.forms import ArticleForm
from comments.forms import CommentForm
from comments.models import Comment
from extusers.decorators import login_required_json
from extusers.decorators.permission_required import permission_required_json
from utils.forms import get_errors
from .models import Article
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from utils.models import Image
from articles.tasks import sync as vk_sync


@login_required
@csrf_protect
@permission_required('articles.add_article', raise_exception=True)
def creation_page(request):
    article_form = ArticleForm(initial={
        'title': '',
        'url_name': '',
        'tags': '',
        'published_at': '',
        'annotation': '',
        'text': ''
    })
    template = loader.get_template('articles/pages/article_creation.html')
    context = {
        'article_form': article_form
    }
    return HttpResponse(template.render(context, request))


@login_required_json
@csrf_protect
@permission_required_json('articles.add_article')
@json_view
def create(request):
    article_form = ArticleForm(request.POST)
    if not article_form.is_valid():
        return {
            'success': False,
            'errors': get_errors(article_form)
        }

    article = article_form.save()
    article.user_id = request.user.id
    if article.published_at > timezone.now():
        article.is_visible = False
    else:
        article.is_visible = True
        if article.to_vk:
            vk_sync.delay(article.id)
    article.save()

    article.logo.make_logo(x1=request.POST['logo_x1'],
                           y1=request.POST['logo_y1'],
                           x2=request.POST['logo_x2'],
                           y2=request.POST['logo_y2'])

    template = loader.get_template('articles/messages/article_creation_success.html')

    context = {
        'article': article
    }
    messages.info(request, template.render(context, request))
    return {
        'success': True,
        'url': reverse('articles.index')
    }


@login_required
@csrf_protect
@permission_required('articles.change_article', raise_exception=True)
def editing_page(request, id):
    article = get_object_or_404(Article, id=id, status=Article.STATUSES.active)

    tags_list = []
    for tag in article.tags.names():
        tags_list.append(tag)
    tags = ', '.join(tags_list)

    logo = Image.objects.get(pk=article.logo_id)
    article_form = ArticleForm(instance=article)

    template = loader.get_template('articles/pages/article_editing.html')
    context = {
        'article_form': article_form,
        'article_id': article.id,
        'logo': logo,
        'tags': tags.lower()
    }
    return HttpResponse(template.render(context, request))


@login_required_json
@csrf_protect
@permission_required_json('articles.change_article')
@json_view
def edit(request, id):
    article = Article.objects.filter(id=id, status=Article.STATUSES.active).first()
    exists_logo = article.logo

    if not isinstance(article, Article):
        return {
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Статья не найдена'
                }
            ]
        }

    article_form = ArticleForm(request.POST, instance=article)
    if not article_form.is_valid():
        return {
            'success': False,
            'errors': get_errors(article_form)
        }

    article = article_form.save()

    if article.published_at > timezone.now():
        article.is_visible = False
    else:
        article.is_visible = True
    article.save()

    x_1 = int(round(float(request.POST['logo_x1'])))
    y_1 = int(round(float(request.POST['logo_y1'])))
    x_2 = int(round(float(request.POST['logo_x2'])))
    y_2 = int(round(float(request.POST['logo_y2'])))

    eq_coords = [exists_logo.x_1 == x_1, exists_logo.y_1 == y_1, exists_logo.x_2 == x_2, exists_logo.y_2 == y_2]

    # Если изменён логотип или изменились координаты миниатюры
    if exists_logo.id != int(request.POST['logo']) or not all(eq_coords):
        article.logo.make_logo(x1=x_1, y1=y_1, x2=x_2, y2=y_2)

    template = loader.get_template('articles/messages/article_editing_success.html')

    if article.is_visible:
        vk_sync.delay(article.id)

    context = {
        'article': article,
    }
    messages.info(request, template.render(context, request))
    return {
        'success': True,
        'url': reverse('articles.index')
    }


def article(request, id):
    art = Article.objects.filter(status=Article.STATUSES.active, id=id).first()
    if not art:
        raise Http404

    if not request.user.has_perm(
            'articles.add_article') and not request.user.has_perm(
            'articles.change_article') and not art.is_visible:
        raise Http404

    if 'viewed_articles' in request.COOKIES:
        viewed_articles = json.loads(request.COOKIES.get('viewed_articles'))
    else:
        viewed_articles = []

    if request.user.id == art.user_id:
        is_owner = True
    else:
        is_owner = False

    if art.id not in viewed_articles and not is_owner:
        art.views += 1
        art.save()
        viewed_articles.append(art.id)

    tags_list = []
    for tag in art.tags.names():
        tags_list.append(tag)
    meta_keywords = ', '.join(tags_list)

    meta_keywords = meta_keywords.capitalize()

    comment_form = CommentForm()

    comments = art.comments.filter(status=Comment.STATUSES.active, parent=None).select_related('user',
                                                                                                   'user__avatar').prefetch_related(
        'photos').order_by('-created_at')
    comments_amount = art.comments.filter(status=Comment.STATUSES.active, parent=None).count()

    similar_articles = art.get_similar()
    template = loader.get_template('articles/pages/article.html')
    context = {
        'article': art,
        'meta_keywords': meta_keywords,
        'is_owner': is_owner,
        'comment_form': comment_form,
        'comments': comments,
        'comments_amount': comments_amount,
        'similar_articles': similar_articles[0:5]
    }

    response = HttpResponse(template.render(context, request))
    response.set_cookie('viewed_articles', json.dumps(viewed_articles))
    return response


def articles(request):
    if 'page' in request.GET:
        page = int(request.GET['page'])
    else:
        page = 1
    page_size = getattr(settings, 'ARTICLES_PAGESIZE', 20)

    if request.user.has_perm('articles.add_article') or request.user.has_perm('articles.change_article'):
        articles_list = Article.objects.filter(status=Article.STATUSES.active)
    else:
        articles_list = Article.objects.filter(is_visible=True, status=Article.STATUSES.active)

    search = ''
    if 'search' in request.GET and len(request.GET['search']) > 0:
        search = request.GET['search']
        articles_list = articles_list.filter(
            Q(title__icontains=search) |
            Q(text__icontains=search) |
            Q(annotation__icontains=search)
        )

    if 'tag' in request.GET:
        articles_list = articles_list.filter(tags__name__in=[request.GET['tag']])

    if not request.user.has_perm('articles.add_article'):
        articles_list = articles_list.filter(status=Article.STATUSES.active)
    articles_list = articles_list.order_by('-published_at', '-created_at').select_related('logo')
    paginator = Paginator(articles_list, page_size)

    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    if request.is_ajax():
        template = loader.get_template('articles/articles_list.html')
        context = {
            'articles': articles
        }
        return JsonResponse({
            'success': True,
            'total': page_size,
            'message': '',
            'max_page': paginator.num_pages,
            'html': template.render(context, request)})

    template = loader.get_template('articles/pages/articles.html')
    context = {
        'articles': articles,
        'max_page': paginator.num_pages,
        'page': page,
        'search': search
    }
    if 'tag' in request.GET:
        context.update({'tag': request.GET['tag']})
    return HttpResponse(template.render(context, request))


@login_required_json
@csrf_exempt
@permission_required_json('articles.add_article')
def image_upload(request):
    message = ''
    if 'upload' not in request.FILES:
        full_path = ''
        message = 'Отсутствует файл'
        return HttpResponse('<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' +
                            request.GET['CKEditorFuncNum'] + '", "' + full_path + '", "' + message + '" );</script>')
    if request.FILES['upload'].size > settings.MAX_ARTICLE_IMAGE_SIZE:
        message = 'Максимальный размер загружаемого файла - ' + str(
            settings.MAX_ARTICLE_IMAGE_SIZE / 1024) + u' кБ'
        full_path = ''
        return HttpResponse('<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' +
                            request.GET['CKEditorFuncNum'] + '", "' + full_path + '", "' + message + '" );</script>')
    try:
        PILimage.open(request.FILES['upload'])
        photo = Image(
            original=request.FILES['upload'],
            user=request.user
        )
        photo.save()
        full_path = photo.original.url

    except IOError as e:
        message = u'Недопустимый формат файла'
        full_path = ''

    return HttpResponse('<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("' +
                        request.GET['CKEditorFuncNum'] + '", "' + full_path + '", "' + message + '" );</script>')


@login_required_json
@permission_required_json('articles.add_article')
@json_view
def logo_upload(request):
    if 'image' not in request.FILES:
        response = {
            'success': False,
            'message': 'Отсутствует файл'
        }
        return response
    if request.FILES['image'].size > settings.MAX_ARTICLE_LOGO_SIZE:
        response = {
            'success': False,
            'message': 'Максимальный размер загружаемого файла - ' + str(
                settings.MAX_ARTICLE_LOGO_SIZE / 1024) + ' кБ',
        }
        return response
    try:
        PILimage.open(request.FILES['image'])
        photo = Image(
            original=request.FILES['image'],
            user=request.user
        )
        photo.save()

        response = {
            'success': True,
            'message': 'OK',
            'id': photo.pk,
            'url': photo.original.url,
            'width': photo.original.width,
            'height': photo.original.height,
        }
        return response
    except IOError as e:
        response = {
            'success': False,
            'message': 'Недопустимый формат файла'
        }
        return response


@login_required_json
@permission_required_json('articles.add_article')
@json_view
def image_browse(request):
    photos = Image.objects.filter(status=Image.STATUSES.active, user=request.user).distinct('md5')
    photos_urls = []
    for photo in photos:
        if photo.preview:
            photos_urls.append(photo.preview.url)
        else:
            photos_urls.append(photo.original.url)
    return photos_urls
