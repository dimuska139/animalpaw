from django.conf.urls import url
from articles import views

urlpatterns = [
    url(r'^$', views.articles, name='articles.index'),
    url(r'^image/browse$', views.image_browse, name='articles.images.browse'),
    url(r'^image/upload$', views.image_upload, name='articles.images.upload'),
    url(r'^logo/upload$', views.logo_upload, name='articles.logos.upload'),
    url(r'^creation$', views.creation_page, name='articles.creation'),
    url(r'^creation/save$', views.create, name='articles.creation.save'),
    url(r'^(?P<id>\d+)/editing$', views.editing_page, name='articles.editing'),
    url(r'^(?P<id>\d+)/save$', views.edit, name='articles.editing.save'),
    url(r'^(?P<id>\d+)$', views.article, name='articles.article'),
]
