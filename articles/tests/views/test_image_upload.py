# -*- coding: utf-8 -*-
import json
import os
from django.conf import settings
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse
from extusers.models import ExtUser
from utils.models import Image


class ImageUploadTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_permissions(self):
        media_before = os.listdir(settings.MEDIA_ROOT)
        url = reverse('articles.images.upload') + '?CKEditorFuncNum=1'
        response = self.client.post(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.post(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(0, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))

    def test_image_upload(self):
        url = reverse('articles.images.upload') + '?CKEditorFuncNum=1'
        user = ExtUser.objects.first()
        # Авторизован, права есть
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        # Авторизован, права есть
        with open('./test_files/correct_img.jpg', 'rb') as file:
            media_before = os.listdir(settings.MEDIA_ROOT)
            response = self.client.post(url, {
                'upload': file
            })
            self.assertEqual(response.status_code, 200)
            self.assertContains(response, 'CKEDITOR')
            self.assertEqual(1, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))

        incorrect_files = ('python_file.py', 'php_file.php', 'bigsize_img.png', )
        for file_name in incorrect_files:
            with open('./test_files/' + file_name, 'rb') as file:
                media_before = os.listdir(settings.MEDIA_ROOT)
                response = self.client.post(url, {
                    'upload': file
                })
                self.assertEqual(response.status_code, 200)
                self.assertEqual(0, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))
