# -*- coding: utf-8 -*-
import json

import os
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.test import override_settings
from django.urls import reverse

from articles.models import Article
from extusers.models import ExtUser


class EditingTest(TestCase):

    fixtures = ['users.json', 'initial.json', 'articles.json']

    def test_permissions_page(self):
        user = ExtUser.objects.first()
        article = Article.objects.filter(status=Article.STATUSES.active).exclude(user_id=user.id).first()

        self.assertIsInstance(article, Article)
        url = reverse('articles.editing', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertIsInstance(Permission.objects.get(codename='add_article'), Permission)
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('articles.add_article'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='change_article'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('articles.change_article'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Статья чужая
        self.assertEqual(response.status_code, 200)

        article.user_id = user.id
        article.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        user.user_permissions.remove(Permission.objects.get(codename='add_article'))
        user.user_permissions.remove(Permission.objects.get(codename='change_article'))
        self.client.logout()


    def test_save(self):
        user = ExtUser.objects.get(id=1)

        article = Article.objects.filter(status=Article.STATUSES.active).exclude(user_id=user.id).first()

        self.assertIsInstance(article, Article)
        url = reverse('articles.editing.save', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.assertIsInstance(Permission.objects.get(codename='add_article'), Permission)
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('articles.add_article'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    @override_settings(DEBUG=True)
    def test_edit(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user.user_permissions.add(Permission.objects.get(codename='change_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        article = Article.objects.filter(status=Article.STATUSES.active).first()
        self.assertIsInstance(article, Article)
        article.user_id = user.id
        article.save()

        amount_before = Article.objects.count()

        url = reverse('articles.editing', args=[
            article.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        save_url = reverse('articles.editing.save', args=[
            article.id
        ])
        new_title = 'Новый заголовок'
        response = self.client.post(save_url, {
            'title': new_title,
            'annotation': article.annotation,
            'text': article.text,
            'logo': article.logo_id,
            'published_at': '2017-02-25 15:41',
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 10,
            'logo_y2': 10,
            'tags': 'тег1,tag2,test'
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('articles.index'))

        self.assertEqual(amount_before, Article.objects.count())

        user.user_permissions.remove(Permission.objects.get(codename='add_article'))
        user.user_permissions.remove(Permission.objects.get(codename='change_article'))
        self.client.logout()

        article_page_url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(article_page_url)
        self.assertContains(response, new_title, status_code=200)
