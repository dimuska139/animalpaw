# -*- coding: utf-8 -*-
import json

import os
from django.conf import settings
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import Client
from django.test import TestCase
from django.urls import reverse
from extusers.models import ExtUser
from utils.models import Image


class ImageBrowseTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_permissions(self):
        url = reverse('articles.images.browse')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_image_browse(self):
        browse_url = reverse('articles.images.browse')
        media_before = os.listdir(settings.MEDIA_ROOT)

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        last_id = Image.objects.last().id

        # Загрузка лого
        with open('./test_files/correct_img.jpg', 'rb') as file:
            url = reverse('articles.logos.upload')
            self.client.post(url, {
                'image': file
            })

        with open('./test_files/correct_img.jpg', 'rb') as file:
            url = reverse('articles.images.upload') + '?CKEditorFuncNum=1'
            self.client.post(url, {
                'upload': file
            })
        uploaded = Image.objects.filter(id__gt=last_id)

        uploaded_urls = []
        for upl in uploaded:
            if upl.preview:
                uploaded_urls.append(upl.preview.url)
            else:
                uploaded_urls.append(upl.original.url)

        self.assertEqual(2, len(uploaded_urls))

        response = self.client.get(browse_url)

        for upl in uploaded_urls:
            self.assertContains(response, upl)
        self.client.logout()

        # Другие пользователи видеть не должны
        user_another = ExtUser.objects.get(pk=2)
        c = Client()
        c.login(username=user_another.email, password='12345')
        user_another.user_permissions.add(Permission.objects.get(codename='add_article'))
        user_another = get_object_or_404(ExtUser, pk=user_another.id)

        response = c.post(browse_url)

        for upl in uploaded:
            self.assertNotContains(response, upl)
