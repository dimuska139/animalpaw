# -*- coding: utf-8 -*-
import json
from datetime import timedelta

from django.contrib.auth.models import Permission
from django.http import SimpleCookie
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from articles.models import Article
from extusers.models import ExtUser


class ArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def login(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        return user

    def test_show_publisher(self):
        article = Article.objects.first()
        article.status = Article.STATUSES.active
        article.show_publisher = True
        article.save()

        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)

        self.assertContains(response, '<div class="publisher', status_code=200)

    def test_not_show_publisher(self):
        article = Article.objects.first()
        article.status = Article.STATUSES.active
        article.show_publisher = False
        article.save()

        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)

        self.assertNotContains(response, '<div class="publisher', status_code=200)

    def test_deleted(self):
        article = Article.objects.first()
        article.status = Article.STATUSES.deleted
        article.save()
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_publish_time(self):
        # Статья, время публикации которой ещё не наступило, должна быть недоступна
        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])

        article.published_at = timezone.now() + timedelta(days=1)
        article.is_visible = False
        article.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_views(self):
        # Просмотр статьи, количество просмотров увеличивается на 1
        article = Article.objects.first()
        article_views = article.views
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        article = Article.objects.get(pk=article.pk)
        article_views_after = article.views
        self.assertEqual(1, article_views_after - article_views)

        # Повторный просмотр статьи - количество просмотров не меняется
        self.client.cookies = SimpleCookie({'viewed_articles': json.dumps([article.id])})

        views_before = article.views
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        article = Article.objects.get(pk=article.pk)
        views_after = article.views
        self.assertEqual(views_before, views_after)

    def test_views_owner(self):
        # Пользователь авторизован, смотрит свою же статью. Количество просмотров меняться не должно.
        user = self.login()
        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])
        article.user_id = user.id
        article.save()

        article_views_before = article.views

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        article = Article.objects.get(pk=article.pk)
        article_views_after = article.views
        self.assertEqual(article_views_before, article_views_after)

    def test_views_not_owner(self):
        # Если смотрит чужую статью, у нёё количество просмотров должно увеличиться на 1
        user = self.login()
        another_article = Article.objects.filter(status=Article.STATUSES.active).exclude(user_id=user.id).first()
        another_article.published_at = timezone.now() - timedelta(days=1)
        another_article.save()

        self.assertIsInstance(another_article, Article)
        another_article_views_before = another_article.views
        url = reverse('articles.article', args=[
            another_article.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        another_article = Article.objects.filter(id=another_article.pk).first()
        another_article_views_after = another_article.views
        self.assertEqual(1, another_article_views_after - another_article_views_before)

    def test_editing_link_anonimous(self):
        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertNotContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_auth(self):
        self.login()
        Article.objects.update(user_id=2)

        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertNotContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_superuser(self):
        user = ExtUser.objects.first()
        user.is_superuser = True
        user.save()

        self.login()

        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_owner(self):
        self.login()
        Article.objects.update(user_id=ExtUser.objects.first().id)

        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='change_article'))
        user.save()
        self.login()

        article = Article.objects.first()
        url = reverse('articles.article', args=[
            article.id
        ])
        response = self.client.get(url)
        self.assertContains(response, reverse('articles.editing', args=[article.id]), status_code=200)
