# -*- coding: utf-8 -*-
import json
import os
from django.conf import settings
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse
from extusers.models import ExtUser


class LogoUploadTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_permissions(self):
        media_before = os.listdir(settings.MEDIA_ROOT)
        url = reverse('articles.logos.upload')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(0, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))

    def test_upload(self):
        url = reverse('articles.logos.upload')
        user = ExtUser.objects.first()
        # Авторизован, права есть
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        with open('./test_files/correct_img.jpg', 'rb') as file:
            browse_url = reverse('articles.images.browse')

            media_before = os.listdir(settings.MEDIA_ROOT)
            response = self.client.post(url, {
                'image': file
            })
            self.assertEqual(response.status_code, 200)
            self.assertEqual(1, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))
            json_response = json.loads(response.content.decode('utf-8'))
            self.assertTrue(json_response['success'])
            self.assertTrue('id' in json_response)
            self.assertTrue('url' in json_response)
            self.assertTrue('width' in json_response)
            self.assertTrue('height' in json_response)

            img_url = json_response['url']
            response = self.client.post(browse_url)

            images = json.loads(response.content.decode('utf-8'))
            self.assertTrue(img_url in images)

        incorrect_files = ('python_file.py', 'php_file.php', 'bigsize_img.png', )
        for file_name in incorrect_files:
            with open('./test_files/' + file_name, 'rb') as file:
                media_before = os.listdir(settings.MEDIA_ROOT)
                response = self.client.post(url, {
                    'image': file
                })
                self.assertEqual(response.status_code, 200)
                self.assertEqual(0, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))
                json_response = json.loads(response.content.decode('utf-8'))
                self.assertFalse(json_response['success'])