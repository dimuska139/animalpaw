# -*- coding: utf-8 -*-

from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('articles.creation'), '/articles/creation')
        self.assertEqual(reverse('articles.creation.save'), '/articles/creation/save')
        self.assertEqual(reverse('articles.article', args=[1]), '/articles/1')
        self.assertEqual(reverse('articles.editing', args=[1]), '/articles/1/editing')
        self.assertEqual(reverse('articles.editing.save', args=[1]), '/articles/1/save')
        self.assertEqual(reverse('articles.index'), '/articles/')
        self.assertEqual(reverse('articles.images.browse'), '/articles/image/browse')
        self.assertEqual(reverse('articles.images.upload'), '/articles/image/upload')
        self.assertEqual(reverse('articles.logos.upload'), '/articles/logo/upload')
