# -*- coding: utf-8 -*-
import json
from datetime import timedelta

from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from articles.models import Article
from extusers.models import ExtUser
from utils.models import Image


class CreationTest(TestCase):
    fixtures = ['users.json', 'initial.json', 'articles.json']

    def test_page(self):
        url = reverse('articles.creation')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('articles.add_article'))
        response = self.client.get(url)
        self.assertContains(response, reverse('articles.creation.save'), status_code=200)

    def test_action(self):
        url = reverse('articles.creation.save')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('articles.add_article'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_creation(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        url = reverse('articles.creation.save')
        amount_before = Article.objects.count()

        response = self.client.post(url, {
            'title': 'TEST_ARTICLE',
            'annotation': 'Краткое содержание',
            'text': '[b]Текст тестовой заметки[/b]',
            'published_at': '2017-02-25 15:41',
            'logo': Image.objects.first().pk,
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 100,
            'logo_y2': 100,
            'tags': 'тег1,tag2, test'
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('articles.index'))

        self.assertNotEqual(amount_before, Article.objects.count())

        new_article = Article.objects.order_by('-id').first()
        self.assertEqual(new_article.user_id, user.id)

        self.assertEqual(3, new_article.tags.count())

        article_page_url = reverse('articles.article', args=[
            new_article.id
        ])
        response = self.client.get(article_page_url)
        self.assertContains(response, 'TEST_ARTICLE', status_code=200)

        # Должна присутствовать в sitemap.xml
        sitemap_url = reverse('django.contrib.sitemaps.views.sitemap')
        response = self.client.get(sitemap_url)
        self.assertContains(response, article_page_url, status_code=200)

    def test_creation_tomorrow(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        user = get_object_or_404(ExtUser, pk=user.id)

        url = reverse('articles.creation.save')
        amount_before = Article.objects.count()

        tomorrow = timezone.now() + timedelta(days=1)
        response = self.client.post(url, {
            'title': 'TEST_ARTICLE',
            'annotation': 'Краткое содержание',
            'text': '[b]Текст тестовой заметки[/b]',
            'published_at': tomorrow.strftime("%Y-%m-%d %H:%M"),
            'logo': Image.objects.first().pk,
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 100,
            'logo_y2': 100,
            'tags': 'тег1,tag2, test'
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('articles.index'))

        self.assertNotEqual(amount_before, Article.objects.count())

        new_article = Article.objects.order_by('-id').first()
        self.assertFalse(new_article.is_visible)
        self.assertEqual(new_article.user_id, user.id)

        self.assertEqual(3, new_article.tags.count())

        article_page_url = reverse('articles.article', args=[
            new_article.id
        ])
        self.client.logout()
        response = self.client.get(article_page_url)
        self.assertEqual(response.status_code, 404)

        self.client.login(username=user.email, password='12345')
        response = self.client.get(article_page_url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'TEST_ARTICLE', status_code=200)
        self.assertContains(response, 'будет опубликована', status_code=200)

        # Должна отсутствовать в sitemap.xml
        sitemap_url = reverse('django.contrib.sitemaps.views.sitemap')
        response = self.client.get(sitemap_url)
        self.assertNotContains(response, article_page_url, status_code=200)
