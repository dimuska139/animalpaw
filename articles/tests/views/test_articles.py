# -*- coding: utf-8 -*-
from datetime import timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from articles.models import Article
from extusers.models import ExtUser


class ArticlesTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_page(self):
        test_article_title = 'TEST_ARTICLE'

        article = Article.objects.first()
        self.assertIsInstance(article, Article)
        articles_url = reverse('articles.index')
        article.title = test_article_title
        article.published_at = timezone.now()-timedelta(days=1)
        article.status = Article.STATUSES.active
        article.save()

        articles_page = self.client.get(articles_url)

        self.assertContains(articles_page, test_article_title, status_code=200)

        article.published_at = timezone.now() + timedelta(days=1)
        article.is_visible = False
        article.save()

        articles_page = self.client.get(articles_url)
        self.assertNotContains(articles_page, test_article_title, status_code=200)

    def test_search(self):
        search_text = '_SEARCH_ARTICLE'
        article = Article.objects.first()
        self.assertIsInstance(article, Article)
        articles_url = reverse('articles.index') + '?search=' + search_text

        # Время публикации статьи уже наступило, а её статус = 1 - статья отображается в поиске
        article.title = 'TEST_SEARCH_ARTICLE'
        article.published_at = timezone.now() - timedelta(days=1)
        article.status = Article.STATUSES.active
        article.save()

        articles_page = self.client.get(articles_url)

        self.assertContains(articles_page, 'TEST_SEARCH_ARTICLE', status_code=200)

        # Ajax
        articles_page = self.client.get(articles_url, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertContains(articles_page, 'TEST_SEARCH_ARTICLE', status_code=200)

        # Если у статьи статус = 0, то она не должна появляться в поиске
        article.status = Article.STATUSES.deleted
        article.save()
        articles_page = self.client.get(articles_url)
        self.assertNotContains(articles_page, 'TEST_SEARCH_ARTICLE', status_code=200)

        # Если время публикации статьи ещё не наступило, то она не должна отображаться в поиске
        article.status = Article.STATUSES.active
        article.published_at = timezone.now() + timedelta(days=1)
        article.is_visible = False
        article.save()
        articles_page = self.client.get(articles_url)
        self.assertNotContains(articles_page, 'TEST_SEARCH_ARTICLE', status_code=200)

    def test_editing_link_anonimous(self):
        url = reverse('articles.index')
        response = self.client.get(url)

        article = Article.objects.last()
        self.assertNotContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_auth(self):
        self.login()
        Article.objects.update(user_id=2)

        url = reverse('articles.index')
        response = self.client.get(url)

        article = Article.objects.last()
        self.assertNotContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_superuser(self):
        user = ExtUser.objects.first()
        user.is_superuser = True
        user.save()

        self.login()

        url = reverse('articles.index')
        response = self.client.get(url)

        article = Article.objects.last()
        self.assertContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_owner(self):
        self.login()
        Article.objects.update(user_id=ExtUser.objects.first().id)

        url = reverse('articles.index')
        response = self.client.get(url)

        article = Article.objects.last()
        self.assertNotContains(response, reverse('articles.editing', args=[article.id]), status_code=200)

    def test_editing_link_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='change_article'))
        user.save()
        self.login()

        url = reverse('articles.index')
        response = self.client.get(url)

        article = Article.objects.last()
        self.assertContains(response, reverse('articles.editing', args=[article.id]), status_code=200)
