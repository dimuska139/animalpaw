# coding: utf-8
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class ArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def setUp(self):
        self.url = reverse('articles.index')

    def test_anon(self):
        response = self.client.get(self.url)
        self.assertNotContains(response, 'Написать статью', status_code=200)

    def test_auth(self):
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)
        self.assertNotContains(response, 'Написать статью', status_code=200)

    def test_superuser(self):
        user = ExtUser.objects.get(pk=1)
        user.is_superuser = True
        user.save()

        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)

        self.assertContains(response, 'Написать статью', status_code=200)

    def test_with_permissions(self):
        user = ExtUser.objects.get(pk=1)
        user.user_permissions.add(Permission.objects.get(codename='add_article'))
        self.client.login(username=user.email, password='12345')

        response = self.client.get(self.url)

        self.assertContains(response, 'Написать статью', status_code=200)




