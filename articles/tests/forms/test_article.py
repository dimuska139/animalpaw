# coding: utf-8
import warnings
from django.test import TestCase
from articles.forms import ArticleForm
from articles.models import Article
from utils.models import Image

warnings.filterwarnings(
    'ignore', r"DateTimeField .* received a naive datetime",
    RuntimeWarning, r'django\.db\.models\.fields')


class ArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_valid_form(self):
        data = {
            'title': 'Заголовок',
            'annotation': 'Аннотация',
            'text': 'Тест статьи',
            'tags': 'Тест, статья',
            'published_at': '2017-02-25 15:41',
            'logo': Image.objects.first().pk
        }
        form = ArticleForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'title': '',
            'annotation': 'Аннотация',
            'text': 'Тест статьи',
            'tags': 'Тест, статья',
            'published_at': '2017-02-25 15:41',
            'logo': Image.objects.first().pk
        }
        form = ArticleForm(data=data)
        self.assertFalse(form.is_valid())

        data = {
            'title': 'Заголовок',
            'annotation': 'Аннотация',
            'text': 'Тест статьи',
            'tags': 'Тест, статья',
            'published_at': '',
            'logo': Image.objects.first().pk
        }
        form = ArticleForm(data=data)
        self.assertFalse(form.is_valid())
