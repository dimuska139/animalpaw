# coding: utf-8

from django.test import TestCase

from articles.models import Article
from extusers.models import ExtUser


class ArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_comment(self):
        article = Article.objects.first()
        comments_before = article.comments.count()
        article.comments.create(text='Комментарий', user=ExtUser.objects.first())
        comments_after = article.comments.count()
        self.assertEqual(1, comments_after - comments_before)
