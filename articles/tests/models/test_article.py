# coding: utf-8
import warnings

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from articles.models import Article
from extusers.models import ExtUser

warnings.filterwarnings(
    'ignore', r"DateTimeField .* received a naive datetime",
    RuntimeWarning, r'django\.db\.models\.fields')


class ArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation(self):
        article = Article.objects.create(title='Animal title',
                                         annotation='Animals',
                                         text='Article about animals',
                                         show_publisher=True,
                                         published_at=timezone.now(),
                                         user=ExtUser.objects.first())

        self.assertTrue(isinstance(article, Article))
        self.assertEqual(article.__str__(), str(article.id) + ' ' + article.title)
        self.assertEqual(article.status, Article.STATUSES.active)
        self.assertTrue(article.show_publisher)
        self.assertEqual(article.views, 0)

    def test_absolute_url(self):
        article = Article.objects.create(title='Animal title',
                                         annotation='Animals',
                                         text='Article about animals',
                                         published_at=timezone.now(),
                                         user=ExtUser.objects.first())
        self.assertEqual(reverse('articles.article', args=[article.id]), article.get_absolute_url())

    def test_similar(self):
        article_1 = Article.objects.create(title='Animal title',
                                         annotation='Animals',
                                         text='Article about animals',
                                         published_at=timezone.now(),
                                         user=ExtUser.objects.first())
        article_1.tags.add("blue")

        article_2 = Article.objects.create(title='Animal title',
                                           annotation='Animals',
                                           text='Article about animals',
                                           published_at=timezone.now(),
                                           user=ExtUser.objects.first())
        article_2.tags.add("red", "green", "blue")

        article_3 = Article.objects.create(title='Animal title',
                                           annotation='Animals',
                                           text='Article about animals',
                                           published_at=timezone.now(),
                                           user=ExtUser.objects.first())
        article_3.tags.add("red", "blue", "white")

        article_4 = Article.objects.create(title='Animal title',
                                           annotation='Animals',
                                           text='Article about animals',
                                           published_at=timezone.now(),
                                           user=ExtUser.objects.first())
        article_4.tags.add("unknown")

        article_5 = Article.objects.create(title='Animal title',
                                           annotation='Animals',
                                           text='Article about animals',
                                           published_at=timezone.now(),
                                           user=ExtUser.objects.first())
        article_5.tags.add("white")

        self.assertEqual(2, len(article_1.get_similar()))
        self.assertEqual(2, len(article_2.get_similar()))
        self.assertEqual(3, len(article_3.get_similar()))
        self.assertEqual(0, len(article_4.get_similar()))

        self.assertEqual(article_3.get_similar()[0]['article'].id, article_2.id)
        self.assertEqual(article_3.get_similar()[0]['tag_hits'], 2)

        self.assertEqual(article_2.get_similar()[0]['article'].id, article_3.id)
        self.assertEqual(article_2.get_similar()[0]['tag_hits'], 2)

        self.assertEqual(article_2.get_similar()[1]['article'].id, article_1.id)
        self.assertEqual(article_2.get_similar()[1]['tag_hits'], 1)

