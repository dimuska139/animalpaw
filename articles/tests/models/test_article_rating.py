# coding: utf-8

from django.test import TestCase

from articles.models import Article
from extusers.models import ExtUser


class ArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'articles.json']

    def test_rating(self):
        article = Article.objects.first()
        article.rating.create(value=3)
        article.rating.create(value=3)
        article.rating.create(value=3)
        rating = article.score()
        self.assertEqual(3, rating)
        self.assertEqual(3, article.reviews_num())
