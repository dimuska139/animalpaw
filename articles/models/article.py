import json

import re
import requests
import vk
import time
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models, connection
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from model_utils import Choices
from taggit.managers import TaggableManager
from django.conf import settings
from django.utils import timezone
from vk.exceptions import VkException
from django.db.models import Sum

from comments.models import Comment
from django.contrib.sites.models import Site

from utils.models.rating import Rating


class Article(models.Model):
    title = models.CharField(max_length=100)
    annotation = models.TextField(max_length=400, null=True)
    text = models.TextField()
    views = models.IntegerField(default=0, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    published_at = models.DateTimeField(null=True)
    user = models.ForeignKey('extusers.ExtUser', on_delete=models.SET_NULL, null=True)
    logo = models.ForeignKey('utils.Image', on_delete=models.SET_NULL, null=True)
    show_publisher = models.BooleanField('Показывать автора', default=False)
    to_vk = models.BooleanField('Публиковать в группе VK', default=False)
    vk_post_id = models.IntegerField(blank=True, null=True)
    is_visible = models.BooleanField('Доступна пользователям', default=True)
    comments = GenericRelation(Comment, related_query_name='articles')
    rating = GenericRelation(Rating, related_query_name='articles')
    tags = TaggableManager(blank=True)

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def score(self):
        rating_amount = self.rating.filter(status=Rating.STATUSES.active).count()
        rating_sum = self.rating.aggregate(Sum('value'))['value__sum']
        if rating_amount == 0:
            return 0.0
        return rating_sum / rating_amount

    def reviews_num(self):
        return self.rating.filter(status=Rating.STATUSES.active).count()

    def get_absolute_url(self):
        return reverse('articles.article', args=[
            self.id
        ])

    def get_content_type(self):
        return ContentType.objects.get_for_model(self).id

    @property
    def in_queue(self):
        return timezone.now() < self.published_at

    def vk_sync(self):
        if settings.DEBUG:
            return
        session = vk.Session(access_token=settings.VK_ADMIN_ACCESS_TOKEN)
        api = vk.API(session)

        owner_id = '-' + str(settings.VK_GROUP_ID)
        post_id = owner_id + '_' + str(self.vk_post_id)

        tags_list = ['#animalpaw']
        for tag in self.tags.names():
            tag = tag.replace(' ', '')  # Удалить пробелы
            tags_list.append('#' + tag.lower())

        current_site = Site.objects.get(pk=1)
        line_separator = '\n\n'
        message_parts = [
            self.title,
            self.annotation,
            ' '.join(tags_list),
            'Подробнее в источнике: https://' + current_site.domain + self.get_absolute_url()
        ]
        message = line_separator.join(message_parts)

        if not self.to_vk and self.vk_post_id is not None:
            if len(api.wall.getById(posts=post_id)) > 0:
                api.wall.delete(owner_id=owner_id, post_id=self.vk_post_id)
                self.vk_post_id = None
                self.save()
        else:
            result = api.photos.getWallUploadServer(gid=settings.VK_GROUP_ID)
            upload_url = result['upload_url']

            img = {
                'photo': (
                    'img.png', open(
                        self.logo.original.path, 'rb'
                    )
                )
            }
            response = requests.post(upload_url, files=img)
            upload_result = json.loads(response.text)
            result = api.photos.saveWallPhoto(photo=upload_result['photo'], hash=upload_result['hash'], server=upload_result['server'], gid=settings.VK_GROUP_ID)
            photo_id = result[0]['id']

            if len(api.wall.getById(posts=post_id)) > 0 and self.vk_post_id is not None:
                try:
                    api.wall.edit(owner_id=owner_id, post_id=self.vk_post_id, message=message,
                                  from_group=1,
                                  friends_only=0, attachments=photo_id)
                except VkException:  # Если не удалось отредактировать - не делаем ничего
                    pass
                #    api.wall.delete(owner_id=owner_id, post_id=self.vk_post_id)
                #    result = api.wall.post(owner_id=owner_id, message=message, from_group=1,
                #                           friends_only=0, attachments=photo_id)
                #    self.vk_post_id = result['post_id']
                #    self.save()
            else:
                result = api.wall.post(owner_id=owner_id, message=message, from_group=1,
                                       friends_only=0, attachments=photo_id)
                self.vk_post_id = result['post_id']
                self.save()

    def get_similar(self):
        tags_list = []
        for tag in self.tags.names():
            tags_list.append(tag)

        # Другие статьи, содержащие хотя бы один тег текущей - похожие
        articles_contains_tags = Article.objects.filter(Q(published_at__lte=timezone.now()), tags__name__in=tags_list,
                                                        status=Article.STATUSES.active).exclude(
            id=self.id).select_related('logo').distinct()

        # id похожих статей
        a_ids = []
        for a in articles_contains_tags:
            a_ids.append(str(a.id))

        similar_articles = []
        if len(a_ids) > 0:
            ctype = ContentType.objects.get(model='article')

            cursor = connection.cursor()
            cursor.execute("SELECT a.id, t_t.name FROM articles_article a\
                    LEFT JOIN taggit_taggeditem t_i\
                        ON a.id = t_i.object_id\
                    LEFT JOIN taggit_tag t_t\
                        ON t_i.tag_id = t_t.id\
                    WHERE t_i.content_type_id = %s AND a.id IN (" + ','.join(a_ids) + ")", [ctype.id])
            results = cursor.fetchall()
            articles_tags = {}
            for result in results:
                if not int(result[0]) in articles_tags:
                    articles_tags[int(result[0])] = [
                        result[1]
                    ]
                else:
                    articles_tags[int(result[0])].append(result[1])

            # Подсчёт количества совпавших тегов
            for article in articles_contains_tags:
                tags_hits = 0
                for tag in tags_list:
                    tags_names = articles_tags[article.id]
                    if tag in tags_names:
                        tags_hits += 1
                similar_articles.append({
                    'article': article,
                    'tag_hits': tags_hits
                })
                # Сортировка по убыванию количества совпавших тегов
                similar_articles = sorted(similar_articles, key=lambda x: x['tag_hits'], reverse=True)
        return similar_articles

    def __str__(self):
        return str(self.id) + ' ' + str(self.title)
