'''from django import template
from articles.models import Article

register = template.Library()


@register.inclusion_tag("last_articles.html")
def last_items():
    items = Article.objects.filter(status=1).order_by('-published_at', '-created_at')[0:10]
    return {
        'items': items
    }'''
