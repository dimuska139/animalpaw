from django.contrib import messages
from django.conf import settings
from django.contrib.auth.decorators import permission_required, login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.template import loader
from django.urls import reverse
from django.utils import timezone
from django.views.decorators.csrf import csrf_protect
from jsonview.decorators import json_view

from extusers.decorators import login_required_json
from extusers.decorators.permission_required import permission_required_json
from jokes.forms import JokeForm
from jokes.models import Joke
from utils.forms import get_errors


def jokes(request):
    page = 1
    if 'page' in request.GET and len(request.GET['page']) > 0:
        page = int(request.GET['page'])

    search = ''
    if 'search' in request.GET and len(request.GET['search']) > 0:
        search = request.GET['search']

    show_censored = 0

    if 'show_censored' in request.COOKIES:
        show_censored = int(request.COOKIES.get('show_censored'))

    if 'show_censored' in request.GET:
        show_censored = int(request.GET['show_censored'])

    if request.user.has_perm('jokes.add_joke') or request.user.has_perm('jokes.change_joke'):
        jokes = Joke.objects.filter(status=Joke.STATUSES.active)
    else:
        jokes = Joke.objects.filter(Q(published_at__lte=timezone.now()), status=Joke.STATUSES.active)

    if show_censored == 0:
        jokes = jokes.filter(is_censored=False)

    jokes = jokes.order_by('-published_at').prefetch_related('source')

    jokes = jokes.filter(
        Q(text__icontains=search)
    )

    paginator = Paginator(jokes, getattr(settings, 'JOKES_PAGESIZE'))
    try:
        jokes = paginator.page(page)
    except PageNotAnInteger:
        jokes = paginator.page(1)
    except EmptyPage:
        jokes = paginator.page(paginator.num_pages)
    max_page = paginator.num_pages

    if request.is_ajax():
        template = loader.get_template('jokes/jokes_list.html')
        context = {
            'jokes': jokes
        }
        return JsonResponse({
            'success': True,
            'max_page': max_page,
            'message': '',
            'html': template.render(context, request)})

    template = loader.get_template('jokes/pages/jokes.html')

    context = {
        'jokes': jokes,
        'max_page': max_page,
        'search': search,
        'show_censored': show_censored
    }

    response = HttpResponse(template.render(context, request))
    return response


def joke(request, id):
    if request.user.has_perm('jokes.add_joke') or request.user.has_perm('jokes.change_joke'):
        joke = Joke.objects.filter(status=Joke.STATUSES.active, id=id).first()
    else:
        joke = Joke.objects.filter(Q(published_at__lte=timezone.now()), status=Joke.STATUSES.active, id=id).first()
    if not isinstance(joke, Joke):
        raise Http404
    template = loader.get_template('jokes/pages/joke.html')
    context = {
        'joke': joke
    }
    return HttpResponse(template.render(context, request))


@json_view
@permission_required_json('jokes.delete_joke')
def delete(request, id):
    joke = Joke.objects.get(id=id)
    joke.status = Joke.STATUSES.deleted
    joke.save()
    return {
        'success': True
    }


@json_view
@permission_required_json('jokes.change_joke')
def censor(request, id):
    joke = Joke.objects.get(id=id)
    joke.is_censored = True
    joke.save()
    return {
        'success': True
    }


@login_required
@permission_required('jokes.add_joke', raise_exception=True)
def creation_page(request):
    form = JokeForm(initial={
        'text': '',
        'published_at': '',
        'source_url': '',
        'is_censored': False
    })
    template = loader.get_template('jokes/pages/joke_creation.html')
    context = {
        'joke_form': form
    }
    return HttpResponse(template.render(context, request))


@json_view
@login_required_json
@permission_required_json('jokes.add_joke')
def create(request):
    form = JokeForm(request.POST)
    if not form.is_valid():
        return {
            'success': False,
            'errors': get_errors(form)
        }

    joke = form.save()
    joke.user_id = request.user.id
    joke.save()

    template = loader.get_template('jokes/messages/joke_creation_success.html')
    if joke.published_at > timezone.now():
        is_active = False
    else:
        is_active = True
    context = {
        'joke': joke,
        'is_active': is_active
    }
    messages.info(request, template.render(context, request))
    return {
        'success': True,
        'url': reverse('jokes.index')
    }


@login_required
@permission_required('jokes.change_joke', raise_exception=True)
def editing_page(request, id):
    joke = get_object_or_404(Joke, id=id, status=Joke.STATUSES.active)
    form = JokeForm(instance=joke)

    template = loader.get_template('jokes/pages/joke_editing.html')
    context = {
        'joke_form': form,
        'joke_id': joke.id
    }
    return HttpResponse(template.render(context, request))


@json_view
@login_required_json
@permission_required_json('jokes.change_joke')
def edit(request, id):
    joke = Joke.objects.filter(id=id, status=Joke.STATUSES.active).first()

    if not isinstance(joke, Joke):
        return {
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Анекдот не найден'
                }
            ]
        }

    form = JokeForm(request.POST, instance=joke)
    if not form.is_valid():
        return {
            'success': False,
            'errors': get_errors(form)
        }

    form.save()

    template = loader.get_template('jokes/messages/joke_editing_success.html')
    if joke.published_at > timezone.now():
        is_active = False
    else:
        is_active = True
    context = {
        'joke': joke,
        'is_active': is_active
    }
    messages.info(request, template.render(context, request))
    return {
        'success': True,
        'url': reverse('jokes.index')
    }
