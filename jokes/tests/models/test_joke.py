from django.test import TestCase

from jokes.models import Joke
from jokes.models import Source


class JokeTest(TestCase):
    fixtures = ['jokes.json']

    def test_creation(self):
        joke = Joke.objects.create(text='Текст анекдота', source=Source.objects.get(pk=1), is_censored=False)
        self.assertTrue(isinstance(joke, Joke))
        self.assertEqual(joke.__str__(), str(joke.id) + ' ' + joke.text[0:50] + '...')
        self.assertEqual(joke.status, joke.STATUSES.active)

    def test_manual_creation(self):
        joke = Joke.objects.create(text='Текст анекдота', source_url='http://yandex.ru', is_censored=True)
        self.assertTrue(isinstance(joke, Joke))
        self.assertEqual(joke.__str__(), str(joke.id) + ' ' + joke.text[0:50] + '...')
        self.assertEqual(joke.status, joke.STATUSES.active)


