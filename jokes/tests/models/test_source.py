from django.test import TestCase

from jokes.models import Source


class SourceTest(TestCase):
    def test_creation(self):
        source = Source.objects.create(url='http://yandex.ru', base_url='http://yandex.ru', alias='yandex')
        self.assertTrue(isinstance(source, Source))
        self.assertEqual(source.__str__(), source.url)
        self.assertEqual(source.status, Source.STATUSES.active)
