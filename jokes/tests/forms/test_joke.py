import warnings
from django.test import TestCase
from jokes.forms import JokeForm

warnings.filterwarnings(
    'ignore', r"DateTimeField .* received a naive datetime",
    RuntimeWarning, r'django\.db\.models\.fields')


class JokeTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'jokes.json']

    def test_valid_form(self):
        data = {
            'text': 'Смешной анекдот',
            'published_at': '2017-02-25 15:41',
            'source_url': 'http://animalpaw.ru'
        }
        form = JokeForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'text': '',
            'published_at': '2017-02-25 15:41',
            'source_url': 'http://animalpaw.ru'
        }
        form = JokeForm(data=data)
        self.assertFalse(form.is_valid())

        data = {
            'text': 'Смешной анекдот',
            'published_at': '',
            'source_url': 'http://animalpaw.ru'
        }
        form = JokeForm(data=data)
        self.assertFalse(form.is_valid())
