from datetime import timedelta

from django.db.models import Q
from django.utils import timezone
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from jokes.models import Joke
from django.contrib.auth.models import Permission


class JokesTest(TestCase):
    fixtures = ['users.json', 'jokes.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_permissions(self):
        joke = Joke.objects.filter(Q(published_at__lte=timezone.now()), status=Joke.STATUSES.active).first()
        url = reverse('jokes.joke', args=[joke.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.login()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_not_exists(self):
        joke = Joke.objects.filter(Q(published_at__lte=timezone.now()), status=Joke.STATUSES.active).order_by('-id').first()
        url = reverse('jokes.joke', args=[joke.id+1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_deleted(self):
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        joke.status = Joke.STATUSES.deleted
        joke.save()

        url = reverse('jokes.joke', args=[joke.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_publish_date(self):
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        joke.published_at = timezone.now() + timedelta(days=1)
        joke.save()

        url = reverse('jokes.joke', args=[joke.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_publish_date_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user.save()
        self.login()

        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        joke.published_at = timezone.now() + timedelta(days=1)
        joke.save()

        url = reverse('jokes.joke', args=[joke.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
