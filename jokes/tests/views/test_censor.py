import json

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from jokes.models import Joke


class CensorTest(TestCase):
    fixtures = ['users.json', 'jokes.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_anonimous(self):
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        url = reverse('jokes.censor', args=[joke.id])
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        url = reverse('jokes.index')
        response = self.client.get(url)
        self.assertNotContains(response, 'censor-joke', status_code=200)
        self.assertNotContains(response, 'options-warning', status_code=200)

    def test_auth(self):
        self.login()
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        url = reverse('jokes.censor', args=[joke.id])
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        url = reverse('jokes.index')
        response = self.client.get(url)
        self.assertNotContains(response, 'censor-joke', status_code=200)
        self.assertNotContains(response, 'options-warning', status_code=200)

    def test_super_admin(self):
        user = ExtUser.objects.first()
        user.is_superuser = True
        user.save()

        self.login()
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        url = reverse('jokes.censor', args=[joke.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        self.assertTrue(Joke.objects.get(id=joke.id).is_censored)

        url = reverse('jokes.index')
        response = self.client.get(url)
        self.assertContains(response, 'censor-joke', status_code=200)
        self.assertContains(response, 'options-warning', status_code=200)

    def test_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='change_joke'))
        user.save()

        self.login()
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        url = reverse('jokes.censor', args=[joke.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        self.assertTrue(Joke.objects.get(id=joke.id).is_censored)

        url = reverse('jokes.index')
        response = self.client.get(url)
        self.assertContains(response, 'censor-joke', status_code=200)
        self.assertContains(response, 'options-warning', status_code=200)

