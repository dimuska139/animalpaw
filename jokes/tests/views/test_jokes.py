from datetime import timedelta
from django.utils import timezone
from django.contrib.auth.models import Permission
from django.http import SimpleCookie
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from jokes.models import Joke


class JokesTest(TestCase):
    fixtures = ['users.json', 'jokes.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_permissions(self):
        url = reverse('jokes.index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.login()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_censored(self):
        Joke.objects.update(is_censored=True)
        url = reverse('jokes.index')
        response = self.client.get(url)
        self.assertNotContains(response, '<div class="joke row">', status_code=200)

        url = reverse('jokes.index') + '?show_censored=1'
        response = self.client.get(url)
        self.assertContains(response, '<div class="joke row">', status_code=200)

    def test_publish_date(self):
        url = reverse('jokes.index')
        joke = Joke.objects.filter(status=Joke.STATUSES.active).last()
        response = self.client.get(url)
        self.assertContains(response, joke.text, status_code=200)

        joke.published_at = timezone.now() + timedelta(days=1)
        joke.save()

        response = self.client.get(url)
        self.assertNotContains(response, joke.text, status_code=200)

    def test_publish_date_has_permission(self):
        url = reverse('jokes.index')
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user.save()
        self.login()

        joke = Joke.objects.filter(status=Joke.STATUSES.active).last()
        joke.published_at = timezone.now() + timedelta(days=1)
        joke.save()

        response = self.client.get(url)
        self.assertContains(response, joke.text, status_code=200)


    def test_search(self):
        joke_text = 'NEW_JOKE'

        joke = Joke.objects.first()
        joke.text = joke_text
        joke.is_censored = True
        joke.save()

        url = reverse('jokes.index') + '?search=' + joke_text + '&show_censored=1'
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, joke_text, 2)  # В поле поиска и в списке
        self.assertContains(response, '<div class="joke row">', 1)

        self.client.cookies = SimpleCookie({'show_censored': 0})
        url = reverse('jokes.index') + '?search=' + joke_text
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, joke_text, 1)  # В поле поиска
        self.assertNotContains(response, '<div class="joke row">')

    def test_editing_link_anonimous(self):
        url = reverse('jokes.index')
        response = self.client.get(url)
        joke = Joke.objects.last()
        self.assertNotContains(response, reverse('jokes.editing', args=[joke.id]), status_code=200)

    def test_editing_link_auth(self):
        self.login()
        url = reverse('jokes.index')
        response = self.client.get(url)
        joke = Joke.objects.last()
        self.assertNotContains(response, reverse('jokes.editing', args=[joke.id]), status_code=200)

    def test_editing_link_superuser(self):
        user = ExtUser.objects.first()
        user.is_superuser = True
        user.save()

        self.login()

        url = reverse('jokes.index')
        response = self.client.get(url)

        joke = Joke.objects.last()
        self.assertContains(response, reverse('jokes.editing', args=[joke.id]), status_code=200)

    def test_editing_link_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='change_joke'))
        user.save()
        self.login()

        url = reverse('jokes.index')
        response = self.client.get(url)

        joke = Joke.objects.last()
        self.assertContains(response, reverse('jokes.editing', args=[joke.id]), status_code=200)

    def test_delete_link_anonimous(self):
        url = reverse('jokes.index')
        response = self.client.get(url)
        joke = Joke.objects.last()
        self.assertNotContains(response, reverse('jokes.delete', args=[joke.id]), status_code=200)

    def test_delete_link_auth(self):
        self.login()
        url = reverse('jokes.index')
        response = self.client.get(url)
        joke = Joke.objects.last()
        self.assertNotContains(response, reverse('jokes.delete', args=[joke.id]), status_code=200)

    def test_delete_link_superuser(self):
        user = ExtUser.objects.first()
        user.is_superuser = True
        user.save()

        self.login()

        url = reverse('jokes.index')
        response = self.client.get(url)

        joke = Joke.objects.last()
        self.assertContains(response, reverse('jokes.delete', args=[joke.id]), status_code=200)

    def test_delete_link_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='delete_joke'))
        user.save()
        self.login()

        url = reverse('jokes.index')
        response = self.client.get(url)

        joke = Joke.objects.last()
        self.assertContains(response, reverse('jokes.delete', args=[joke.id]), status_code=200)

    def test_censor_link_anonimous(self):
        url = reverse('jokes.index')
        response = self.client.get(url)
        joke = Joke.objects.last()
        self.assertNotContains(response, reverse('jokes.censor', args=[joke.id]), status_code=200)

    def test_censor_link_auth(self):
        self.login()
        url = reverse('jokes.index')
        response = self.client.get(url)
        joke = Joke.objects.last()
        self.assertNotContains(response, reverse('jokes.censor', args=[joke.id]), status_code=200)

    def test_censor_link_superuser(self):
        user = ExtUser.objects.first()
        user.is_superuser = True
        user.save()

        self.login()

        url = reverse('jokes.index')
        response = self.client.get(url)

        joke = Joke.objects.last()
        self.assertContains(response, reverse('jokes.censor', args=[joke.id]), status_code=200)

    def test_censor_link_has_permission(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='change_joke'))
        user.save()
        self.login()

        url = reverse('jokes.index')
        response = self.client.get(url)

        joke = Joke.objects.last()
        self.assertContains(response, reverse('jokes.censor', args=[joke.id]), status_code=200)




