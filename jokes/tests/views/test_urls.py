from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('jokes.index'), '/jokes/')
        self.assertEqual(reverse('jokes.delete', args=[1]), '/jokes/1/delete')
        self.assertEqual(reverse('jokes.censor', args=[1]), '/jokes/1/censor')
        self.assertEqual(reverse('jokes.creation.save'), '/jokes/creation/save')
        self.assertEqual(reverse('jokes.editing', args=[1]), '/jokes/1/editing')
        self.assertEqual(reverse('jokes.editing.save', args=[1]), '/jokes/1/save')
