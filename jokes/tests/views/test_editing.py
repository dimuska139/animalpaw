# -*- coding: utf-8 -*-
import json

from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse

from jokes.models import Joke
from extusers.models import ExtUser


class EditingTest(TestCase):
    fixtures = ['users.json', 'jokes.json']

    def test_permissions_page(self):
        user = ExtUser.objects.first()
        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        joke.user_id = 2
        joke.save()

        self.assertIsInstance(joke, Joke)
        url = reverse('jokes.editing', args=[
            joke.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertIsInstance(Permission.objects.get(codename='add_joke'), Permission)
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('jokes.add_joke'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='change_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('jokes.change_joke'))
        response = self.client.get(url)
        # Чужой анекдот
        self.assertEqual(response.status_code, 200)

        # Снова сделали своим
        joke.user_id = user.id
        joke.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        user.user_permissions.remove(Permission.objects.get(codename='add_joke'))
        user.user_permissions.remove(Permission.objects.get(codename='change_joke'))
        self.client.logout()

    def test_save(self):
        user = ExtUser.objects.get(id=1)

        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        joke.user_id = 1
        joke.save()

        self.assertIsInstance(joke, Joke)
        url = reverse('jokes.editing.save', args=[
            joke.id
        ])
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.assertIsInstance(Permission.objects.get(codename='add_joke'), Permission)
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('jokes.add_joke'))
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.remove(Permission.objects.get(codename='add_joke'))
        user.user_permissions.remove(Permission.objects.get(codename='change_joke'))
        self.client.logout()

    def test_edit(self):
        user = ExtUser.objects.first()
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user.user_permissions.add(Permission.objects.get(codename='change_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.client.login(username=user.email, password='12345')

        joke = Joke.objects.filter(status=Joke.STATUSES.active).first()
        self.assertIsInstance(joke, Joke)

        amount_before = Joke.objects.count()

        url = reverse('jokes.editing', args=[
            joke.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        save_url = reverse('jokes.editing.save', args=[
            joke.id
        ])
        new_text = 'Новый анекдот'
        response = self.client.post(save_url, {
            'text': new_text,
            'published_at': '2017-02-25 15:41',
            'source_url': 'yandex.ru',
            'is_censored': True
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('jokes.index'))

        self.assertEqual(amount_before, Joke.objects.count())

        user.user_permissions.remove(Permission.objects.get(codename='add_joke'))
        user.user_permissions.remove(Permission.objects.get(codename='change_joke'))
        self.client.logout()

        response = self.client.get(reverse('jokes.index') + '?show_censored=1')
        self.assertContains(response, new_text, status_code=200)
