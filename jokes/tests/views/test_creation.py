# -*- coding: utf-8 -*-
import json
from datetime import timedelta

from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from jokes.models import Joke
from extusers.models import ExtUser


class CreationTest(TestCase):
    fixtures = ['users.json', 'jokes.json']

    def test_page(self):
        url = reverse('jokes.creation')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('jokes.add_joke'))
        response = self.client.get(url)
        self.assertContains(response, reverse('jokes.creation.save'), status_code=200)

    def test_action(self):
        url = reverse('jokes.creation.save')
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('jokes.add_joke'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_creation(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)

        url = reverse('jokes.creation.save')
        amount_before = Joke.objects.count()

        source_url = 'http://test-source-test.ru'
        joke_text = 'TEST_JOKE'
        response = self.client.post(url, {
            'text': joke_text,
            'published_at': timezone.now().strftime("%Y-%m-%d %H:%M"),
            'source_url': source_url,
            'is_censored': True
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('jokes.index'))

        self.assertNotEqual(amount_before, Joke.objects.count())

        new_joke = Joke.objects.order_by('-id').first()
        self.assertEqual(new_joke.user_id, user.id)
        self.assertTrue(new_joke.is_censored)

        # В списке должно отсутствовать
        response = self.client.get(reverse('jokes.index'))
        self.assertNotContains(response, joke_text, status_code=200)
        self.assertNotContains(response, source_url, status_code=200)

        # В списке должно присутствовать, если включён поках нецензурных
        response = self.client.get(reverse('jokes.index') + '?show_censored=1')
        self.assertContains(response, joke_text, status_code=200)
        self.assertContains(response, source_url, status_code=200)

    def test_creation_tomorrow(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_joke'))
        user = get_object_or_404(ExtUser, pk=user.id)

        url = reverse('jokes.creation.save')
        amount_before = Joke.objects.count()

        tomorrow = timezone.now() + timedelta(days=1)
        response = self.client.post(url, {
            'text': 'TEST_ARTICLE',
            'published_at': tomorrow.strftime("%Y-%m-%d %H:%M"),
            'source_url': 'http://animalpaw.ru',
            'is_censored': True
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('jokes.index'))

        self.assertNotEqual(amount_before, Joke.objects.count())

        new_joke = Joke.objects.order_by('-id').first()
        self.assertEqual(new_joke.user_id, user.id)

