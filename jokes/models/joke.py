# Анекдот
from django.db import models
from django.urls import reverse
from django.utils import timezone
from model_utils import Choices


class Joke(models.Model):
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    published_at = models.DateTimeField(null=True)
    source = models.ForeignKey('jokes.Source', on_delete=models.SET_NULL, null=True, blank=True)
    source_url = models.URLField(blank=True, default='')
    is_censored = models.BooleanField('Нецензурно')
    user = models.ForeignKey('extusers.ExtUser', on_delete=models.SET_NULL, null=True, blank=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    class Meta:
        app_label = 'jokes'

    @property
    def in_queue(self):
        return timezone.now() < self.published_at

    def __str__(self):
        return str(self.id) + ' ' + self.text[0:50] + '...'

    def get_absolute_url(self):
        return reverse('jokes.joke', args=[
            self.id
        ])

