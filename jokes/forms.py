from django.forms import ModelForm

from jokes.models import Joke


class JokeForm(ModelForm):
    class Meta:
        model = Joke
        exclude = (
            'status',
            'created_at',
            'source',
            'user'
        )

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['text'].label = 'Анекдот'
        self.fields['published_at'].label = 'Дата публикации'
        self.fields['source_url'].label = 'Источник'
        self.fields['is_censored'].label = 'Нецензурный'

