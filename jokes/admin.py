from django.contrib import admin

# Register your models here.
from .models import Source, Joke

admin.site.register((Source, Joke, ))
