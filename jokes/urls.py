from django.conf.urls import url
from jokes import views


urlpatterns = [
    url(r'^$', views.jokes, name='jokes.index'),
    url(r'^(?P<id>\d+)/delete$', views.delete, name='jokes.delete'),
    url(r'^(?P<id>\d+)/censor$', views.censor, name='jokes.censor'),
    url(r'^creation$', views.creation_page, name='jokes.creation'),
    url(r'^creation/save$', views.create, name='jokes.creation.save'),
    url(r'^(?P<id>\d+)/editing$', views.editing_page, name='jokes.editing'),
    url(r'^(?P<id>\d+)/save$', views.edit, name='jokes.editing.save'),
    url(r'^(?P<id>\d+)$', views.joke, name='jokes.joke')
]
