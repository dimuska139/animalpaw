from lxml import etree

import lxml.html
import html
import requests

from jokes.models import Joke
from jokes.models.source import Source
from .abstract_strategy import AbstractStrategy


class Comedycentral(AbstractStrategy):
    def __init__(self):
        self.source = Source.objects.filter(alias='comedycentral').first()
        self.base_url = self.source.base_url

    def pages(self):
        # from jokes.parser.strategy.comedycentral import Comedycentral
        for i in range(10):
            if i == 0:
                url = self.base_url
            else:
                url = self.base_url + '?start=' + str(i)

            page = requests.get(url).text
            parser = lxml.html.fromstring(page)

            jokes_list = parser.cssselect('.article-content p')
            parsed = []
            for item in jokes_list:
                item = etree.tostring(item)
                item = item.decode('utf-8')
                item = html.unescape(item)

                if not item.startswith('<p align="center">'):
                    item = item[3:-4]
                    parsed.append(Joke(text=item, source=self.source, is_censored=True))
            yield parsed
