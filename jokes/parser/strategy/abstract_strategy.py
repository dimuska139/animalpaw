from abc import ABCMeta, abstractmethod


class AbstractStrategy:
    __metaclass__ = ABCMeta

    @abstractmethod
    def pages(self):
        pass
