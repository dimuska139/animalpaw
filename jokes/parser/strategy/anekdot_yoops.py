from lxml import etree

import lxml.html
import html
import requests

from jokes.models import Joke
from jokes.models.source import Source
from .abstract_strategy import AbstractStrategy


class AnekdotYoops(AbstractStrategy):
    def __init__(self):
        self.source = Source.objects.filter(alias='anekdot_yoops').first()
        self.base_url = self.source.base_url

    def pages(self):
        # from jokes.parser.strategy.anekdot_yoops import AnekdotYoops
        for i in range(20):
            real_page = i + 1
            if real_page == 1:
                url = self.base_url + '/'
            else:
                url = self.base_url + '/?page=' + str(real_page)

            page = requests.get(url).text

            parser = lxml.html.fromstring(page)

            jokes_list = parser.cssselect('td .item')
            parsed = []
            for item in jokes_list:
                item = etree.tostring(item)
                item = item.decode('utf-8')
                item = html.unescape(item)
                item = item[18:-6]
                info_ind = item.find('<div')
                if info_ind != -1:
                    item = item[0:info_ind]

                item = item.strip()
                if item.endswith('<br />'):
                    item = item[0:-6]

                parsed.append(Joke(text=item, source=self.source, is_censored=True))
            yield parsed
