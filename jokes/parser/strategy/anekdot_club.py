from lxml import etree

import lxml.html
import html
import requests

from jokes.models import Joke
from jokes.models.source import Source
from .abstract_strategy import AbstractStrategy


class AnekdotClub(AbstractStrategy):
    def __init__(self):
        self.source = Source.objects.filter(alias='anekdot_club').first()
        self.base_url = self.source.base_url

    def pages(self):
        for i in range(90):
            real_number = i + 1
            if real_number == 1:
                url = '%s/zhivotnye.html' % self.base_url
            else:
                url = '%s/zhivotnye%s.html' % (self.base_url, real_number)

            page = requests.get(url).text
            parser = lxml.html.fromstring(page)

            table = parser.cssselect('td.container_wait')[0]

            table = etree.tostring(table)
            table = table.decode('utf-8')
            table = html.unescape(table)
            table = table.replace('<td class="container_wait">', '')
            table = table.replace('</td>', '')
            table = ' '.join(table.split())
            table = table.replace('<br/> <br/>', '')

            jokes_list = table.split('<center>* * *</center>')
            parsed = []
            for joke in jokes_list:
                if joke.startswith('<br/> '):
                    joke = joke[6:]

                if joke.startswith(' <br/> '):
                    joke = joke[7:]

                pages_ind = joke.find(' <br/> <b>Страницы: ')
                if pages_ind != -1:
                    joke = joke[0:pages_ind]
                joke = joke.strip(' ')

                parsed.append(Joke(text=joke, source=self.source, is_censored=True))
            yield parsed



