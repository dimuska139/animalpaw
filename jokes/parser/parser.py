from jokes.models import Joke
from .strategy import AnekdotClub
from .strategy import AnekdotYoops
from .strategy import Comedycentral


class Parser:
    def __init__(self):
        self.sources = [AnekdotClub(), AnekdotYoops(), Comedycentral()]

    def db_fill(self):
        for source in self.sources:
            for page in source.pages():
                Joke.objects.bulk_create(page)



