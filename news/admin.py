from django.contrib import admin

# Register your models here.
from news.models import News, Source, Blacklist

admin.site.register((News, Source, Blacklist))
