# Источник (откуда парсятся новости)
from django.db import models
from model_utils import Choices


class Source(models.Model):
    domain = models.TextField()
    is_enabled = models.BooleanField('Включён', default=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.domain
