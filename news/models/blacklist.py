# Запрещённые слова (для парсера
from django.db import models
from model_utils import Choices


class Blacklist(models.Model):
    word = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.word
