from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from model_utils import Choices
from taggit.managers import TaggableManager
from django.utils import timezone
from django.db.models import Sum
from django.conf import settings

from comments.models import Comment
from news.models.blacklist import Blacklist

from utils.models.rating import Rating



class News(models.Model):
    title = models.CharField(max_length=300)
    logo = models.ForeignKey('utils.Image', on_delete=models.SET_NULL, null=True)
    annotation = models.TextField(null=True, blank=True)
    text = models.TextField()
    url = models.TextField(null=True)
    site_id = models.IntegerField(null=True)
    views = models.IntegerField(default=0, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    published_at = models.DateTimeField(null=True)
    image_url = models.URLField(null=True, blank=True)
    is_visible = models.BooleanField('Доступна пользователям', default=True)
    comments = GenericRelation(Comment, related_query_name='articles')
    source = models.ForeignKey('news.Source', on_delete=models.SET_NULL, null=True, blank=True)
    rating = GenericRelation(Rating, related_query_name='articles')
    tags = TaggableManager(blank=True)

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def score(self):
        rating_amount = self.rating.filter(status=Rating.STATUSES.active).count()
        rating_sum = self.rating.aggregate(Sum('value'))['value__sum']
        if rating_amount == 0:
            return 0.0
        return rating_sum / rating_amount

    def reviews_num(self):
        return self.rating.filter(status=Rating.STATUSES.active).count()

    def get_absolute_url(self):
        return reverse('news.article', args=[
            self.id
        ])

    @property
    def img_amount(self):
        return self.text.count('<img')

    @property
    def is_trash(self):
        if len(self.text) < getattr(settings, 'MIN_NEWS_LENGTH', 500):
            return True

        blacklist_words = Blacklist.objects.filter(status=Blacklist.STATUSES.active).values_list('word', flat=True)
        for bl in blacklist_words:
            bl = bl.lower()
            if bl in self.title.lower() or bl in self.text.lower():
                return True
        return False

    def get_content_type(self):
        return ContentType.objects.get_for_model(self).id

    @property
    def in_queue(self):
        return timezone.now() < self.published_at

    def __str__(self):
        return str(self.id) + ' ' + str(self.title)


from imagekit import ImageSpec, register
from imagekit.processors import ResizeToFill


class Thumbnail(ImageSpec):
    processors = [ResizeToFill(300, 300)]
    format = 'JPEG'
    options = {'quality': 90}

register.generator('news:thumbnail', Thumbnail)
