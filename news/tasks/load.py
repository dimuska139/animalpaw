from celery.task import task
from news.parser import Loader


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def load():
    Loader().load()
