import json

import os
import requests
import vk
from django.contrib.sites.models import Site
from django.urls import reverse
from django.utils.dateformat import DateFormat
from celery.task import task
from django.conf import settings

from news.models import News
from django.utils import timezone
from datetime import timedelta
from django.contrib.staticfiles.templatetags.staticfiles import static


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def daily_aggregation():
    yesterday = timezone.now() - timedelta(days=1)
    df = DateFormat(yesterday)
    yesterday = df.format('Y-m-d')

    today = timezone.now()
    df = DateFormat(today)
    today = df.format('Y-m-d')

    daily_news = News.objects.filter(created_at__gt=yesterday, created_at__lt=today,
                                     status=News.STATUSES.active, is_visible=True)

    if len(daily_news) == 0:
        return

    session = vk.Session(access_token=settings.VK_ADMIN_ACCESS_TOKEN)
    api = vk.API(session)
    owner_id = '-' + str(settings.VK_GROUP_ID)
    current_site = Site.objects.get(pk=1)
    message_parts = [
        'Новости на https://' + current_site.domain + ' за ' + yesterday + ':'
    ]

    for num, news in enumerate(daily_news):
        message_parts.append(str(num + 1) + ') ' + news.title)
        message_parts.append('Подробнее: https://' + current_site.domain + news.get_absolute_url())
    message_parts.append(' '.join(['#animalpaw', '#новости', '#проживотных']))
    message_parts.append('Другие новости: https://' + current_site.domain + reverse('news.index'))

    line_separator = '\n\n'
    message = line_separator.join(message_parts)

    result = api.photos.getWallUploadServer(gid=settings.VK_GROUP_ID)
    upload_url = result['upload_url']

    img = {
        'photo': (
            'img.png', open(
                os.path.join(settings.BASE_DIR, 'static') + '/img/newspaper.png', 'rb'
            )
        )
    }
    response = requests.post(upload_url, files=img)
    upload_result = json.loads(response.text)
    result = api.photos.saveWallPhoto(photo=upload_result['photo'], hash=upload_result['hash'],
                                      server=upload_result['server'], gid=settings.VK_GROUP_ID)
    api.wall.post(owner_id=owner_id, message=message, from_group=1, friends_only=0, attachments=result[0]['id'])
