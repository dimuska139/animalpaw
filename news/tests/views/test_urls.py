# -*- coding: utf-8 -*-

from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('news.article', args=[1]), '/news/1')
        self.assertEqual(reverse('news.index'), '/news/')
