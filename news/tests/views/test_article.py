# -*- coding: utf-8 -*-
import json
from django.http import SimpleCookie
from django.test import TestCase
from django.urls import reverse
from extusers.models import ExtUser
from news.models import News


class NewsArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'news.json']

    def login(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        return user

    def test_deleted(self):
        news = News.objects.first()
        news.status = News.STATUSES.deleted
        news.save()
        url = reverse('news.article', args=[
            news.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_views(self):
        # Просмотр новости, количество просмотров увеличивается на 1
        news = News.objects.first()
        news_views = news.views
        url = reverse('news.article', args=[
            news.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        news = News.objects.get(pk=news.pk)
        news_views_after = news.views
        self.assertEqual(1, news_views_after - news_views)

        # Повторный просмотр новости - количество просмотров не меняется
        self.client.cookies = SimpleCookie({'viewed_news': json.dumps([news.id])})

        views_before = news.views
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        news = News.objects.get(pk=news.pk)
        views_after = news.views
        self.assertEqual(views_before, views_after)

    def test_is_visible(self):
        news = News.objects.first()
        url = reverse('news.article', args=[
            news.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        news.is_visible = False
        news.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

