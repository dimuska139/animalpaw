# -*- coding: utf-8 -*-
from datetime import timedelta

from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from articles.models import Article
from extusers.models import ExtUser
from news.models import News


class NewsTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'news.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_page(self):
        test_news_title = 'TEST_News'

        news = News.objects.first()
        self.assertIsInstance(news, News)
        news_url = reverse('news.index')
        news.title = test_news_title
        news.save()

        news_page = self.client.get(news_url)

        self.assertContains(news_page, test_news_title, status_code=200)

        news.is_visible = False
        news.save()

        news_page = self.client.get(news_url)
        self.assertNotContains(news_page, test_news_title, status_code=200)
