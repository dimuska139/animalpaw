# coding: utf-8
import warnings

from django.test import TestCase
from news.models import Source

class SourceTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation(self):
        source = Source.objects.create(domain='ria.ru')

        self.assertTrue(isinstance(source, Source))
        self.assertEqual(source.__str__(), source.domain)
        self.assertEqual(source.status, Source.STATUSES.active)
        self.assertTrue(source.is_enabled, True)
