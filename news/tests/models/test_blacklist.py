# coding: utf-8

from django.test import TestCase
from news.models import Blacklist


class BlacklistTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation(self):
        blacklist = Blacklist.objects.create(word='twitter')

        self.assertTrue(isinstance(blacklist, Blacklist))
        self.assertEqual(blacklist.__str__(), blacklist.word)
        self.assertEqual(blacklist.status, Blacklist.STATUSES.active)
