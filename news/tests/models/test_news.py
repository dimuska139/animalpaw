# coding: utf-8
import warnings

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from news.models import News
from news.models import Source

warnings.filterwarnings(
    'ignore', r"DateTimeField .* received a naive datetime",
    RuntimeWarning, r'django\.db\.models\.fields')


class NewsArticleTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation(self):
        news = News.objects.create(title='Animal title',
                                   annotation='Animals',
                                   text='Article about animals',
                                   url='http://logo.jpg',
                                   site_id=1,
                                   views=0,
                                   published_at=timezone.now(),
                                   image_url='http://logo.jpg',
                                   source=Source.objects.first())

        self.assertTrue(isinstance(news, News))
        self.assertEqual(news.__str__(), str(news.id) + ' ' + news.title)
        self.assertEqual(news.status, News.STATUSES.active)
        self.assertTrue(news.is_visible, True)
        self.assertEqual(news.views, 0)

    def test_absolute_url(self):
        news = News.objects.create(title='Animal title',
                                   annotation='Animals',
                                   text='Article about animals',
                                   url='http://logo.jpg',
                                   site_id=1,
                                   views=0,
                                   published_at=timezone.now(),
                                   image_url='http://logo.jpg',
                                   source=Source.objects.first())
        self.assertEqual(reverse('news.article', args=[news.id]), news.get_absolute_url())
