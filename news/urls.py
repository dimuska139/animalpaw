from django.conf.urls import url
from news import views

urlpatterns = [
    url(r'^$', views.news, name='news.index'),
    url(r'^(?P<id>\d+)$', views.article, name='news.article'),
]
