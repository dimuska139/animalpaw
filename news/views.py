import json

from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import Http404
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.template import loader

from comments.forms import CommentForm
from comments.models import Comment
from news.models import News


def news(request):
    if 'page' in request.GET:
        page = int(request.GET['page'])
    else:
        page = 1
    page_size = getattr(settings, 'NEWS_PAGESIZE', 20)

    news_list = News.objects.filter(status=News.STATUSES.active, is_visible=True).select_related('logo')

    if 'tag' in request.GET:
        news_list = news_list.filter(tags__name__in=[request.GET['tag']])

    news_list = news_list.order_by('-published_at', '-created_at')
    paginator = Paginator(news_list, page_size)

    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    if request.is_ajax():
        template = loader.get_template('news/news_list.html')
        context = {
            'news': articles
        }
        return JsonResponse({
            'success': True,
            'total': page_size,
            'message': '',
            'max_page': paginator.num_pages,
            'html': template.render(context, request)})

    template = loader.get_template('news/pages/news.html')
    context = {
        'news': articles,
        'max_page': paginator.num_pages,
        'page': page
    }
    if 'tag' in request.GET:
        context.update({'tag': request.GET['tag']})
    return HttpResponse(template.render(context, request))


def article(request, id):
    art = News.objects.filter(status=News.STATUSES.active, is_visible=True, id=id).first()
    if not art:
        raise Http404

    if 'viewed_news' in request.COOKIES:
        viewed_news = json.loads(request.COOKIES.get('viewed_news'))
    else:
        viewed_news = []

    if art.id not in viewed_news:
        art.views += 1
        art.save()
        viewed_news.append(art.id)

    tags_list = []
    for tag in art.tags.names():
        tags_list.append(tag)
    meta_keywords = ', '.join(tags_list)

    meta_keywords = meta_keywords.capitalize()

    comment_form = CommentForm()

    comments = art.comments.filter(status=Comment.STATUSES.active, parent=None).select_related('user',
                                                                                               'user__avatar').prefetch_related(
        'photos').order_by('-created_at')
    comments_amount = art.comments.filter(status=Comment.STATUSES.active, parent=None).count()

    template = loader.get_template('news/pages/article.html')
    context = {
        'article': art,
        'meta_keywords': meta_keywords,
        'comment_form': comment_form,
        'comments': comments,
        'comments_amount': comments_amount
    }

    response = HttpResponse(template.render(context, request))
    response.set_cookie('viewed_news', json.dumps(viewed_news))
    return response
