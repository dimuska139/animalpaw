import requests
from requests.adapters import HTTPAdapter
from abc import ABCMeta, abstractmethod, abstractproperty

from news.models.source import Source
from datetime import datetime
from lxml import etree
import lxml.html
import html

from utils.models import Image


class AbstractStrategy:
    __metaclass__ = ABCMeta

    def __init__(self):
        self.data = {}
        self.page = None
        self.source = Source.objects.filter(domain=self.domain).first()

    def get_full_url(self, src):
        if self.domain.startswith('www'):  # Случай с www у домена. У картинок в url www может отсутствовать.
            if self.domain[4:] not in src:  # Ищем без www
                return self.scheme + '://' + self.domain + src

        # //cdn-st1.rtr-vesti.ru/vh/pictures/xw/135/512/5.jpg
        if src.startswith('//'):
            return self.scheme + ':' + src

        # Обработка ссылок вида /img/img.png
        if self.domain not in src:
            return self.scheme + '://' + self.domain + src

        # Обработка ссылок вида sitename.ru/img/img.png
        if (self.scheme + '://') not in src:
            return self.scheme + '://' + src

        return src

    @abstractproperty
    @abstractmethod
    def domain(self):
        pass

    @abstractproperty
    @abstractmethod
    def scheme(self):
        pass

    def text_processing(self, paragraph):
        # Удаление ссылок
        for a in paragraph.cssselect('a'):
            a.drop_tag()

        # Обработка картинок
        for img in paragraph.cssselect('img'):
            if 'class' in img.attrib:
                img.attrib.pop('class')
            if 'style' in img.attrib:
                img.attrib.pop('style')
            img.set('alt', self.get_title())

            # Скачиваем изображение себе
            image = Image.from_url(self.get_full_url(img.attrib['src']))

            # Меняем урл на свой
            img.set('src', image.original.url)

        # Удаление inline css у абзацев
        if 'style' in paragraph.attrib:
            paragraph.attrib.pop('style')

        # Удаление классов стилей у абзацев
        if 'class' in paragraph.attrib:
            paragraph.attrib.pop('class')

        # Удаление жирного текста
        for strong in paragraph.cssselect('strong'):
            strong.drop_tag()

        # Удаление жирного текста
        for b in paragraph.cssselect('b'):
            b.drop_tag()

        # Удаление подписей к видео (например, на goodnewsanimal)
        for em in paragraph.cssselect('em'):
            em.drop_tree()

        # Удаление iframe
        for iframe in paragraph.cssselect('iframe'):
            iframe.drop_tree()

        # Преобразование в текст
        data = etree.tostring(paragraph)
        data = data.decode('utf-8')
        return html.unescape(data)

    def load(self, data):
        self.data = data
        headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}
        html = requests.get(self.get_url(), headers=headers).text
        self.page = lxml.html.fromstring(html)

    def get_title(self):
        return self.data['title']

    def get_url(self):
        return self.get_full_url(self.data['url'])

    def get_domain(self):
        return self.data['domain']

    @abstractmethod
    def get_annotation(self):
        pass

    @abstractmethod
    def get_text(self):
        pass

    @abstractmethod
    def get_image_url(self):
        pass

    def get_id(self):
        return int(self.data['id'])

    def get_dt(self):
        return datetime.fromtimestamp(self.data['timestamp'])

    @abstractmethod
    def get_tags(self):
        pass

    def get_source(self):
        return self.source
