from news.parser.strategy.abstract_strategy import AbstractStrategy


class Lenta(AbstractStrategy):
    @property
    def scheme(self):
        return 'https'

    def get_tags(self):
        return []

    def get_annotation(self):
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.b-text p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.b-topic__title-image img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        image = self.page.cssselect('.b-text img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def domain(self):
        return 'lenta.ru'
