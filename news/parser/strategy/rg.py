from news.parser.strategy.abstract_strategy import AbstractStrategy


class Rg(AbstractStrategy):
    @property
    def scheme(self):
        return 'https'

    def get_tags(self):
        tags = self.page.cssselect('.b-link.b-material-head__rubric-name')[0]
        return tags.text

    def get_annotation(self):
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('#articleContainer .b-material-wrapper__text p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        return None

    @property
    def domain(self):
        return 'rg.ru'
