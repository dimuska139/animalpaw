from news.parser.strategy.abstract_strategy import AbstractStrategy


class Vokrugsveta(AbstractStrategy):
    @property
    def scheme(self):
        return 'http'

    def get_tags(self):
        tags = self.page.cssselect('.tags a')
        tags_list = []
        for tag in tags:
            tags_list.append(tag.text)
        return tags_list

    def get_annotation(self):
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.detail-text p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.detail-text p>img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def domain(self):
        return 'www.vokrugsveta.ru'
