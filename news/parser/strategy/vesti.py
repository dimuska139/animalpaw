from news.parser.strategy.abstract_strategy import AbstractStrategy

class Vesti(AbstractStrategy):
    @property
    def scheme(self):
        return 'http'

    def get_tags(self):
        return None

    def get_annotation(self):
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.article__text .js-mediator-article p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            paragraph_text = self.text_processing(paragraph)
            text += paragraph_text
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.article__img img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        image = self.page.cssselect('.article__text .js-mediator-article p img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def domain(self):
        return 'vesti.ru'
