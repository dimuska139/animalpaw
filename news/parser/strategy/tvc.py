from news.parser.strategy.abstract_strategy import AbstractStrategy


class Tvc(AbstractStrategy):
    @property
    def scheme(self):
        return 'http'

    def get_tags(self):
        tags = self.page.cssselect('.b-article__text > p > a')
        tags_list = []
        for tag in tags:
            tags_list.append(tag.text)
        return tags_list

    def get_annotation(self):
        annotation = self.page.cssselect('.b-article__text .b-article__anons')
        try:
            annotation = annotation[0]
            if len(annotation.text) > 0:
                return annotation.text.strip()
        except:
            pass
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.b-article__text .js-mediator-article p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            paragraph_text = self.text_processing(paragraph)
            if 'Читайте также:' in paragraph_text:
                break
            text += paragraph_text
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.b-article-content img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def domain(self):
        return 'www.tvc.ru'
