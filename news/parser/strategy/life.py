from news.parser.strategy.abstract_strategy import AbstractStrategy


class Life(AbstractStrategy):
    @property
    def scheme(self):
        return 'https'

    def get_tags(self):
        tags = self.page.cssselect('.tags-item a')
        tags_list = []
        for tag in tags:
            if 'life' not in tag.text.lower():
                tags_list.append(tag.text.replace('#', ''))
        return tags_list

    def get_annotation(self):
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.post-page-content-main .content-note p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.post-page-image img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        image = self.page.cssselect('.content-note img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def domain(self):
        return 'life.ru'
