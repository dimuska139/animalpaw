from news.parser.strategy.abstract_strategy import AbstractStrategy


class GoodNewsAnimal(AbstractStrategy):
    def get_tags(self):
        tags = self.page.cssselect('.tags noindex .eTag')
        tags_list = []
        for tag in tags:
            tags_list.append(tag.text)
        return tags_list

    def get_annotation(self):
        annotation = self.page.cssselect('.message p')
        try:
            annotation = annotation[0]
            if len(annotation.text) > 0:
                return annotation.text.strip()
        except:
            pass
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.message p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.message p img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def scheme(self):
        return 'http'

    @property
    def domain(self):
        return 'goodnewsanimal.ru'
