from news.parser.strategy.abstract_strategy import AbstractStrategy


class Mir24(AbstractStrategy):
    @property
    def scheme(self):
        return 'http'

    def get_tags(self):
        return None

    def get_annotation(self):
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.part__wrap ._ga1_on_ p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.player-wrapper a')
        if len(image) > 0:
            return self.get_full_url(image[0].get('data-img'))
        image = self.page.cssselect('.img__box a img')
        if len(image) > 0:
            return self.get_full_url(image[0].get('src'))
        return None

    @property
    def domain(self):
        return 'mir24.tv'
