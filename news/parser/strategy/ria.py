from news.parser.strategy.abstract_strategy import AbstractStrategy


class Ria(AbstractStrategy):
    @property
    def scheme(self):
        return 'https'

    def get_tags(self):
        tags = self.page.cssselect('.b-article__tags-item a span')
        tags_list = []
        for tag in tags:
            tags_list.append(tag.text.replace('#', ''))
        return tags_list

    def get_annotation(self):
        annotation = self.page.cssselect('.b-article__ind .b-article__lead')
        try:
            annotation = annotation[0]
            if len(annotation.text) > 0:
                return annotation.text.strip()
        except:
            pass
        return None

    def get_text(self):
        paragraphs = self.page.cssselect('.b-article__ind .b-article__body p')
        if len(paragraphs) == 0:
            return None
        text = ''
        for paragraph in paragraphs:
            text += self.text_processing(paragraph)
        return text.strip()

    def get_image_url(self):
        image = self.page.cssselect('.b-share-media__default .l-photoview__open')
        if len(image) > 0:
            return self.get_full_url(image[0].get('data-photoview_src'))
        return None


    @property
    def domain(self):
        return 'ria.ru'
