import re
import requests
from django.conf import settings
from requests.adapters import HTTPAdapter
import json
from random import randint
from urllib.parse import quote

import time

from news.models import News, Blacklist
from news.parser.parser import Parser


class Loader:
    def __init__(self):
        self.keywords = ['животные', 'питомцы', 'кошки', 'собаки']
        self.load()

    def load(self):
        empty_result = False
        page_number = 0
        while not empty_result:  # По страницам
            # &d=month - за месяц
            url = 'http://mediametrics.ru/search.pl?ac=search&nolimit=1&p=' + str(page_number) + '&c=ru&q=' + quote(
                ','.join(self.keywords))
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}
            html = requests.get(url, headers=headers).text

            matches = re.findall(',\s\[\n\{(.*?)\},\nnull', html, re.DOTALL)
            if len(matches) > 0:
                list_string = '[{' + matches[0] + '}]'
                try:
                    decoded_json = json.loads(list_string)
                except ValueError:
                    print('Error')
                    continue

                for item in decoded_json:  # По новостям на странице
                    # Пропуск дублей
                    if News.objects.filter(site_id=item['id'], status=News.STATUSES.active).exists():
                        continue

                    # Поиск по началу заголовка (чтобы не было дублей с РИАновостей, например).
                    # TODO: переделать на степень схожести
                    short_title = item['title'][:30]
                    if News.objects.filter(title__startswith=short_title, status=News.STATUSES.active).exists():
                        continue

                    parser = Parser(item)
                    result = parser.parse()
                    if result is None:
                        continue

                    if result['news'].is_trash:
                        continue

                    result['news'].save()
                    if result['tags'] is not None:  # Добавление тегов, если необходимо
                        for tag in result['tags']:
                            if len(tag) > 5:
                                result['news'].tags.add(tag)
            else:
                empty_result = True
            page_number += 1
            time.sleep(randint(1, 3))  # Случайная задержка
