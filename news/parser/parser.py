from news.models import News
from news.parser.strategy.lenta import Lenta
from news.parser.strategy.life import Life
from news.parser.strategy.mir24 import Mir24
from news.parser.strategy.rg import Rg
from news.parser.strategy.ria import Ria
from news.parser.strategy.goodnewsanimal import GoodNewsAnimal
from news.parser.strategy.tvc import Tvc
from news.parser.strategy.vesti import Vesti
from news.parser.strategy.vokrugsveta import Vokrugsveta
from utils.models import Image


class Parser:
    def __init__(self, data):
        self.data = data
        self.sources = [
            Ria(),
            GoodNewsAnimal(),
            Tvc(),
            Rg(),
            Vesti(),
            Mir24(),
            Life(),
            Lenta(),
            Vokrugsveta()
        ]

    def get_source(self):
        for src in self.sources:
            if src.domain == self.data['domain'] and src.source.is_enabled:
                return src
        return None

    def parse(self):
        src = self.get_source()
        if src is None:
            return None
        src.load(self.data)

        logo = None
        image_url = src.get_image_url()

        if image_url is not None:
            logo = Image.from_url(image_url)

        title = src.get_title()
        text = src.get_text()

        if not all([title, text]):
            return None

        return {
            'news': News(title=title,
                         logo=logo,
                         annotation=src.get_annotation(),
                         text=text,
                         url=src.get_url(),
                         site_id=src.get_id(),
                         published_at=src.get_dt(),
                         source=src.get_source(),
                         image_url=image_url
                         ),
            'tags': src.get_tags()
        }
