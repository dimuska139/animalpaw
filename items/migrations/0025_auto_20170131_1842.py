# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-01-31 18:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0024_photo_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='logo',
            name='user',
        ),
        migrations.RemoveField(
            model_name='photo',
            name='user',
        ),
    ]
