# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-12-15 09:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0007_complainttype_code_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='base',
            name='url',
        ),
    ]
