__author__ = 'dimuska139'
from django.template.defaultfilters import floatformat
from django.template import Library

register = Library()


def formatted_float(value, arg=4):
    value = floatformat(value, arg=arg)
    return str(value).replace(',', '.')

register.filter('formatted_float', formatted_float)
