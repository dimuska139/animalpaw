# -*- coding: utf-8 -*-

__author__ = 'dimuska139'
from django.template import Library
from datetime import datetime

register = Library()

month_names = [
    'января',
    'февраля',
    'марта',
    'апреля',
    'мая',
    'июня',
    'июля',
    'августа',
    'сентября',
    'октября',
    'ноября',
    'декабря'
]


def publish_datetime(value):
    try:
        current_date = datetime.today()
        if current_date.year == value.year and current_date.month == value.month and current_date.day == value.day:
            return 'сегодня в ' + datetime.strftime(value, '%H:%M')
        if current_date.year == value.year:
            return str(value.day) + ' ' + month_names[value.month - 1] + " в " + datetime.strftime(value, '%H:%M')
        return '{0} {1}{2} года в {3}'.format(str(value.day), month_names[value.month - 1], str(value.year),
                                              datetime.strftime(value, '%H:%M'))
    except AttributeError:
        return "-"


register.filter('publish_datetime', publish_datetime)
