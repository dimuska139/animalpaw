'''from django import template
from items.models import Pet

register = template.Library()


@register.inclusion_tag("last_items.html")
def last_items():
    items = Pet.objects.filter(is_history=False).exclude(status_id=-1).order_by('-published_at', '-created_at')[0:5]
    return {
        'last_items': items
    }
'''