# -*- coding: utf-8 -*-
from extusers.models import ExtUser
from items.models import Type

__author__ = 'dimuska139'


from django import forms
from django.conf import settings
from .models import Base, Pet, Feedback


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        exclude = ('status', )


class BaseForm(forms.ModelForm):
    name = forms.CharField(max_length=100)
    phone = forms.CharField(max_length=17)

    class Meta:
        model = Base
        exclude = ()

    def clean_photos(self):
        photos_list = self.cleaned_data.get('photos')
        if photos_list.count() > settings.MAX_ITEM_PHOTOS_AMOUNT:
            raise forms.ValidationError('Загружено более ' + str(settings.MAX_ITEM_PHOTOS_AMOUNT) + ' фотографий')
        return photos_list

    def clean_price(self):
        if self.cleaned_data.get('type') == Type.selling() and self.cleaned_data.get('price') is None:
            raise forms.ValidationError('Не указана цена')
        return self.cleaned_data.get('price')


class PetForm(BaseForm):
    class Meta:
        model = Pet
        exclude = (
            'views',
            'published_at',
            'status',
            'user',
            'complaints',
            'url'
        )

    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        self.fields['kind'].label = 'Животное'
        self.fields['breed'].label = 'Порода'
        self.fields['nickname'].label = 'Кличка'
        self.fields['gender'].label = 'Пол'
        self.fields['logo'].label = 'Миниатюра'
        self.fields['photos'].label = 'Фотографии'
        self.fields['type'].label = 'Тип объявления'
        self.fields['price'].label = 'Цена'
        self.fields['title'].label = 'Заголовок'
        self.fields['description'].label = 'Описание'
        self.fields['address'].label = 'Место (адрес)'
        self.fields['latitude'].label = 'Широта'
        self.fields['longitude'].label = 'Долгота'
        self.fields['region'].label = 'Регион'

