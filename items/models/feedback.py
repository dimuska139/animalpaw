from django.db import models
from model_utils import Choices


class Feedback(models.Model):
    email = models.EmailField('Электронная почта', max_length=255)
    name = models.CharField(max_length=300, blank=True)
    text = models.TextField(max_length=500)
    user = models.ForeignKey('extusers.ExtUser', on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name
