# Объявления о животных
from django.db import models
from django.urls import reverse
from model_utils import Choices
from items.models.base import Base


class Pet(Base):
    breed = models.ForeignKey('animals.Breed', null=True, blank=True)  # Порода
    kind = models.ForeignKey('animals.Kind', null=True)
    nickname = models.CharField(max_length=100, null=True, blank=True)  # Кличка
    GENDER = Choices(
        (0, 'female', 'Самка'),
        (1, 'male', 'Самец'))
    gender = models.IntegerField(choices=GENDER, default=GENDER.male)

    def get_absolute_url(self):
        return reverse('item', args=[
            self.region.url_alias,
            self.type.url_alias,
            self.kind.url_alias,
            self.pk
        ])
