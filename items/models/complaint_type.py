# Тип жалобы
from django.db import models
from model_utils import Choices


class ComplaintType(models.Model):
    name = models.CharField(max_length=100)
    code_name = models.CharField(max_length=100, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    category = models.ForeignKey('items.Category', on_delete=models.SET_NULL, null=True)
    order = models.IntegerField(default=0, blank=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name
