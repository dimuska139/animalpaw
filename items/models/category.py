# Категория объявления
from django.db import models
from model_utils import Choices


class Category(models.Model):
    name = models.CharField(max_length=100)
    url_alias = models.CharField(max_length=150, null=True)
    alias = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name
