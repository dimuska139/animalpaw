# Тип объявления
from django.db import models
from model_utils import Choices


class Type(models.Model):
    category = models.ForeignKey('items.Category', on_delete=models.SET_NULL, null=True, default=1)  # Тип объявления
    url_alias = models.CharField(max_length=150, null=True)
    name = models.CharField(max_length=100)
    code_name = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    @staticmethod
    def selling():
        return Type.objects.get(id=4)

    def __str__(self):
        return self.name
