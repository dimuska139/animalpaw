# Объявление, общие данные
from django.db import models
from model_utils import Choices
from utils.models import Image


class Base(models.Model):
    type = models.ForeignKey('items.Type', on_delete=models.SET_NULL, null=True)  # Тип объявления
    title = models.CharField(max_length=200)
    description = models.TextField()
    price = models.PositiveIntegerField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    latitude = models.DecimalField(max_digits=19, decimal_places=15, null=True, blank=True)
    longitude = models.DecimalField(max_digits=19, decimal_places=15, null=True, blank=True)
    region = models.ForeignKey('utils.Region', on_delete=models.SET_NULL, null=True)  # Регион
    views = models.IntegerField(default=0, blank=True, null=True)
    user = models.ForeignKey('extusers.ExtUser', on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    published_at = models.DateTimeField(auto_now_add=True, null=True)
    photos = models.ManyToManyField(Image, related_name='photos')
    complaints = models.ManyToManyField('items.Complaint', blank=True)
    logo = models.ForeignKey('utils.Image', on_delete=models.SET_NULL, null=True)
    is_fake = models.BooleanField(default=False, blank=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    @property
    def is_sale(self):
        if self.type.code_name == 'sale':
            return True
        return False

    @property
    def is_lost(self):
        if self.type.code_name == 'lost':
            return True
        return False

    @property
    def is_find(self):
        if self.type.code_name == 'find':
            return True
        return False

    @property
    def is_gift(self):
        if self.type.code_name == 'gift':
            return True
        return False

    def __str__(self):
        return str(self.id)+' '+str(self.title)
