from django.contrib import admin
from .models import Pet, Category, Type, Complaint, ComplaintType


# Register your models here.
admin.site.register(
    (Pet, Category, Type, Complaint, ComplaintType))
