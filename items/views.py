# --*-- coding: utf-8 --*--
import base64
import hashlib
from django import template
from django.contrib import messages

from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import HttpResponse
from django.http.response import JsonResponse, Http404, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect
from django.template import loader
from django.template.loader import render_to_string
from django.urls import reverse

from django.utils.datastructures import MultiValueDictKeyError
from django.contrib.auth.decorators import login_required, permission_required
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.conf import settings
from django.core.cache import cache
from django.db.models import Q
from honeypot.decorators import check_honeypot
from jsonview.decorators import json_view

from extusers.decorators import login_required_json
from items.forms import FeedbackForm
from animals.models import Characteristic
from animals.models import PetCharacteristic
from animals.models import Breed
from animals.models import Kind
from utils.forms import get_errors
from .forms import PetForm
from extusers.models import ExtUser
from .models import Base, Pet, Type, Category, Complaint, ComplaintType
from utils.models import Region
from utils.models import Image
from PIL import ImageDraw, ImageFont
from PIL import Image as PILImage


import json
import os
import io


@json_view
def send_complaint(request, id):
    if 'complaint_type' not in request.POST:
        raise Http404
    if not ComplaintType.objects.filter(status=ComplaintType.STATUSES.active,
                                        code_name=request.POST['complaint_type']).exists():
        raise Http404
    item = get_object_or_404(Base, pk=id, status=Base.STATUSES.active)
    type = get_object_or_404(ComplaintType, code_name=request.POST['complaint_type'])
    complaint = Complaint()
    complaint.type = type
    if 'comment' in request.POST and len(request.POST['comment']) > 0:
        complaint.comment = request.POST['comment']
    if not request.user.is_anonymous():
        complaint.user = request.user
    complaint.save()
    item.complaints.add(complaint)
    return {
        'success': True,
        'error': ''
    }


def get_breeds():
    breeds = list(Breed.objects.filter(status=Breed.STATUSES.active).order_by('name'))
    dog_breeds = []
    cat_breeds = []
    bird_breeds = []
    for breed in breeds:
        if breed.kind_id == 1:
            dog_breeds.append({'id': str(breed.id), 'name': breed.name})
        if breed.kind_id == 2:
            cat_breeds.append({'id': str(breed.id), 'name': breed.name})
        if breed.kind_id == 3:
            bird_breeds.append({'id': str(breed.id), 'name': breed.name})
    return dog_breeds, cat_breeds, bird_breeds


# @cache_page(60 * 15)
def item(request, region_alias, type_alias, kind_alias, id):
    # Проверка url
    exists_region = Region.objects.filter(status=Region.STATUSES.active, url_alias=region_alias).exists()
    if not exists_region:
        raise Http404

    exists_type = Type.objects.filter(status=Type.STATUSES.active, url_alias=type_alias).exists()
    if not exists_type:
        raise Http404

    exists_kind = Kind.objects.filter(status=Kind.STATUSES.active, url_alias=kind_alias).exists()
    if not exists_kind:
        raise Http404

    if not Pet.objects.filter(id=id).exists():
        raise Http404

    pet = Pet.objects.get(id=id)
    if 'viewed_items' in request.COOKIES:
        viewed_items = json.loads(request.COOKIES.get('viewed_items'))
    else:
        viewed_items = []

    if request.user.is_authenticated and request.user.id == pet.user_id:
        is_owner = True
    else:
        is_owner = False
        if pet.status != Pet.STATUSES.active:
            raise Http404

        if not pet.id in viewed_items:
            pet.views += 1
            pet.save()
            viewed_items.append(pet.id)

    complaint_types = ComplaintType.objects.filter(status=ComplaintType.STATUSES.active,
                                                   category_id=pet.type.category_id)

    template = loader.get_template('items/pages/item.html')
    if request.user.is_authenticated:
        feedback_form = FeedbackForm(initial={
            'name': request.user.name,
            'email': request.user.email
        })
    else:
        feedback_form = FeedbackForm(initial={
            'name': '',
            'email': ''
        })
    context = {
        'item': pet,
        'complaint_types': complaint_types,
        'is_owner': is_owner,
        'id': pet.pk,
        'feedback_form': feedback_form,
        'page_id': 'item_' + str(pet.pk),
    }

    response = HttpResponse(template.render(context, request))
    response.set_cookie('viewed_items', json.dumps(viewed_items))
    return response


# Создание изображения номера телефона
@json_view
def get_phone_image(request, id):
    if not Base.objects.filter(id=id, status=Base.STATUSES.active).exists():
        return {
            'success': False,
            'message': 'Item not found',
            'html': ''
        }

    phone = Base.objects.get(id=id, status=Base.STATUSES.active).user.phone

    font_size = 30
    image = PILImage.new('RGBA', (250, 40), (255, 255, 255, 0))
    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype(os.path.dirname(__file__) + '/phone_font/UbuntuBold.ttf', font_size)
    draw.text((0, 2), phone, font=font, fill='#3c763d')
    output = io.BytesIO()
    image.save(output, format="PNG")
    contents = base64.b64encode(output.getvalue()).decode()
    output.close()

    template = loader.get_template('items/phone_image.html')
    context = {
        'image_base64': contents
    }
    return {
        'success': True,
        'message': '',
        'html': template.render(context, request)
    }


@json_view
@login_required_json
def delete(request, id):
    item = Pet.objects.get(pk=id)
    if item.user_id != request.user.id:
        return {
            'success': False
        }
    item.status = Pet.STATUSES.deleted
    item.save()
    response = {
        'success': True,
    }
    return response


@login_required
def editing_page(request, id):
    if request.user.is_superuser:  # Админ может редактировать любые объявления
        item = get_object_or_404(Pet, id=id)
    else:  # Остальные пользователи - только свои
        item = get_object_or_404(Pet, id=id, user_id=request.user.id)
    dog_breeds, cat_breeds, bird_breeds = get_breeds()
    regions = Region.objects.filter(status=Region.STATUSES.active).order_by('-items_amount', 'name')
    types = Type.objects.filter(status=Type.STATUSES.active)
    kinds = Kind.objects.filter(status=Kind.STATUSES.active)

    max_photos_amount = getattr(settings, 'MAX_ITEM_PHOTOS_AMOUNT', 3)

    pet_form = PetForm(instance=item)
    template = loader.get_template('items/pages/pet_editing.html')

    photos = item.photos.all()
    logo = item.logo

    context = {
        'kinds': kinds,
        'types': types,
        'cat_breeds': cat_breeds,
        'dog_breeds': dog_breeds,
        'bird_breeds': bird_breeds,
        'regions': regions,
        'pet_form': pet_form,
        'max_photos_amount': max_photos_amount,
        'user': request.user,
        'photos': photos,
        'logo': logo,
        'item_id': id
    }
    return HttpResponse(template.render(context, request))


@json_view
@login_required_json
def edit(request, id):
    if request.user.is_superuser:  # Админ может редактировать любые объявления
        item = Pet.objects.filter(id=id, status=Pet.STATUSES.active).first()
    else:  # Остальные пользователи - только свои
        item = Pet.objects.filter(id=id, user_id=request.user.id, status=Pet.STATUSES.active).first()

    if not item:
        return {
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Объявление не найдено'
                }
            ]
        }

    pet_form = PetForm(request.POST, instance=item)
    if not pet_form.is_valid():
        return {
            'success': False,
            'errors': get_errors(pet_form)
        }

    pet = pet_form.save(commit=False)
    pet.user = ExtUser.objects.get(pk=request.user.id)

    pet.published_at = pet.created_at
    pet.status = Pet.STATUSES.active
    pet.save()

    # Обновление имени и телефона пользователя
    request.user.name = pet_form.cleaned_data['name']
    request.user.phone = pet_form.cleaned_data['phone']
    request.user.save()

    pet.logo.make_logo(x1=request.POST['logo_x1'],
                       y1=request.POST['logo_y1'],
                       x2=request.POST['logo_x2'],
                       y2=request.POST['logo_y2'])

    item.photos.through.objects.all().delete()

    # Связать загруженные фотографии с созданным объявлением
    photos_ids = request.POST.getlist('photos')
    photos = Image.objects.filter(id__in=photos_ids).all()
    for photo in photos:
        pet.photos.add(photo)

    # Увеличить счётчик количества объявлений для соответствующего региона
    region = Region.objects.filter(id=pet.region_id).first()
    region.items_amount += 1
    region.save()

    template = loader.get_template('items/messages/item_editing_success.html')

    context = {
        'item': item,
    }
    messages.info(request, template.render(context, request))
    return {
        'success': True,
        'url': reverse('items.my')
    }


@login_required
def creation_page(request):
    dog_breeds, cat_breeds, bird_breeds = get_breeds()
    regions_list = Region.objects.filter(status=Region.STATUSES.active).order_by('-items_amount', 'name')
    regions = []
    for region in regions_list:
        regions.append({
            'id': str(region.id),
            'name': region.name,
            'latitude': region.latitude,
            'longitude': region.longitude,
        })

    types_list = Type.objects.filter(status=Type.STATUSES.active)
    types = []
    for type in types_list:
        types.append({
            'id': str(type.id),
            'name': type.name
        })

    kinds_list = Kind.objects.filter(status=Kind.STATUSES.active)
    kinds = []
    for kind in kinds_list:
        kinds.append({
            'id': str(kind.id),
            'name': kind.name
        })

    max_photos_amount = getattr(settings, 'MAX_ITEM_PHOTOS_AMOUNT', 3)

    pet_form = PetForm(initial={
        'title': '',
        'description': '',
        'nickname': '',
        'breed': '',
        'region': '',
        'address': '',
        'latitude': '',
        'longitude': '',
        'name': request.user.name,
        'phone': request.user.phone,
    })

    template = loader.get_template('items/pages/pet_creation.html')
    context = {
        'kinds': kinds,
        'types': types,
        'cat_breeds': cat_breeds,
        'dog_breeds': dog_breeds,
        'bird_breeds': bird_breeds,
        'regions': regions,
        'pet_form': pet_form,
        'max_photos_amount': max_photos_amount,
        'user': request.user
    }
    return HttpResponse(template.render(context, request))


@json_view
@login_required_json
def create(request):
    pet_form = PetForm(request.POST)
    if not pet_form.is_valid():
        return {
            'success': False,
            'errors': get_errors(pet_form)
        }

    pet = pet_form.save(commit=False)
    pet.user = ExtUser.objects.get(pk=request.user.id)
    pet.published_at = pet.created_at
    pet.status = Pet.STATUSES.active
    pet.save()

    pet.logo.make_logo(x1=request.POST['logo_x1'],
                       y1=request.POST['logo_y1'],
                       x2=request.POST['logo_x2'],
                       y2=request.POST['logo_y2'])

    # Связать загруженные фотографии с созданным объявлением
    photos_ids = request.POST.getlist('photos')
    photos = Image.objects.filter(id__in=photos_ids).all()
    for photo in photos:
        pet.photos.add(photo)

    # Увеличить счётчик количества объявлений для соответствующего региона
    region = Region.objects.filter(id=pet.region_id).first()
    region.items_amount += 1
    region.save()

    # Обновление имени и телефона пользователя
    request.user.name = pet_form.cleaned_data['name']
    request.user.phone = pet_form.cleaned_data['phone']
    request.user.save()

    template = loader.get_template('items/messages/item_creation_success.html')

    context = {
        'item': item,
    }
    messages.info(request, template.render(context, request))
    return {
        'success': True,
        'url': reverse('items.my')
    }


@json_view
@login_required_json
def logo_upload(request):
    if 'image' not in request.FILES:
        response = {
            'success': False,
            'message': u'Отсутствует файл'
        }
        return response
    if request.FILES['image'].size > settings.MAX_ITEM_LOGO_SIZE:
        response = {
            'success': False,
            'message': u'Максимальный размер загружаемого файла - ' + str(
                settings.MAX_ITEM_LOGO_SIZE / 1024) + u' кБ',
        }
        return response
    try:
        PILImage.open(request.FILES['image'])
    except IOError as e:
        response = {
            'success': False,
            'message': u'Недопустимый формат файла'
        }
        return response

    logo = Image(
        original=request.FILES['image'],
        user=request.user
    )
    logo.save()

    context = {
        'logo': logo
    }
    tpl = loader.get_template('items/uploaded_logo.html')
    html = tpl.render(context, request)

    response = {
        'success': True,
        'message': 'OK',
        'id': logo.pk,
        'html': html,
        'url': logo.original.url,
        'width': logo.original.width,
        'height': logo.original.height,
    }
    return response


@json_view
@login_required_json
def photo_upload(request):
    if 'photo' not in request.FILES:
        response = {
            'success': False,
            'message': 'Файл не обнаружен',
            'html': ''
        }
        return response

    response = {
        'success': True,
        'message': '',
        'html': ''
    }

    if request.FILES['photo'].size > settings.MAX_ITEM_PHOTOS_SIZE:
        response['success'] = False
        response['message'] = 'Максимальный размер загружаемого файла - ' + str(
            settings.MAX_ITEM_PHOTOS_SIZE / 1024) + ' кБ'
        return response

    # Check if uploaded file is image
    try:
        PILImage.open(request.FILES['photo'])
    except IOError as e:
        response['success'] = False
        response['message'] = "Недопустимый формат файла"
        return response
    photo = Image(
        original=request.FILES['photo'],
        user=request.user
    )
    photo.save()

    context = {
        'photo': photo
    }

    template = loader.get_template('items/uploaded_photo.html')
    rendered_template = template.render(context, request)
    response['id'] = photo.pk
    response['message'] = 'OK'
    response['html'] = rendered_template

    return response


def get_page_items(request, user_id=None, page_size=getattr(settings, 'ITEMS_PAGESIZE')):
    if user_id is not None:
        pets = Pet.objects.filter(user=request.user).exclude(status=Pet.STATUSES.deleted)
    else:
        pets = Pet.objects.filter(status=Pet.STATUSES.active)

    if 'gender' in request.GET and len(request.GET['gender']) > 0:  # Пол
        pets = pets.filter(gender=request.GET['gender'])
    if 'kind' in request.GET and len(request.GET['kind']) > 0:  # Тип
        pets = pets.filter(kind_id=request.GET['kind'])
    if 'breed' in request.GET and len(request.GET['breed']) > 0:  # Порода
        pets = pets.filter(breed_id=request.GET['breed'])
    if 'region' in request.GET and len(request.GET['region']) > 0:
        pets = pets.filter(region_id=request.GET['region'])
    if 'type' in request.GET and len(request.GET['type']) > 0:
        pets = pets.filter(type_id=request.GET['type'])

    page = 1
    if 'page' in request.GET and len(request.GET['page']) > 0:
        page = int(request.GET['page'])

    pets = pets.order_by('-published_at', '-created_at', '-id').prefetch_related('photos', 'logo')
    if user_id is None:
        pets = pets.prefetch_related('type',
                                     'breed',
                                     'kind',
                                     'region',
                                     'logo'
                                     )

    paginator = Paginator(pets, page_size)
    try:
        pets = paginator.page(page)
    except PageNotAnInteger:
        pets = paginator.page(1)
    except EmptyPage:
        pets = paginator.page(paginator.num_pages)
    return pets, paginator.num_pages


@login_required
def user_items(request):
    items, max_page = get_page_items(request, None)

    if request.is_ajax():
        template = loader.get_template('items/user_items_list.html')
        context = {
            'items': items
        }
        return JsonResponse({
            'success': True,
            'max_page': max_page,
            'message': '',
            'html': template.render(context, request)})

    template = loader.get_template('items/pages/user_items.html')

    context = {
        'items': items,
        'max_page': max_page,
    }
    return HttpResponse(template.render(context, request))


# @cache_page(60 * 15)
def items(request):
    items, max_page = get_page_items(request, None)

    if request.is_ajax():
        template = loader.get_template('items/items_list.html')
        context = {
            'items': items
        }
        return JsonResponse({
            'success': True,
            'max_page': max_page,
            'message': '',
            'html': template.render(context, request)})

    template = loader.get_template('items/pages/items.html')
    regions = Region.objects.filter(status=Region.STATUSES.active).order_by('-items_amount', 'name')
    types = Type.objects.filter(status=Type.STATUSES.active)
    kinds = Kind.objects.filter(status=Kind.STATUSES.active)
    dog_breeds, cat_breeds, bird_breeds = get_breeds()

    filter_region = None
    if 'region' in request.GET:
        filter_region = int(request.GET['region'])

    filter_type = None
    if 'type' in request.GET:
        filter_type = int(request.GET['type'])

    filter_kind = None
    if 'kind' in request.GET:
        filter_kind = int(request.GET['kind'])

    filter_breed = None
    if 'breed' in request.GET:
        filter_breed = int(request.GET['breed'])

    filter_gender = None
    if 'gender' in request.GET:
        filter_gender = int(request.GET['gender'])

    context = {
        'items': items,
        'max_page': max_page,
        'regions': regions,
        'cat_breeds': cat_breeds,
        'dog_breeds': dog_breeds,
        'bird_breeds': bird_breeds,
        'types': types,
        'kinds': kinds,
        'filter_region': filter_region,
        'filter_type': filter_type,
        'filter_kind': filter_kind,
        'filter_breed': filter_breed,
        'filter_gender': filter_gender,
    }
    return HttpResponse(template.render(context, request))


@json_view
@check_honeypot(field_name='hp')
def send_feedback(request, id):
    item = get_object_or_404(Pet, id=id, status=Pet.STATUSES.active)
    feedback_form = FeedbackForm(request.POST)

    if not feedback_form.is_valid():
        return {
            'success': False,
            'errors': get_errors(feedback_form)
        }

    feedback_form.save()
    current_site = Site.objects.get_current()
    html_message = render_to_string(
        'items/email/item_feedback.html',
        {
            'current_site': current_site,
            'item': item,
            'name': request.POST['name'],
            'email': request.POST['email'],
            'text': request.POST['text']
        }
    )
    send_mail('Письмо по поводу Вашего объявления', '', getattr(settings, 'EMAIL_HOST_USER'), [item.user.email],
              fail_silently=False, html_message=html_message)

    return {
        'success': True
    }
