from django.conf.urls import url
from items import views


urlpatterns = [
    url(r'^$', views.items, name='items'),
    url(r'^my$', views.user_items, name='items.my'),
    url(r'^creation$', views.creation_page, name='items.creation'),
    url(r'^creation/save$', views.create, name='items.creation.save'),

    url(r'^(?P<id>\d+)/editing$', views.editing_page, name='items.editing'),
    url(r'^(?P<id>\d+)/save$', views.edit, name='items.editing.save'),

    url(r'^(?P<id>\d+)/delete$', views.delete, name='items.delete'),
    url(r'^photo_upload$', views.photo_upload, name='item_photo_upload'),
    url(r'^logo_upload$', views.logo_upload, name='item_logo_upload'),
    url(r'^(?P<id>\d+)/complaint', views.send_complaint, name='item_send_complaint'),
    url(r'^(?P<id>\d+)/phone_image', views.get_phone_image, name='item_phone_image'),
    url(r'^(?P<id>\d+)/feedback', views.send_feedback, name='items.feedback.creation.save'),
]
