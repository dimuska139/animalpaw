# -*- coding: utf-8 -*-

from django.test import TestCase
from items.models import Type, Category


class TypeTest(TestCase):
    fixtures = ['initial.json']

    def test_creation(self):
        type = Type.objects.create(name='Отдам', url_alias='otdam', code_name='gift', category=Category.objects.get(pk=1))
        self.assertTrue(isinstance(type, Type))
        self.assertEqual(type.__str__(), type.name)
        self.assertEqual(type.status, Type.STATUSES.active)
