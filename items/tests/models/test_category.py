# -*- coding: utf-8 -*-

from django.test import TestCase

from items.models import Category


class CategoryTest(TestCase):
    def test_creation(self):
        category = Category.objects.create(name='Животные', url_alias='pets', alias='pets')
        self.assertTrue(isinstance(category, Category))
        self.assertEqual(category.__str__(), category.name)
        self.assertEqual(category.status, Category.STATUSES.active)
