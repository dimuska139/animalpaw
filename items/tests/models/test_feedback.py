# -*- coding: utf-8 -*-

from django.test import TestCase

from extusers.models import ExtUser
from items.models import Feedback


class FeedbackTest(TestCase):
    fixtures = ['users.json']

    def test_creation(self):
        feedback = Feedback.objects.create(email='test@test.test', name='Dima', text='Нашёл Вашу собаку', user=ExtUser.objects.get(pk=1))
        self.assertTrue(isinstance(feedback, Feedback))
        self.assertEqual(feedback.__str__(), feedback.name)
        self.assertEqual(feedback.status, Feedback.STATUSES.active)
