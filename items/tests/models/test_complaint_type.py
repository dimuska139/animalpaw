# -*- coding: utf-8 -*-

from django.test import TestCase
from items.models import ComplaintType, Category


class ComplaintTypeTest(TestCase):
    fixtures = ['initial.json']

    def test_creation(self):
        ct = ComplaintType.objects.create(name='Ошибка в объявлении', code_name='error',
                                          category=Category.objects.get(pk=1),
                                          order=1)
        self.assertTrue(isinstance(ct, ComplaintType))
        self.assertEqual(ct.__str__(), ct.name)
        self.assertEqual(ct.status, ComplaintType.STATUSES.active)
