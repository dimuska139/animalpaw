# -*- coding: utf-8 -*-

from django.test import TestCase

from extusers.models import ExtUser
from items.models import Complaint, ComplaintType


class ComplaintTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation(self):
        complaint = Complaint.objects.create(type=ComplaintType.objects.get(pk=1), comment='Комментарий',
                                        user=ExtUser.objects.get(pk=1))
        self.assertTrue(isinstance(complaint, Complaint))
        self.assertEqual(complaint.status, Complaint.STATUSES.active)
