# -*- coding: utf-8 -*-

from django.test import TestCase
from django.urls import reverse
from extusers.models import ExtUser
from items.models import Type, Base, Pet
from utils.models import Region
from animals.models import Breed, Kind


class PetTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def test_creation(self):
        pet = Pet.objects.create(type=Type.objects.get(pk=1),
                                 title='Найдена собака',
                                 description='Найдена собака',
                                 address='Москва, Россия',
                                 latitude=30,
                                 longitude=30,
                                 region=Region.objects.get(pk=1),
                                 user=ExtUser.objects.get(pk=1),
                                 breed=Breed.objects.get(pk=1),
                                 kind=Kind.objects.get(pk=1),
                                 nickname='Рекс',
                                 gender=1)
        self.assertTrue(pet.is_lost)
        self.assertTrue(isinstance(pet, Base))
        self.assertTrue(isinstance(pet, Pet))
        self.assertEqual(pet.__str__(), str(pet.id) + ' ' + str(pet.title))
        self.assertEqual(pet.status, Pet.STATUSES.active)
        self.assertEqual(pet.get_absolute_url(), reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.pk])
        )
