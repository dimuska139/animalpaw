# -*- coding: utf-8 -*-

from django.test import TestCase
from extusers.models import ExtUser
from items.models import Type, Base
from utils.models import Region
from moneyed import Money, RUB


class BaseTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def test_creation(self):
        base = Base.objects.create(type=Type.objects.get(pk=1),
                                   title='Пропала собака!',
                                   description='Пропала собака! Помогите!',
                                   address='Москва, Россия',
                                   latitude=30,
                                   longitude=30,
                                   region=Region.objects.get(pk=1),
                                   user=ExtUser.objects.get(pk=1))
        self.assertTrue(isinstance(base, Base))
        self.assertEqual(base.__str__(), str(base.id) + ' ' + str(base.title))
        self.assertEqual(base.status, Base.STATUSES.active)

    def test_creation_with_price(self):
        base = Base.objects.create(type=Type.selling(),
                                   title='Пропала собака!',
                                   description='Пропала собака! Помогите!',
                                   price=10,
                                   address='Москва, Россия',
                                   latitude=30,
                                   longitude=30,
                                   region=Region.objects.get(pk=1),
                                   user=ExtUser.objects.get(pk=1))
        self.assertTrue(isinstance(base, Base))
        self.assertEqual(base.__str__(), str(base.id) + ' ' + str(base.title))
        self.assertEqual(base.status, Base.STATUSES.active)

