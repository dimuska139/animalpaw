# coding: utf-8
from django.conf import settings
from django.test import TestCase
from items.forms import PetForm
from items.models import Type
from utils.models import Image


class PetTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_valid_form(self):
        data = {
            'type': 1,
            'kind': 1,
            'gender': 1,
            'title': 'Пропала собака',
            'description': 'Пропала собака. Ребёнок плачет!',
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'logo': Image.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'name': 'Дмитрий',
            'phone': '8(921)785-84-20'
        }
        form = PetForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        # Empty title
        data = {
            'type': 1,
            'kind': 1,
            'gender': 1,
            'description': 'Пропала собака. Ребёнок плачет!',
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'logo': Image.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'name': 'Дмитрий',
            'phone': '8(921)785-84-30'
        }
        form = PetForm(data=data)
        self.assertFalse(form.is_valid())

        # Empty logo
        data = {
            'type': 1,
            'kind': 1,
            'gender': 1,
            'title': 'Пропала собака',
            'description': 'Пропала собака. Ребёнок плачет!',
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'name': 'Дмитрий',
            'phone': '8(921)785-84-30'
        }
        form = PetForm(data=data)
        self.assertFalse(form.is_valid())

        # Empty name
        data = {
            'type': 1,
            'kind': 1,
            'gender': 1,
            'title': 'Пропала собака',
            'description': 'Пропала собака. Ребёнок плачет!',
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'logo': Image.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'name': '',
            'phone': '8(921)785-84-30'
        }
        form = PetForm(data=data)
        self.assertFalse(form.is_valid())

        # A lot of photos
        data = {
            'type': 1,
            'kind': 1,
            'gender': 1,
            'title': 'Пропала собака',
            'description': 'Пропала собака. Ребёнок плачет!',
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'logo': Image.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT + 1]),
            'name': 'Дмитрий',
            'phone': '8(921)785-84-30'
        }
        form = PetForm(data=data)
        self.assertFalse(form.is_valid())

    def test_valid_price_form(self):
        # Empty title
        data = {
            'type': Type.selling().id,
            'kind': 1,
            'gender': 1,
            'title': 'Пропала собака',
            'description': 'Пропала собака. Ребёнок плачет!',
            'price': 100,
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'logo': Image.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'name': 'Дмитрий',
            'phone': '8(921)785-84-20'
        }
        form = PetForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_price_form(self):
        # Empty price
        data = {
            'type': Type.selling().id,
            'kind': 1,
            'gender': 1,
            'title': 'Пропала собака',
            'description': 'Пропала собака. Ребёнок плачет!',
            'price': '',
            'address': 'Улица Пушкина, дом Колотушкина',
            'latitude': 30,
            'longitude': 30,
            'region': 1,
            'logo': Image.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'name': 'Дмитрий',
            'phone': '8(921)785-84-20'
        }
        form = PetForm(data=data)
        self.assertFalse(form.is_valid())
