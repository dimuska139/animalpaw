# coding: utf-8
from django.test import TestCase
from items.forms import FeedbackForm


class FeedbackTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def test_valid_form(self):
        data = {
            'email': 'test@test.test',
            'name': 'Dima',
            'text': 'Нашёл Вашу собаку!'
        }
        form = FeedbackForm(data=data)

        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        # Empty email
        data = {
            'name': 'Dima',
            'text': 'Нашёл Вашу собаку!'
        }
        form = FeedbackForm(data=data)
        self.assertFalse(form.is_valid())

        # Empty comment
        data = {
            'email': 'test@test.test',
            'name': 'Dima',
        }
        form = FeedbackForm(data=data)
        self.assertFalse(form.is_valid())