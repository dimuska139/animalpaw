from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet, Type
from utils.models import Region
from animals.models import Kind


class ItemsTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_permissions(self):
        url = reverse('items')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.login()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_list(self):
        url = reverse('items')
        Pet.objects.update(region_id=Region.objects.first().pk)

        response = self.client.get(url + '?region=' + str(Region.objects.last().pk))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'объявлений не найдено')

        response = self.client.get(url + '?region=' + str(Region.objects.first().pk))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'объявлений не найдено')

        Pet.objects.update(type_id=Type.objects.first().pk)

        response = self.client.get(url + '?region=' + str(Region.objects.first().pk) + '&type=' + str(Type.objects.first().pk))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'объявлений не найдено')

        response = self.client.get(
            url + '?region=' + str(Region.objects.first().pk) + '&type=' + str(Type.objects.last().pk))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'объявлений не найдено')

        Pet.objects.update(kind_id=Kind.objects.first().pk)

        response = self.client.get(
            url + '?region=' + str(Region.objects.first().pk) + '&type=' + str(Type.objects.first().pk) + '&kind=' + str(Kind.objects.first().pk))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'объявлений не найдено')

        response = self.client.get(
            url + '?region=' + str(Region.objects.first().pk) + '&type=' + str(Type.objects.first().pk) + '&kind=' + str(Kind.objects.last().pk))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'объявлений не найдено')
