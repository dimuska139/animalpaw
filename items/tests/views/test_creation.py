import json

from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet, Type
from animals.models import Breed, Kind
from utils.models import Image
from utils.models import Region


class CreationTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_page(self):
        url = reverse('items.creation')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        self.login()

        response = self.client.get(url)
        self.assertContains(response, reverse('items.creation.save'), status_code=200)

    def test_action(self):
        url = reverse('items.creation.save')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.login()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_creation(self):
        url = reverse('items.creation.save')
        amount_before = Pet.objects.count()

        user = self.login()

        response = self.client.post(url, {
            'type': Type.objects.first().pk,
            'title': 'Пропала собака',
            'description': 'Помогите найти собаку',
            'nickname': 'Рекс',
            'gender': Pet.GENDER.male,
            'breed': Breed.objects.first().pk,
            'address': 'Москва, Россия',
            'latitude': 30,
            'longitude': 30,
            'region': Region.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'logo': tuple(Image.objects.values_list('id', flat=True)[0:1]),
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 30,
            'logo_y2': 30,
            'kind': Kind.objects.first().pk,
            'name': user.name,
            'phone': user.phone
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('items.my'))
        self.assertNotEqual(amount_before, Pet.objects.count())

        created_item = Pet.objects.order_by('-id').first()

        self.assertEqual(created_item.user_id, user.id)
        self.assertEqual(created_item.status, Pet.STATUSES.active)

        self.logout()

        item_page_url = reverse('item', args=[
            created_item.region.url_alias,
            created_item.type.url_alias,
            created_item.kind.url_alias,
            created_item.id
        ])
        response = self.client.get(item_page_url)
        self.assertContains(response, 'Пропала собака', status_code=200)
        self.assertContains(response, 'Помогите найти собаку', status_code=200)

    def test_creation_user_data_change(self):
        url = reverse('items.creation.save')
        amount_before = Pet.objects.count()

        user = ExtUser.objects.first()
        user.name = ''
        user.phone = ''
        user.save()

        user = self.login()
        new_name = 'Danya'
        new_phone = '8(921)666-66-66'

        response = self.client.post(url, {
            'type': Type.objects.first().pk,
            'title': 'Пропала собака',
            'description': 'Помогите найти собаку',
            'nickname': 'Рекс',
            'gender': Pet.GENDER.male,
            'breed': Breed.objects.first().pk,
            'address': 'Москва, Россия',
            'latitude': 30,
            'longitude': 30,
            'region': Region.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'logo': tuple(Image.objects.values_list('id', flat=True)[0:1]),
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 30,
            'logo_y2': 30,
            'kind': Kind.objects.first().pk,
            'name': new_name,
            'phone': new_phone
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('items.my'))
        self.assertNotEqual(amount_before, Pet.objects.count())

        created_item = Pet.objects.order_by('-id').first()

        self.assertEqual(created_item.user_id, user.id)
        self.assertEqual(created_item.status, Pet.STATUSES.active)

        self.logout()

        item_page_url = reverse('item', args=[
            created_item.region.url_alias,
            created_item.type.url_alias,
            created_item.kind.url_alias,
            created_item.id
        ])
        response = self.client.get(item_page_url)
        self.assertContains(response, 'Пропала собака', status_code=200)
        self.assertContains(response, 'Помогите найти собаку', status_code=200)

        user = ExtUser.objects.first()
        self.assertEqual(user.name, new_name)
        self.assertEqual(user.phone, new_phone)
