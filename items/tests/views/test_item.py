import json

from django.http import SimpleCookie
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet


class ItemTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_item_permissions(self):
        pet = Pet.objects.first()
        pet.status = Pet.STATUSES.deleted
        pet.save()

        url = reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        pet.status = Pet.STATUSES.active
        pet.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_item(self):
        pet = Pet.objects.first()
        pet.status = Pet.STATUSES.active
        pet.save()

        url = reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_item_views_anonimous(self):
        pet = Pet.objects.first()
        pet.status = Pet.STATUSES.active
        pet.save()

        url = reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.id
        ])

        response = self.client.get(url)
        views_after = Pet.objects.get(id=pet.pk).views  # Количество просмотров после отображения объявления
        self.assertEqual(1, views_after - pet.views)

    def test_item_views_unique(self):
        pet = Pet.objects.first()
        pet.status = Pet.STATUSES.active
        pet.save()

        url = reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.id
        ])

        response = self.client.get(url)
        self.client.cookies = SimpleCookie({'viewed_items': json.dumps([pet.id])})
        response = self.client.get(url)
        views_after = Pet.objects.get(id=pet.pk).views  # Количество просмотров после отображения объявления
        self.assertEqual(1, views_after - pet.views)

    def test_item_views_auth_owner(self):
        # Авторизован, пользователь смотрит своё объявление
        pet = Pet.objects.first()
        pet.status = Pet.STATUSES.active
        pet.save()

        url = reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.id
        ])

        user = self.login()
        pet.user_id = user.id
        pet.save()

        views_before = Pet.objects.get(id=pet.pk).views
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Пользователь авторизован, смотрит своё объявление - кол-во просмотров не должно меняться
        self.assertEqual(0, Pet.objects.get(id=pet.pk).views - views_before)

    def test_item_views_auth_not_owner(self):
        # Авторизован, пользователь смотрит чужое объявление
        pet = Pet.objects.first()
        pet.status = Pet.STATUSES.active
        pet.save()

        url = reverse('item', args=[
            pet.region.url_alias,
            pet.type.url_alias,
            pet.kind.url_alias,
            pet.id
        ])

        user = self.login()
        user_another = ExtUser.objects.get(id=2)
        pet.user_id = user_another.id
        pet.save()

        views_before = Pet.objects.get(id=pet.pk).views
        response = self.client.get(url)
        self.client.cookies = SimpleCookie({'viewed_items': json.dumps([pet.id])})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        # Чужое объявление - кол-во просмотров должно увеличиться на 1
        self.assertEqual(1, Pet.objects.get(id=pet.pk).views - views_before)
