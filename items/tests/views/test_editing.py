import json

from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet, Type
from animals.models import Breed, Kind
from utils.models import Image
from utils.models import Region


class EditingTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_page(self):
        user = ExtUser.objects.get(id=1)
        user_another = ExtUser.objects.get(id=2)
        pet = Pet.objects.first()

        url = reverse('items.editing', args=[
            pet.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        self.login()

        pet.user = user_another
        pet.save()

        # Объявление принадлежит другому пользователю
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        pet.user = user
        pet.save()

        response = self.client.get(url)
        self.assertContains(response, reverse('items.editing.save', args=[pet.id]), status_code=200)

    def test_action(self):
        user = ExtUser.objects.get(id=1)
        user_another = ExtUser.objects.get(id=2)
        pet = Pet.objects.filter(status=Pet.STATUSES.active).first()

        url = reverse('items.editing.save', args=[
            pet.id
        ])

        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.login()

        pet.user = user_another
        pet.save()

        # Объявление принадлежит другому пользователю
        response = self.client.get(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        pet.user = user
        pet.save()

    def test_editing(self):
        user = ExtUser.objects.get(id=1)

        pet = Pet.objects.filter(status=Pet.STATUSES.active).first()
        pet.user_id = user.id
        pet.save()

        url = reverse('items.editing.save', args=[
            pet.id
        ])
        amount_before = Pet.objects.count()

        self.login()

        response = self.client.post(url, {
            'type': Type.objects.first().pk,
            'title': 'Изменённый заголовок',
            'description': 'Помогите найти собаку',
            'nickname': 'Рекс',
            'gender': Pet.GENDER.male,
            'breed': Breed.objects.first().pk,
            'address': 'Москва, Россия',
            'latitude': 30,
            'longitude': 30,
            'region': Region.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'logo': tuple(Image.objects.values_list('id', flat=True)[0:1]),
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 30,
            'logo_y2': 30,
            'kind': Kind.objects.first().pk,
            'name': user.name,
            'phone': user.phone
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('items.my'))

        self.assertEqual(amount_before, Pet.objects.count())

        edited_item = Pet.objects.get(pk=pet.id)
        self.assertEqual(edited_item.status, Pet.STATUSES.active)

        self.assertEqual(pet.views, edited_item.views)

        self.logout()

        item_page_url = reverse('item', args=[
            edited_item.region.url_alias,
            edited_item.type.url_alias,
            edited_item.kind.url_alias,
            edited_item.id
        ])
        response = self.client.get(item_page_url)
        self.assertContains(response, 'Изменённый заголовок', status_code=200)
        self.assertContains(response, 'Помогите найти собаку', status_code=200)

    def test_editing_user_data_change(self):
        user = ExtUser.objects.get(id=1)
        user.name = ''
        user.phone = ''
        user.save()

        pet = Pet.objects.filter(status=Pet.STATUSES.active).first()
        pet.user_id = user.id
        pet.save()

        url = reverse('items.editing.save', args=[
            pet.id
        ])
        amount_before = Pet.objects.count()

        self.login()

        new_name = 'Danya'
        new_phone = '8(921)666-66-66'

        response = self.client.post(url, {
            'type': Type.objects.first().pk,
            'title': 'Изменённый заголовок',
            'description': 'Помогите найти собаку',
            'nickname': 'Рекс',
            'gender': Pet.GENDER.male,
            'breed': Breed.objects.first().pk,
            'address': 'Москва, Россия',
            'latitude': 30,
            'longitude': 30,
            'region': Region.objects.first().pk,
            'photos': tuple(Image.objects.values_list('id', flat=True)[0:settings.MAX_ITEM_PHOTOS_AMOUNT]),
            'logo': tuple(Image.objects.values_list('id', flat=True)[0:1]),
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 30,
            'logo_y2': 30,
            'kind': Kind.objects.first().pk,
            'name': new_name,
            'phone': new_phone
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('items.my'))

        self.assertEqual(amount_before, Pet.objects.count())

        edited_item = Pet.objects.get(pk=pet.id)
        self.assertEqual(edited_item.status, Pet.STATUSES.active)

        self.assertEqual(pet.views, edited_item.views)

        self.logout()

        item_page_url = reverse('item', args=[
            edited_item.region.url_alias,
            edited_item.type.url_alias,
            edited_item.kind.url_alias,
            edited_item.id
        ])
        response = self.client.get(item_page_url)
        self.assertContains(response, 'Изменённый заголовок', status_code=200)
        self.assertContains(response, 'Помогите найти собаку', status_code=200)

        user = ExtUser.objects.first()
        self.assertEqual(user.name, new_name)
        self.assertEqual(user.phone, new_phone)
