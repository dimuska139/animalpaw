from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('items'), '/items/')
        self.assertEqual(reverse('items.my'), '/items/my')
        self.assertEqual(reverse('items.creation'), '/items/creation')
        self.assertEqual(reverse('items.creation.save'), '/items/creation/save')
        self.assertEqual(reverse('items.editing', args=[1]), '/items/1/editing')
        self.assertEqual(reverse('items.editing.save', args=[1]), '/items/1/save')
        self.assertEqual(reverse('items.delete', args=[1]), '/items/1/delete')
        self.assertEqual(reverse('item_photo_upload'), '/items/photo_upload')
        self.assertEqual(reverse('item_logo_upload'), '/items/logo_upload')
        self.assertEqual(reverse('item_send_complaint', args=[1]), '/items/1/complaint')
        self.assertEqual(reverse('item_phone_image', args=[1]), '/items/1/phone_image')
        self.assertEqual(reverse('items.feedback.creation.save', args=[1]), '/items/1/feedback')
        self.assertEqual(reverse('item', args=['moscow', 'propala', 'sobaka', 1]), '/moscow/propala/sobaka/1')