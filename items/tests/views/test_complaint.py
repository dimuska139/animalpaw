import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet, ComplaintType, Complaint


class ComplaintTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_send_complaint(self):
        pet = Pet.objects.first()

        url = reverse('item_send_complaint', args=[
            pet.id
        ])

        ct = ComplaintType.objects.first()
        response = self.client.post(url, {
            'complaint_type': ct.code_name
        })
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        complaint = Complaint.objects.last()
        self.assertEqual(complaint.user_id, None)

        user = self.login()
        response = self.client.post(url, {
            'complaint_type': ct.code_name
        })
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        complaint = Complaint.objects.last()
        self.assertEqual(complaint.user_id, user.id)