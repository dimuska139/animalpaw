import json

from django.core import mail
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet, Feedback


class FeedbackTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_creation(self):
        pet = Pet.objects.first()
        self.assertIsInstance(pet, Pet)
        url = reverse('items.feedback.creation.save', args=[pet.id])
        amount_before = Feedback.objects.count()

        self.assertEqual(len(mail.outbox), 0)
        response = self.client.post(url, {
            'name': 'Test',
            'email': 'test@test.test',
            'text': 'Test feedback',
            'hp': ''
        })
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        self.assertNotEqual(amount_before, Feedback.objects.count())

    def test_honeypot(self):
        pet = Pet.objects.first()
        self.assertIsInstance(pet, Pet)
        url = reverse('items.feedback.creation.save', args=[pet.id])
        amount_before = Feedback.objects.count()

        response = self.client.post(url, {
            'name': 'Test',
            'email': 'test@test.test',
            'text': 'Test feedback',
            'hp': '12345'
        })

        self.assertEqual(response.status_code, 400)

        self.assertEqual(amount_before, Feedback.objects.count())