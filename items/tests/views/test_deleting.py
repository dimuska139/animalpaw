import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet


class DeletingTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_permissions(self):
        user_another = ExtUser.objects.get(id=2)

        pet = Pet.objects.first()
        pet.user_id = user_another.id
        pet.save()

        url = reverse('items.delete', args=[
            pet.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.login()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_delete(self):
        user = self.login()
        pet = Pet.objects.first()
        pet.user_id = user.id
        pet.status = Pet.STATUSES.active
        pet.save()

        url = reverse('items.delete', args=[
            pet.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        deleted_pet = Pet.objects.get(pk=pet.id)
        self.assertEqual(deleted_pet.status, Pet.STATUSES.deleted)