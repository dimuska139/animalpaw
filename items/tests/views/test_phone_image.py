import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser
from items.models import Pet


class PhoneImageTest(TestCase):
    fixtures = ['initial.json', 'users.json', 'items.json']

    def login(self):
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        return user

    def logout(self):
        self.client.logout()

    def test_permissions(self):
        # Доступно вне зависимости от того, авторизован или нет
        pet = Pet.objects.first()
        url = reverse('item_phone_image', args=[
            pet.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.login()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_phone_image(self):
        pet = Pet.objects.first()
        url = reverse('item_phone_image', args=[
            pet.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertNotEqual(0, len(json_response['html']))
        self.assertContains(response, 'data:image/png;base64,')
        self.assertNotContains(response, '<img class="phone-img" src="data:image/png;base64," />')