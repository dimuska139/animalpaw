from .settings_orig import *

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dimuska139',
        'USER': 'dimuska139',
        'PASSWORD': '12345',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}


MEDIA_ROOT = os.path.join(BASE_DIR, 'media_test')
SECRET_KEY = '12345'

'''TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
    '--with-coverage',
    '--cover-package=items,extusers,articles,feedback',
]'''

EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'
CELERY_ALWAYS_EAGER = True
DEBUG = True
