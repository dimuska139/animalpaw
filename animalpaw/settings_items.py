MAX_ITEM_PHOTOS_AMOUNT = 3
MAX_ITEM_PHOTOS_SIZE = 500*1024  # bytes
MAX_ITEM_LOGO_SIZE = 2048*1024  # Mb
ITEMS_PAGESIZE = 18