from channels.routing import route
from channels import include

comments_routing = [
    route('websocket.connect', "comments.views.ws_connect"),
    route('websocket.disconnect', "comments.views.ws_disconnect"),
]

rating_routing = [
    route('websocket.connect', "utils.views.ws_connect"),
    route('websocket.disconnect', "utils.views.ws_disconnect"),
]

routing = [
    include("animalpaw.routing.comments_routing", path=r"^/ws/comments/(?P<content_type>\d+)/(?P<object_id>\d+)"),
    include("animalpaw.routing.rating_routing", path=r"^/ws/rating/(?P<content_type>\d+)/(?P<object_id>\d+)"),
]
