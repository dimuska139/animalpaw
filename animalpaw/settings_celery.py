from datetime import timedelta

BROKER_URL = 'redis://redis:6379/0'
REDIS_CONNECT_RETRY = True
CELERY_RESULT_BACKEND = 'redis://redis:6379/0'
CELERY_TASK_RESULT_EXPIRES = 7*86400  # 7 days
CELERY_SEND_EVENTS = True
CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"
CELERY_TIMEZONE = 'Europe/Moscow'
#CELERY_ENABLE_UTC = False
CELERYCAM_EXPIRE_SUCCESS = timedelta(days=1)
CELERYCAM_EXPIRE_ERROR = timedelta(days=1)
CELERYCAM_EXPIRE_PENDING = timedelta(days=1)

import djcelery
djcelery.setup_loader()
