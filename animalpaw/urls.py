"""animalpaw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.sitemaps import GenericSitemap
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.contrib import sitemaps

from articles.models import Article
from items.models import Pet
from jokes.models import Joke
from news.models import News
from . import views as main_views
from extusers import views as user_views
from items import views as items_views
from django.contrib.sitemaps.views import sitemap


class StaticViewSitemap(sitemaps.Sitemap):
    priority = 1
    changefreq = 'daily'

    def items(self):
        return ['main', 'animals.whatdog']

    def location(self, item):
        return reverse(item)


stmps = {
    'static': StaticViewSitemap,
    'pets': GenericSitemap({
            'queryset': Pet.objects.filter(status=Pet.STATUSES.active),
            'date_field': 'published_at',
        },
        priority=0.5),
    'articles': GenericSitemap({
            'queryset': Article.objects.filter(Q(published_at__lte=timezone.now()), status=Article.STATUSES.active),
            'date_field': 'published_at',
        },
        priority=0.5),
    'news': GenericSitemap({
            'queryset': News.objects.filter(Q(published_at__lte=timezone.now()), status=News.STATUSES.active),
            'date_field': 'published_at',
        },
        priority=0.5),
    #'jokes': GenericSitemap({
    #        'queryset': Joke.objects.filter(status=Joke.STATUSES.active),
    #        'date_field': 'published_at',
    #    },
    #    priority=0.5),
}


urlpatterns = [
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', admin.site.urls),
    url(r'^animals/', include('animals.urls')),
    url(r'^news/', include('news.urls')),
    url(r'^articles/', include('articles.urls')),
    url(r'^feedback/', include('feedback.urls')),
    url(r'^$', main_views.index, name='main'),
    url(r'^items/', include('items.urls')),
    url(r'^jokes/', include('jokes.urls')),
    url(r'^utils/', include('utils.urls')),
    url(r'^comments/', include('comments.urls')),
    url(r'^user/', include('extusers.urls')),
    url(r'^signin', user_views.sign_in, name='sign_in'),
    url(r'^(?P<region_alias>[-\w]+)/(?P<type_alias>[-\w]+)/(?P<kind_alias>[-\w]+)/(?P<id>\d+)', items_views.item, name='item'),
    url(r'^sitemap\.xml$', sitemap,
        {
            'sitemaps': stmps,
        },
        name='django.contrib.sitemaps.views.sitemap'),
    url(r'^robots\.txt', include('robots.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#if settings.DEBUG:
import debug_toolbar

urlpatterns += [
    url(r'^__debug__/', include(debug_toolbar.urls)),
]
