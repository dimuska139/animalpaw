# --*-- coding: utf-8 --*--
import json

from django.http import HttpResponse
from django.template import loader
from django.views.decorators.cache import cache_page
from django.contrib.contenttypes.models import ContentType

from feedback.forms import FeedbackForm
from utils.models.rating import Rating

__author__ = 'dimuska139'


#@cache_page(60 * 15)
def index(request):
    template = loader.get_template('pages/index.html')

    if request.user.is_authenticated:
        feedback_form = FeedbackForm(initial={
            'name': request.user.name,
            'email': request.user.email
        })
        context = {
            'feedback_form': feedback_form
        }
        response = HttpResponse(template.render(context, request))

        # Перемещение голосов из cookie
        cts = ContentType.objects.all()
        for content_type in cts:
            cookie_name = 'rating_' + str(content_type.id)
            if cookie_name in request.COOKIES:
                cookies = json.loads(request.COOKIES.get(cookie_name))
                if len(cookies) > 0:
                    for cookie in cookies:
                        if not Rating.objects.filter(user=request.user, content_type_id=cookie['content_type'], object_id=cookie['object_id'], status=Rating.STATUSES.active).exists():
                            Rating.objects.filter(id=cookie['id'], user=None, status=Rating.STATUSES.active).update(user=request.user)
                        else:
                            Rating.objects.filter(id=cookie['id'], user=None, status=Rating.STATUSES.active).delete()
                    response.delete_cookie(cookie_name)

    else:
        feedback_form = FeedbackForm(initial={
            'name': '',
            'email': ''
        })
        context = {
            'feedback_form': feedback_form
        }
        response = HttpResponse(template.render(context, request))

    return response


