from celery.task import task
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.template.loader import render_to_string


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def registration_confirmation(email, key, ended_at):
    current_site = Site.objects.get_current()
    html_message = render_to_string(
        'extusers/email/confirm_registration.html',
        {
            'email': email,
            'current_site': current_site,
            'confirmation_key': key,
            'end_time': ended_at
        }
    )
    send_mail('Подтверждение регистрации на сайте', '', getattr(settings, 'EMAIL_HOST_USER'), [email],
              fail_silently=False, html_message=html_message)
