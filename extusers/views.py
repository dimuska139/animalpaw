# --*-- coding: utf-8 --*--
import uuid

import os

from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.http import JsonResponse
from django.template import loader
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash, authenticate, login
from django.views.decorators.http import require_http_methods
from jsonview.decorators import json_view

from extusers.decorators import login_required_json
from extusers.decorators import only_anonymous_json
from utils.forms import get_errors
from utils.models import Image
from .forms import RegistrationForm, AuthenticationForm, ProfileForm, PasswordForm, ConfirmationForm, \
    ResetPasswordForm, NewPasswordForm
from .models import ExtUser, Confirmation

from .decorators import only_anonymous
from PIL import Image as PILimage


# Create your views here.


@login_required
@json_view
def avatar_upload(request):
    if 'image' not in request.FILES:
        response = {
            'success': False,
            'message': 'Отсутствует файл'
        }
        return response
    if request.FILES['image'].size > settings.MAX_USER_AVATAR_SIZE:
        response = {
            'success': False,
            'message': 'Максимальный размер загружаемого файла - ' + str(
                settings.MAX_USER_AVATAR_SIZE / 1024) + u' кБ',
        }
        return response
    try:
        PILimage.open(request.FILES['image'])
        avatar = Image(
            original=request.FILES['image'],
            user=request.user
        )
        avatar.save()

        response = {
            'success': True,
            'message': 'OK',
            'id': avatar.pk,
            'url': avatar.original.url,
            'width': avatar.original.width,
            'height': avatar.original.height,
        }
        return response
    except IOError as e:
        response = {
            'success': False,
            'message': 'Загруженный файл не является растровым изображением'
        }
        return response


@login_required
def profile(request):
    user = ExtUser.objects.select_related('avatar').get(status=1, id=request.user.id)
    profile_form = ProfileForm(instance=user)
    password_form = PasswordForm(instance=user)
    context = {
        'user': user,
        'avatar': user.avatar,
        'profile_form': profile_form,
        'password_form': password_form
    }
    return render(request, 'extusers/pages/profile.html', context)


@json_view
@login_required_json
def profile_update(request):
    user = ExtUser.objects.select_related('avatar').get(status=1, id=request.user.id)
    profile_form = ProfileForm(request.POST, instance=user)

    if profile_form.is_valid():
        user = profile_form.save()
        if 'avatar' in request.POST and request.POST['avatar'].isnumeric():
            user.avatar.make_logo(x1=request.POST['avatar_x1'],
                                  y1=request.POST['avatar_y1'],
                                  x2=request.POST['avatar_x2'],
                                  y2=request.POST['avatar_y2'])

        return {
            'success': True,
            'message': 'Изменения сохранены',
        }

    response = {
        'success': False,
        'message': '',
        'errors': get_errors(profile_form)
    }
    return JsonResponse(response)


@json_view
@login_required_json
def profile_changepassword(request):
    user = ExtUser.objects.get(status=ExtUser.STATUSES.active, id=request.user.id)
    password_form = PasswordForm(request.POST, instance=user)

    if password_form.is_valid():
        user = password_form.save()
        update_session_auth_hash(request, user)
        return {
            'success': True,
            'message': 'Пароль успешно изменён'
        }

    response = {
        'success': False,
        'message': '',
        'errors': get_errors(password_form)
    }
    return JsonResponse(response)


@json_view
def check_email(request):
    response = {
        'success': True,
        'message': '',
    }
    if 'email' not in request.POST:
        response = {
            'success': False,
            'message': 'Email required',
        }
        return response
    if not request.user.is_authenticated:
        if ExtUser.objects.filter(email=request.POST['email'], is_confirmed=1).exists():
            response = {
                'success': False,
                'message': 'Пользователь с таким email уже зарегистрирован',
            }
    else:
        if ExtUser.objects.filter(email=request.POST['email'], is_confirmed=1).exclude(pk=request.user.id).exists():
            response = {
                'success': False,
                'message': 'Этот e-mail уже занят',
            }
    return response


@only_anonymous
def password_restore(request):
    reset_password_form = ResetPasswordForm()
    context = {
        'reset_password_form': reset_password_form
    }
    return render(request, 'extusers/pages/reset_password.html', context)


@only_anonymous
@json_view
def reset_password(request):
    reset_password_form = ResetPasswordForm(request.POST)
    if reset_password_form.is_valid():
        reset_password_form.reset()
        return {
            'success': True,
            'url': reverse('users.password.reset.completed')
        }
    return {
        'success': False,
        'errors': get_errors(reset_password_form)
    }


@only_anonymous
def reset_password_completed(request):
    return render(request, 'extusers/pages/reset_password_completed.html')


@only_anonymous
def new_password(request, key):
    try:
        confirmation = Confirmation.objects.get(status=1, key=key, type=Confirmation.TYPES.reset_password)
        user = ExtUser.objects.get(pk=confirmation.user_id)
        if timezone.now() > confirmation.ended_at:  # Если ссылка просрочена
            # Поиск старых попыток сбросить пароль (их надо удалять)
            Confirmation.objects.filter(status=1, type=Confirmation.TYPES.reset_password, user_id=user.pk).update(
                status=Confirmation.STATUSES.deleted)

            context = {
                'date': confirmation.ended_at
            }
            return render(request, 'extusers/pages/new_password_outdated_confirmation_key.html', context)
        context = {
            'confirmation_key': confirmation.key,
            'new_password_form': NewPasswordForm()
        }

        return render(request, 'extusers/pages/new_password.html', context)
    except Confirmation.DoesNotExist:  # Несуществующий ключ
        return render(request, 'extusers/pages/new_password_incorrect_confirmation_key.html')

@json_view
@only_anonymous_json
def new_password_save(request, key):
    try:
        confirmation = Confirmation.objects.get(status=1, key=key, type=Confirmation.TYPES.reset_password)
        user = ExtUser.objects.get(pk=confirmation.user_id)

        if timezone.now() > confirmation.ended_at:  # Если ссылка просрочена
            return {
                'success': False
            }

        new_password_form = NewPasswordForm(request.POST)

        if not new_password_form.is_valid():
            return {
                'success': False,
                'errors': get_errors(new_password_form)
            }

        user.set_password(request.POST['password1'])
        user.save()
        # Поиск старых попыток сбросить пароль (их надо удалять)
        Confirmation.objects.filter(status=1, type=Confirmation.TYPES.reset_password, user_id=user.pk).update(
            status=Confirmation.STATUSES.deleted)

        current_site = Site.objects.get_current()
        html_message = render_to_string(
            'extusers/email/new_password.html',
            {
                'current_site': current_site,
                'email': user.email,
                'password': request.POST['password1']
            }
        )
        send_mail('Ваш новый пароль', '', getattr(settings, 'EMAIL_HOST_USER'), [user.email],
                  fail_silently=False, html_message=html_message)

        return {
            'success': True,
            'url': reverse('users.password.new.completed')
        }
    except Confirmation.DoesNotExist:
        return {
            'success': False
        }


@only_anonymous
def new_password_completed(request):
    return render(request, 'extusers/pages/new_password_completed.html')


@json_view
@only_anonymous_json
def registration(request):
    registration_form = RegistrationForm(request.POST)
    if registration_form.is_valid():
        not_confirmed_user = ExtUser.objects.filter(email=request.POST['email'],
                                                    is_confirmed=False).first()
        if not_confirmed_user:
            confirmation = Confirmation(user_id=not_confirmed_user.pk, type=Confirmation.TYPES.registration)
            confirmation.save()
            not_confirmed_user.set_password(request.POST['password1'])
            not_confirmed_user.save()
        else:
            if registration_form.is_valid():
                registration_form.save()
        return {
            'success': True,
            'url': reverse('users.registration.completion')
        }
    return {
        'success': False,
        'errors': get_errors(registration_form)
    }


@only_anonymous
def registration_completion(request):
    return render(request, 'extusers/pages/registration_completion.html')


def registration_confirmation(request, key):
    try:
        confirmation = Confirmation.objects.get(status=Confirmation.STATUSES.active, key=key,
                                                type=Confirmation.TYPES.registration)
        user = ExtUser.objects.get(pk=confirmation.user_id)

        if timezone.now() > confirmation.ended_at:
            # Поиск старых попыток сбросить пароль (их надо удалять)
            Confirmation.objects.filter(status=1, type=Confirmation.TYPES.registration, user_id=user.pk).update(
                status=Confirmation.STATUSES.deleted)

            context = {
                'date': confirmation.ended_at
            }
            return render(request, 'extusers/pages/registration_outdated_confirmation_key.html', context)

        confirmation_form = ConfirmationForm(initial={
            'name': '',
            'phone': '',
            'description': '',
        })

        context = {
            'user': user,
            'confirmation_key': confirmation.key,
            'confirmation_form': confirmation_form
        }
        return render(request, 'extusers/pages/registration_confirmation.html', context)
    except Confirmation.DoesNotExist:
        return render(request, 'extusers/pages/registration_incorrect_confirmation_key.html')


@json_view
def registration_confirm(request, key):
    try:
        confirmation = Confirmation.objects.get(status=Confirmation.STATUSES.active, key=key,
                                                type=Confirmation.TYPES.registration)
        user = ExtUser.objects.get(pk=confirmation.user_id)

        if timezone.now() > confirmation.ended_at:  # Если ссылка просрочена
            return {
                'success': False
            }

        confirmation_form = ConfirmationForm(request.POST, instance=user)

        if not confirmation_form.is_valid():
            return {
                'success': False,
                'errors': get_errors(confirmation_form)
            }
        confirmation_form.save()

        # Поиск старых попыток зарегистрироваться (их надо удалять)
        Confirmation.objects.filter(status=1, type=Confirmation.TYPES.registration, user_id=user.pk).update(
            status=Confirmation.STATUSES.deleted)
        # user.groups.add(Group.objects.get(name='users'))
        return {
            'success': True,
            'url': reverse('users.registration.completed')
        }
    except Confirmation.DoesNotExist:
        return {
            'success': False
        }


def registration_completed(request):
    return render(request, 'extusers/pages/registration_completed.html')


@json_view
@only_anonymous_json
def login(request):
    login_form = AuthenticationForm(data=request.POST)

    if login_form.is_valid():
        user = auth.authenticate(username=login_form.cleaned_data.get('username'),
                                 password=login_form.cleaned_data.get('password'))
        auth.login(request, user)
        if 'next' in request.POST:
            return {
                'url': request.POST['next'],
                'success': True,
            }
        else:
            return_path = request.META.get('HTTP_REFERER', '/')
            return {
                'url': return_path,
                'success': True
            }
    return {
        'success': False,
        'errors': get_errors(login_form)
    }


@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('main'))


# @cache_page(60 * 15)
@only_anonymous
@csrf_protect
def sign_in(request):
    login_form = AuthenticationForm()
    registration_form = RegistrationForm()

    if 'next' in request.GET:
        next = request.GET['next']
    else:
        next = False

    context = {
        'next': next,
        'login_form': login_form,
        'registration_form': registration_form
    }
    return render(request, 'pages/sign_in.html', context)
