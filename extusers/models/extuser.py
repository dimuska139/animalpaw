from django.apps import apps
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from model_utils import Choices
from extusers.managers import User as UserManager

import utils.models


class ExtUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('Электронная почта', max_length=255, unique=True, db_index=True)
    username = models.CharField(max_length=300, blank=True)  # ВК-логин
    name = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    created_at = models.DateField('Дата регистрации', auto_now_add=True)
    is_confirmed = models.BooleanField('Подтверждена', default=False)
    is_activated = models.BooleanField('Активирована', default=False)
    avatar = models.ForeignKey('utils.Image', on_delete=models.SET_NULL, null=True, blank=True)
    phone = models.CharField(max_length=17, blank=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def get_full_name(self):
        return self.name

    @property
    def is_staff(self):
        return self.is_superuser

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.email

    def has_module_perms(self, app_label):
        return True

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'