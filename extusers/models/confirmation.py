import string

import random
from django.apps import apps
from django.conf import settings
from django.db import models
from django.utils import timezone
from model_utils import Choices

from extusers.tasks import reset_password_confirmation, registration_confirmation


class Confirmation(models.Model):
    user = models.ForeignKey('extusers.ExtUser', null=True)
    key = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    ended_at = models.DateTimeField()

    TYPES = Choices(
        (0, 'registration', 'Регистрация'),
        (1, 'reset_password', 'Сброс пароля'))

    type = models.IntegerField(choices=TYPES, default=TYPES.registration)

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.user.email

    def save(self, send_message=True, **kwargs):
        self.key = ''.join(
            random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for x in range(30))

        new_date = timezone.now()

        if self.ended_at is None:
            if self.type == self.TYPES.registration:
                delta = timezone.timedelta(days=getattr(settings, "REGISTRATION_TIMEOUT_DAYS", 7))
                self.ended_at = new_date + delta
            if self.type == self.TYPES.reset_password:
                delta = timezone.timedelta(days=getattr(settings, "RESETPASSWORD_TIMEOUT_DAYS", 7))
                self.ended_at = new_date + delta

        super(Confirmation, self).save()

        if send_message:
            if self.type == self.TYPES.registration:
                registration_confirmation.delay(self.user.email, self.key, self.ended_at)
            if self.type == self.TYPES.reset_password:
                reset_password_confirmation.delay(self.user.email, self.key, self.ended_at)
