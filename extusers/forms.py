# -- * -- coding: utf8 -- * --
from string import ascii_uppercase, ascii_letters

from django.conf import settings
from random import choice

__author__ = 'dimuska139'
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm as BaseAuthenticationForm
from django.utils.text import capfirst
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth import authenticate
from .models import Confirmation, ExtUser


class NewPasswordForm(forms.Form):
    password1 = forms.CharField(
        label='Новый пароль',
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label='Подтверждение',
        widget=forms.PasswordInput
    )

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != password2:
            raise forms.ValidationError(
                'Пароли не совпадают'
            )
        return self.cleaned_data


class ResetPasswordForm(forms.Form):
    email = forms.EmailField(label='Email')

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            user = ExtUser.objects.get(status=ExtUser.STATUSES.active, email=email)
            if not user.is_confirmed:
                raise forms.ValidationError('Эта учётная запись ещё не подтверждена пользователем')
        except ExtUser.DoesNotExist:
            pass
            #raise forms.ValidationError('Пользователя с таким email не существует')

        return email

    def reset(self):
        try:
            user = ExtUser.objects.get(status=1, email=self.cleaned_data['email'])
            confirmation = Confirmation(user_id=user.pk, type=Confirmation.TYPES.reset_password)
            confirmation.save()
        except ExtUser.DoesNotExist:
            pass


class ProfileForm(forms.ModelForm):
#    def __init__(self, *args, **kwargs):
#        super(forms.ModelForm, self).__init__(*args, **kwargs)

    class Meta:
        model = get_user_model()
        fields = ('email', 'name', 'phone', 'avatar', 'description',)

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Ваше имя'
        self.fields['phone'].label = 'Телефон'
        self.fields['avatar'].label = 'Изображение профиля'
        self.fields['description'].label = 'О вас'


class PasswordForm(forms.ModelForm):
    password = forms.CharField(
        label='Старый пароль',
        widget=forms.PasswordInput
    )

    password1 = forms.CharField(
        label='Новый пароль',
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label='Подтверждение',
        widget=forms.PasswordInput
    )

    def clean_password(self):
        password = self.cleaned_data.get('password')
        auth_status = authenticate(username=self.instance.email, password=password)
        if auth_status is None:
            raise forms.ValidationError('Старый пароль введён неверно')
        return password

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != password2:
            raise forms.ValidationError('Пароли не совпадают')
        return self.cleaned_data

    def save(self, commit=True):
        user = super(PasswordForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('password1', 'password2',)


class ConfirmationForm(forms.ModelForm):
    password1 = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label='Подтверждение',
        widget=forms.PasswordInput
    )

    name = forms.CharField(
        widget=forms.TextInput
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Пароль и подтверждение не совпадают')
        return password2

    class Meta:
        model = get_user_model()
        fields = ('password1', 'password2', 'name', 'phone', 'description', )

    def save(self, commit=True):
        user = super(ConfirmationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.is_activated = True
        user.is_confirmed = True
        user.save()
        return user

    def __init__(self, *args, **kwargs):
        super(ConfirmationForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Ваше имя'
        self.fields['phone'].label = 'Телефон'
        self.fields['description'].label = 'О Вас'


class RegistrationForm(forms.ModelForm):
    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.is_confirmed = False
        user.set_password(''.join(choice(ascii_letters) for i in range(10)))

        if commit:
            user.save()
            confirmation = Confirmation(user_id=user.pk, type=Confirmation.TYPES.registration)
            confirmation.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('email',)


class AuthenticationForm(BaseAuthenticationForm):
    username = forms.EmailField(max_length=254, label='E-mail')
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': "Неправильный email или пароль",  #_("Please enter a correct email and password"),
        'inactive': "Учётная запись не активна",  #_("This account is inactive."),
        'unconfirmed': "Учётная запись не подтверждена",  #_("This account is unconirmed."),
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login'
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        '''if not user.is_activated:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )'''

        if not user.is_confirmed:
            raise forms.ValidationError(
                self.error_messages['unconfirmed'],
                code='unconfirmed',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache
