from django.contrib import admin

# Register your models here.
from extusers.models import ExtUser, Confirmation

admin.site.register((ExtUser, Confirmation, ))