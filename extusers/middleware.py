from social.apps.django_app.middleware import SocialAuthExceptionMiddleware
from django.shortcuts import redirect
from social.exceptions import AuthCanceled


class CustomSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_exception(self, request, exception):
        if type(exception) == AuthCanceled:
            return redirect('main')
        else:
            pass