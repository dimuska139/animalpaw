from django.test import TestCase

from extusers.forms import RegistrationForm


class RegistrationTest(TestCase):
    def test_empty(self):
        data = {}
        form = RegistrationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_empty_email(self):
        data = {
        }
        form = RegistrationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_correct(self):
        data = {
            'email': 'test@test.test'
        }
        form = RegistrationForm(data=data)
        self.assertTrue(form.is_valid())
