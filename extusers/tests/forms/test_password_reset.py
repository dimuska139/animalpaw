from django.test import TestCase

from extusers.forms import ResetPasswordForm
from extusers.models import ExtUser


class ResetPasswordTest(TestCase):
    fixtures = ['users.json']

    def test_correct(self):
        exists_user = ExtUser.objects.first()
        data = {
            'email': exists_user.email
        }
        form = ResetPasswordForm(data=data)
        self.assertTrue(form.is_valid())

    def test_not_confirmed_user(self):
        exists_user = ExtUser.objects.first()
        exists_user.is_confirmed = False
        exists_user.save()

        data = {
            'email': exists_user.email
        }
        form = ResetPasswordForm(data=data)
        self.assertFalse(form.is_valid())
