from django.test import TestCase

from extusers.forms import ConfirmationForm


class ConfirmationTest(TestCase):
    def test_empty(self):
        data = {}
        form = ConfirmationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_empty_paswords(self):
        data = {
            'password1': '',
            'password2': '',
            'name': 'Дмитрий Алексеевич',
            'phone': '+7(921)123-45-67',
            'description': 'Разработчик, создатель проекта'
        }
        form = ConfirmationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_not_equal_passwords(self):
        data = {
            'password1': '12345',
            'password2': '123ab',
            'name': 'Дмитрий Алексеевич',
            'phone': '+7(921)123-45-67',
            'description': 'Разработчик, создатель проекта'
        }
        form = ConfirmationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_correct(self):
        data = {
            'password1': '12345',
            'password2': '12345',
            'name': 'Дмитрий Алексеевич',
            'phone': '+7(921)123-45-67',
            'description': 'Разработчик, создатель проекта'
        }
        form = ConfirmationForm(data=data)
        self.assertTrue(form.is_valid())
