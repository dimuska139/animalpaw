from django.test import TestCase

from extusers.forms import NewPasswordForm


class NewPasswordTest(TestCase):
    fixtures = ['users.json']

    def test_correct(self):
        data = {
            'password1': '12345',
            'password2': '12345',
        }
        form = NewPasswordForm(data=data)
        self.assertTrue(form.is_valid())

    def test_not_equals(self):
        data = {
            'password1': '12345',
            'password2': '54321',
        }
        form = NewPasswordForm(data=data)
        self.assertFalse(form.is_valid())
