from django.test import TestCase

from extusers.forms import ProfileForm


class ProfileTest(TestCase):
    def test_correct(self):
        data = {
            'email': 'test@test.ru',
            'name': 'Дмитрий Алексеевич',
            'phone': '+7(921)123-45-67',
            'description': 'Разработчик, создатель проекта',
            'avatar_id': 1,
            'avatar_x1': 0,
            'avatar_y1': 0,
            'avatar_x2': 50,
            'avatar_y2': 50
        }
        form = ProfileForm(data=data)
        self.assertTrue(form.is_valid())

    def test_no_avatar(self):
        data = {
            'email': 'test@test.ru',
            'name': 'Дмитрий Алексеевич',
            'phone': '+7(921)123-45-67',
            'description': 'Разработчик, создатель проекта',
        }
        form = ProfileForm(data=data)
        self.assertTrue(form.is_valid())

    def test_no_email(self):
        data = {
            'email': '',
            'name': 'Дмитрий Алексеевич',
            'phone': '+7(921)123-45-67',
            'description': 'Разработчик, создатель проекта',
        }
        form = ProfileForm(data=data)
        self.assertFalse(form.is_valid())