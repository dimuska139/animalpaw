from django.test import TestCase

from extusers.forms import AuthenticationForm
from extusers.models import ExtUser


class AuthenticationTest(TestCase):
    fixtures = ['users.json']

    def test_empty(self):
        data = {}
        form = AuthenticationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_incorrect_password(self):
        exists_user = ExtUser.objects.first()
        data = {
            'username': exists_user.email,
            'password': 'incorrect'
        }
        form = AuthenticationForm(data=data)
        self.assertFalse(form.is_valid())

    def test_correct(self):
        exists_user = ExtUser.objects.first()
        data = {
            'username': exists_user.email,
            'password': '12345'
        }
        form = AuthenticationForm(data=data)
        self.assertTrue(form.is_valid())