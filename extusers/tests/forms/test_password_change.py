from django.test import TestCase

from extusers.forms import PasswordForm
from extusers.models import ExtUser


class PasswordChangeTest(TestCase):
    fixtures = ['users.json']

    def test_empty(self):
        data = {
            'password': '',
            'password1': '',
            'password2': '',
        }
        form = PasswordForm(data=data)
        self.assertFalse(form.is_valid())

    def test_empty_old_password(self):
        data = {
            'password': '',
            'password1': '12345',
            'password2': '12345',
        }
        form = PasswordForm(data=data)
        self.assertFalse(form.is_valid())

    def test_not_equal_passwords(self):
        data = {
            'password': '12345',
            'password1': '12345',
            'password2': '54321',
        }
        form = PasswordForm(data=data)
        self.assertFalse(form.is_valid())

    def test_correct(self):
        exists_user = ExtUser.objects.first()
        data = {
            'password': '12345',
            'password1': '54321',
            'password2': '54321',
        }
        form = PasswordForm(data=data)
        form.instance.email = exists_user.email
        self.assertTrue(form.is_valid())