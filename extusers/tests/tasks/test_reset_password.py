from datetime import timedelta

from django.core import mail
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from extusers.tasks import reset_password_confirmation
from extusers.models import ExtUser


class ResetPasswordTest(TestCase):
    fixtures = ['users.json']

    def test_reset_password(self):
        key = 'randkey'

        user = ExtUser.objects.get(pk=1)
        self.assertEqual(len(mail.outbox), 0)
        result = reset_password_confirmation.delay(user.email, key, timezone.now() + timedelta(days=10))
        self.assertTrue(result.successful())
        self.assertEqual(len(mail.outbox), 1)

        new_password_url = reverse('users.password.new', args=[
            key
        ])

        # Ссылка должна присутствовать в письме
        self.assertTrue(new_password_url in mail.outbox[0].alternatives[0][0])
