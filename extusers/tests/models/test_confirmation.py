from django.core import mail
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from extusers.models import Confirmation, ExtUser


class ConfirmationTest(TestCase):
    fixtures = ['initial.json', 'users.json', ]

    def test_creation(self):
        self.assertEqual(len(mail.outbox), 0)
        confirmation = Confirmation.objects.create(
            type=Confirmation.TYPES.registration,
            ended_at=timezone.now(),
            user=ExtUser.objects.first()
        )
        self.assertEqual(confirmation.type, Confirmation.TYPES.registration)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Подтверждение регистрации на сайте')
        confirmation_url = reverse('users.registration.confirmation', args=[
            confirmation.key
        ])
        self.assertTrue(confirmation_url in mail.outbox[0].alternatives[0][0])
