from django.test import TestCase

from extusers.models import ExtUser


class UserTest(TestCase):
    def test_creation(self):
        user = ExtUser.objects.create_user(
            username='Дмитрий Алексеевич',
            email='dimuska139@mail.ru',
            password='12345'
        )
        self.assertTrue(isinstance(user, ExtUser))
        self.assertEqual(user.__str__(), user.email)
        self.assertEqual(user.status, ExtUser.STATUSES.active)
        self.assertFalse(user.is_superuser)
        self.assertFalse(user.is_confirmed)
