from django.test import TestCase

from extusers.models import ExtUser


class SuperuserTest(TestCase):
    def test_creation(self):
        user = ExtUser.objects.create_superuser(
            username='Дмитрий Алексеевич',
            email='superuser@mail.ru',
            password='12345'
        )

        self.assertTrue(isinstance(user, ExtUser))
        self.assertEqual(user.__str__(), user.email)
        self.assertEqual(user.status, ExtUser.STATUSES.active)
        self.assertTrue(user.is_admin)
        self.assertTrue(user.is_superuser)
        self.assertFalse(user.is_confirmed)