import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class CheckEmailTest(TestCase):
    fixtures = ['users.json']

    def test_access(self):
        url = reverse('check_email')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_empty_email(self):
        url = reverse('check_email')

        response = self.client.post(url)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertTrue('required' in json_response['message'])

    def test_not_free_email_auth(self):
        url = reverse('check_email')
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')

        another_user = ExtUser.objects.get(pk=2)
        response = self.client.post(url, {
            'email': another_user.email
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_not_free_email(self):
        url = reverse('check_email')

        user = ExtUser.objects.get(pk=1)
        response = self.client.post(url, {
            'email': user.email
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_user_email_auth(self):
        url = reverse('check_email')
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')

        response = self.client.post(url, {
            'email': user.email
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

    def test_new_email(self):
        url = reverse('check_email')
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')

        response = self.client.post(url, {
            'email': 'new-email@test.test'
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
