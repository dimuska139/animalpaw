import json

from django.core import mail
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class PasswordResetTest(TestCase):
    fixtures = ['users.json']

    def test_access(self):
        url = reverse('users.password.reset')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_auth_access(self):
        url = reverse('users.password.reset')
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_not_existing_email(self):
        url = reverse('users.password.reset')
        response = self.client.post(url, {
            'email': 'not-exists@test.test'
        })
        self.assertTrue(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('users.password.reset.completed'))

    def test_not_confirmed_user(self):
        url = reverse('users.password.reset')
        user = ExtUser.objects.get(id=1)
        user.is_confirmed = False
        user.save()
        response = self.client.post(url, {
            'email': user.email
        })
        self.assertTrue(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertFalse('url' in json_response)
        self.assertTrue('errors' in json_response)
        self.assertNotEqual(0, len(json_response['errors']))

    def test_confirmed_user(self):
        url = reverse('users.password.reset')
        user = ExtUser.objects.get(id=1)

        self.assertEqual(len(mail.outbox), 0)
        response = self.client.post(url, {
            'email': user.email
        })

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertTrue(response.status_code, 200)
        self.assertEqual(json_response['url'], reverse('users.password.reset.completed'))

        self.assertEqual(len(mail.outbox), 1)

        success_text = 'Для того, чтобы создать новый пароль, перейдите по следующей ссылке:'
        self.assertTrue(success_text in mail.outbox[0].alternatives[0][0])
