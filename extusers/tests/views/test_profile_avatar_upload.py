import json

import os
from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class ProfileAvatarUploadTest(TestCase):
    fixtures = ['users.json']

    def test_access(self):
        url = reverse('users.profile.avatar.upload')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_auth_access(self):
        url = reverse('users.profile.avatar.upload')
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_no_image(self):
        url = reverse('users.profile.avatar.upload')
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.post(url)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertTrue('Отсутствует' in json_response['message'])

    def test_incorrect_image(self):
        url = reverse('users.profile.avatar.upload')
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        incorrect_files = ('python_file.py', 'php_file.php', 'bigsize_img.png',)
        for file_name in incorrect_files:
            with open('./test_files/' + file_name, 'rb') as file:
                media_before = os.listdir(settings.MEDIA_ROOT)
                response = self.client.post(url, {
                    'image': file
                })
                self.assertEqual(response.status_code, 200)
                self.assertEqual(0, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))
                json_response = json.loads(response.content.decode('utf-8'))
                self.assertFalse(json_response['success'])

    def test_correct_image(self):
        url = reverse('users.profile.avatar.upload')
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        with open('./test_files/correct_img.jpg', 'rb') as file:
            media_before = os.listdir(settings.MEDIA_ROOT)
            response = self.client.post(url, {
                'image': file
            })
            self.assertEqual(response.status_code, 200)
            self.assertEqual(1, len(os.listdir(settings.MEDIA_ROOT)) - len(media_before))
            json_response = json.loads(response.content.decode('utf-8'))
            self.assertTrue(json_response['success'])
            self.assertTrue('id' in json_response)
            self.assertTrue('url' in json_response)
            self.assertTrue('width' in json_response)
            self.assertTrue('height' in json_response)
