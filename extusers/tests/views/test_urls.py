from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('sign_in'), '/signin')
        self.assertEqual(reverse('profile'), '/user/profile')
        self.assertEqual(reverse('users.password.restore'), '/user/password/restore')
        self.assertEqual(reverse('users.password.reset'), '/user/password/reset')
        self.assertEqual(reverse('users.password.reset.completed'), '/user/password/reset/completed')
        self.assertEqual(reverse('users.password.new', args=['test']), '/user/password/new/test')
        self.assertEqual(reverse('users.password.new.completed'), '/user/password/new/completed')
        self.assertEqual(reverse('users.profile.avatar.upload'), '/user/profile/avatar/upload')
        self.assertEqual(reverse('check_email'), '/user/check_email')
        self.assertEqual(reverse('logout'), '/user/logout')
        self.assertEqual(reverse('users.login'), '/user/login')
        self.assertEqual(reverse('users.registration.confirmation', args=['test']), '/user/registration/confirmation/test')
        self.assertEqual(reverse('users.registration.confirm', args=['test']), '/user/registration/confirm/test')
        self.assertEqual(reverse('users.profile.password.change'), '/user/profile/password/change')
        self.assertEqual(reverse('users.profile.update'), '/user/profile/update')
        self.assertEqual(reverse('users.registration'), '/user/registration')
        self.assertEqual(reverse('users.registration.completion'), '/user/registration/completion')
