import json

from django.core import mail
from django.test import TestCase
from django.urls import reverse

from extusers.models import Confirmation, ExtUser


class RegistrationTest(TestCase):
    fixtures = ['users.json']

    def test_registration_exists_email(self):
        user = ExtUser.objects.get(pk=1)
        url = reverse('users.registration')
        self.assertEqual(len(mail.outbox), 0)
        response = self.client.post(url, {
            'email': user.email
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertFalse('url' in json_response)

    def test_registration_incorrect_email(self):
        url = reverse('users.registration')
        self.assertEqual(len(mail.outbox), 0)
        response = self.client.post(url, {
            'email': 'testtest.test'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertFalse('url' in json_response)

    def test_registration_correct(self):
        url = reverse('users.registration')
        self.assertEqual(len(mail.outbox), 0)
        conf_before = Confirmation.objects.count()
        response = self.client.post(url, {
            'email': 'new-user@test.test'
        })
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertNotEqual(len(json_response['url']), 0)
        self.assertEqual(json_response['url'], reverse('users.registration.completion'))

        self.assertEqual(len(mail.outbox), 1)
        self.assertNotEqual(conf_before, Confirmation.objects.count())

        confirmation = Confirmation.objects.last()
        self.assertNotEqual(0, confirmation.key)
        self.assertEqual(confirmation.type, Confirmation.TYPES.registration)

        confirmation_url = reverse('users.registration.confirmation', args=[
            confirmation.key
        ])

        self.assertTrue(confirmation_url in mail.outbox[0].alternatives[0][0])
