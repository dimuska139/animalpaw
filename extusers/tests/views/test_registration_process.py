import json
from datetime import timedelta

from django.core import mail
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from extusers.models import ExtUser, Confirmation


class RegistrationProcessTest(TestCase):
    def test_process(self):
        new_email = 'new-user@test.test'
        new_pass = '12345'

        response = self.client.post(reverse('users.registration'), {
            'email': new_email
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('users.registration.completion'))

        # Должен быть создан неподтверждённый простой (не администратор) пользователь
        new_user = ExtUser.objects.last()
        self.assertEqual(new_user.email, new_email)
        self.assertFalse(new_user.is_confirmed)
        self.assertFalse(new_user.is_activated)
        self.assertFalse(new_user.is_superuser)
        self.assertEqual(ExtUser.STATUSES.active, new_user.status)
        # Не должно быть прав на создание и редактирование статей
        self.assertFalse(any((new_user.has_perm('articles.add_article'), new_user.has_perm('articles.change_article'))))

        # Должно быть отправлено письмо для подтверждения
        self.assertEqual(len(mail.outbox), 1)

        confirmation = Confirmation.objects.last()
        self.assertEqual(confirmation.status, Confirmation.STATUSES.active)
        confirmation_url = reverse('users.registration.confirmation', args=[
            confirmation.key
        ])

        # Ссылка должна присутствовать в письме
        self.assertTrue(confirmation_url in mail.outbox[0].alternatives[0][0])

        # Переход по ссылке из письма
        confirmation_url = reverse('users.registration.confirmation', args=[
            confirmation.key
        ])
        response = self.client.get(confirmation_url)
        self.assertEqual(response.status_code, 200)

        # Пользователь не должен измениться
        new_user = ExtUser.objects.get(id=new_user.id)
        self.assertEqual(new_user.email, new_email)
        self.assertFalse(new_user.is_confirmed)
        self.assertFalse(new_user.is_activated)

        # Ссылка для подтверждения не должна удалиться
        confirmation = Confirmation.objects.get(id=confirmation.id)
        self.assertEqual(confirmation.status, Confirmation.STATUSES.active)

        confirm_url = reverse('users.registration.confirm', args=[
            confirmation.key
        ])
        # Завершение регистрации
        response = self.client.post(confirm_url, {
            'password1': new_pass,
            'password2': new_pass,
            'name': 'Dima',
            'phone': '8(921)543-32-23',
            'description': 'Test user',
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertEqual(json_response['url'], reverse('users.registration.completed'))

        # Ссылка для подтверждения должна быть удалена
        changed_confirmation = Confirmation.objects.get(id=confirmation.id)
        self.assertEqual(changed_confirmation.status, Confirmation.STATUSES.deleted)

        # Пользователь должен быть подтверждён
        new_user = ExtUser.objects.get(id=new_user.id)
        self.assertEqual(new_user.email, new_email)
        self.assertTrue(new_user.is_confirmed)
        self.assertTrue(new_user.is_activated)

        # Пытаемся авторизоваться
        self.assertTrue(self.client.login(username=new_email, password=new_pass))

    def test_outdated_key(self):
        new_email = 'new-user@test.test'
        new_pass = '12345'

        response = self.client.post(reverse('users.registration'), {
            'email': new_email
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        confirmation = Confirmation.objects.last()
        confirmation.ended_at = timezone.now() - timedelta(days=200)
        confirmation.save()

        confirmation_url = reverse('users.registration.confirmation', args=[
            confirmation.key
        ])

        # Переход по ссылке из письма
        response = self.client.get(confirmation_url)
        self.assertContains(response, '<h2>Ошибка</h2>', status_code=200)
        self.assertContains(response, 'устарела', status_code=200)

        confirm_url = reverse('users.registration.confirm', args=[
            confirmation.key
        ])
        response = self.client.post(confirm_url, {
            'password1': new_pass,
            'password2': new_pass,
            'name': 'Dima',
            'phone': '8(921)543-32-23',
            'description': 'Test user',
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertTrue('url' not in json_response)

    def test_incorrect_key(self):
        new_pass = '12345'
        incorrect_key = 'testkey'  # Неправильный ключ
        confirmation_url = reverse('users.registration.confirmation', args=[
            incorrect_key
        ])
        response = self.client.get(confirmation_url)
        self.assertContains(response, '<h2>Ошибка</h2>', status_code=200)
        self.assertContains(response, 'что-то пошло не так', status_code=200)

        confirm_url = reverse('users.registration.confirm', args=[
            incorrect_key
        ])
        response = self.client.post(confirm_url, {
            'password1': new_pass,
            'password2': new_pass,
            'name': 'Dima',
            'phone': '8(921)543-32-23',
            'description': 'Test user',
        })
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertTrue('url' not in json_response)
