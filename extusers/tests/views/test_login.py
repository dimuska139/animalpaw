import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class LoginTest(TestCase):
    fixtures = ['users.json']

    def test_authenticated(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        response = self.client.get(reverse('users.login'))
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_invalid_password(self):
        user = ExtUser.objects.first()
        url = reverse('users.login')
        response = self.client.post(url, {
            'username': user.email,
            'password': '54321'
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertFalse('url' in json_response)

    def test_login(self):
        user = ExtUser.objects.first()
        url = reverse('users.login')
        response = self.client.post(url, {
            'username': user.email,
            'password': '12345'
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertNotEqual(len(json_response['url']), 0)

