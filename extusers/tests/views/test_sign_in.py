from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class SignInTest(TestCase):
    fixtures = ['users.json']

    def test_access(self):
        response = self.client.get(reverse('sign_in'))
        self.assertEqual(response.status_code, 200)

    def test_auth_access(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        response = self.client.get(reverse('sign_in'))
        self.assertEqual(response.status_code, 302)