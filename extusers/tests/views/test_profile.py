import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class ProfileTest(TestCase):
    fixtures = ['users.json']

    def test_access(self):
        url = reverse('profile')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_auth_access(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        response = self.client.get(reverse('profile'))
        self.assertEqual(response.status_code, 200)

    def test_update_access(self):
        url = reverse('users.profile.update')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_update_auth_access(self):
        url = reverse('users.profile.update')
        user = ExtUser.objects.first()

        self.client.login(username=user.email, password='12345')
        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_update(self):
        url = reverse('users.profile.update')
        user = ExtUser.objects.get(pk=1)

        self.client.login(username=user.email, password='12345')

        response = self.client.post(url)
        self.assertEqual(response.status_code, 200)

    def test_update_exists_email(self):
        url = reverse('users.profile.update')

        user = ExtUser.objects.get(pk=1)

        self.client.login(username=user.email, password='12345')

        another_user = ExtUser.objects.get(pk=2)
        response = self.client.post(url, {
            'email': another_user.email
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_update_name(self):
        url = reverse('users.profile.update')

        user = ExtUser.objects.get(pk=1)

        self.client.login(username=user.email, password='12345')

        name = 'Test Name'
        response = self.client.post(url, {
            'email': user.email,
            'phone': user.phone,
            'name': name
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        self.assertEqual(ExtUser.objects.get(id=1).name, name)
        self.assertNotEqual(ExtUser.objects.get(id=2).name, name)