import json

from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class ProfilePasswordTest(TestCase):
    fixtures = ['users.json']

    def test_access(self):
        url = reverse('users.profile.password.change')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_auth_access(self):
        user = ExtUser.objects.first()

        self.client.login(username=user.email, password='12345')
        response = self.client.get(reverse('users.profile.password.change'))
        self.assertEqual(response.status_code, 200)

    def test_incorrect_old_password(self):
        user = ExtUser.objects.first()

        self.client.login(username=user.email, password='12345')

        url = reverse('users.profile.password.change')
        response = self.client.post(url, {
            'password': 'incorrect_password',
            'password1': 'test_12345',
            'password2': 'test_12345',
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.client.logout()
        self.assertTrue(self.client.login(username=user.email, password='12345'))

    def test_change_password(self):
        user = ExtUser.objects.first()

        self.client.login(username=user.email, password='12345')

        url = reverse('users.profile.password.change')

        new_password = 'test_12345'
        response = self.client.post(url, {
            'password': '12345',
            'password1': new_password,
            'password2': new_password,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        self.client.logout()

        self.assertFalse(self.client.login(username=user.email, password='12345'))
        self.assertTrue(self.client.login(username=user.email, password=new_password))