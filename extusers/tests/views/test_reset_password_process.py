import json
from datetime import timedelta

from django.core import mail
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from extusers.models import ExtUser, Confirmation


class ResetPasswordProcessTest(TestCase):
    fixtures = ['users.json']

    def test_process(self):
        new_password = '54321'
        user = ExtUser.objects.first()
        url = reverse('users.password.reset')
        response = self.client.post(url, {
            'email': user.email
        })
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('users.password.reset.completed'))

        # Должно быть отправлено письмо для сброса пароля
        self.assertEqual(len(mail.outbox), 1)

        confirmation = Confirmation.objects.last()
        self.assertEqual(confirmation.type, Confirmation.TYPES.reset_password)
        self.assertEqual(confirmation.status, Confirmation.STATUSES.active)
        new_password_url = reverse('users.password.new', args=[
            confirmation.key
        ])

        # Ссылка должна присутствовать в письме
        self.assertTrue(new_password_url in mail.outbox[0].alternatives[0][0])

        # Переход по ссылке из письма
        response = self.client.get(new_password_url)
        self.assertEqual(response.status_code, 200)

        # Авторизовываться всё ещё можно
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        self.client.logout()

        new_password_save_url = reverse('users.password.new.save', args=[
            confirmation.key
        ])
        # Меняем пароль
        response = self.client.post(new_password_save_url, {
            'password1': new_password,
            'password2': new_password
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertEqual(json_response['url'], reverse('users.password.new.completed'))

        # Ссылка для сброса пароля должна быть удалена
        changed_confirmation = Confirmation.objects.get(id=confirmation.id)
        self.assertEqual(changed_confirmation.status, Confirmation.STATUSES.deleted)

        # Авторизовываться под старым паролем уже нельзя
        self.assertFalse(self.client.login(username=user.email, password='12345'))

        # Авторизация под новым паролем
        self.assertTrue(self.client.login(username=user.email, password=new_password))

    def test_outdated_key(self):
        new_password = '54321'
        user = ExtUser.objects.first()
        url = reverse('users.password.reset')
        response = self.client.post(url, {
            'email': user.email
        })
        self.assertEqual(response.status_code, 200)

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])

        # Делаем ключ просроченным
        confirmation = Confirmation.objects.last()
        confirmation.ended_at = timezone.now()-timedelta(days=200)
        confirmation.save()

        new_password_url = reverse('users.password.new', args=[
            confirmation.key
        ])

        # Переход по ссылке из письма
        response = self.client.get(new_password_url)
        self.assertContains(response, '<h2>Ошибка</h2>', status_code=200)
        self.assertContains(response, 'устарела', status_code=200)

        new_password_save_url = reverse('users.password.new.save', args=[
            confirmation.key
        ])
        # Пытаемся изменить пароль
        response = self.client.post(new_password_save_url, {
            'password1': new_password,
            'password2': new_password
        })

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertTrue('url' not in json_response)

        # Убеждаемся, что пароль не поменялся
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        self.client.logout()

    def test_incorrect_key(self):
        new_password = '54321'
        incorrect_key = 'testkey'  # Неправильный ключ
        new_password_url = reverse('users.password.new', args=[
            incorrect_key
        ])
        response = self.client.get(new_password_url)
        self.assertContains(response, '<h2>Ошибка</h2>', status_code=200)
        self.assertContains(response, 'что-то пошло не так', status_code=200)

        new_password_save_url = reverse('users.password.new.save', args=[
            incorrect_key
        ])
        # Пытаемся изменить пароль
        response = self.client.post(new_password_save_url, {
            'password1': new_password,
            'password2': new_password
        })

        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])
        self.assertTrue('url' not in json_response)

        # Убеждаемся, что пароль не поменялся
        user = ExtUser.objects.first()
        self.assertTrue(self.client.login(username=user.email, password='12345'))
        self.client.logout()