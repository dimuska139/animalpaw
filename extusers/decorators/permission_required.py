from django.http import JsonResponse
from functools import wraps

from django.utils import six
from django.utils.decorators import available_attrs


def user_passes_test(test_func):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            response = {
                'success': False,
                'message': 'Permission denied'
            }
            return JsonResponse(response)
        return _wrapped_view
    return decorator


def permission_required_json(perm):
    def check_perms(user):
        if isinstance(perm, six.string_types):
            perms = (perm, )
        else:
            perms = perm
        if user.has_perms(perms):
            return True
        return False
    return user_passes_test(check_perms)
