from django.http import JsonResponse
from functools import wraps

from django.urls import reverse
from django.utils.decorators import available_attrs
from django.shortcuts import redirect


def user_passes_test(test_func, is_json=False):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                return view_func(request, *args, **kwargs)
            if not is_json:
                return redirect(reverse('main'))
            else:
                response = {
                    'success': False,
                    'message': 'Authenticated'
                }
                return JsonResponse(response)
        return _wrapped_view
    return decorator


def only_anonymous(function=None):
    actual_decorator = user_passes_test(lambda u: not u.is_authenticated())
    if function:
        return actual_decorator(function)
    return actual_decorator


def only_anonymous_json(function=None):
    actual_decorator = user_passes_test(lambda u: not u.is_authenticated(), is_json=True)
    if function:
        return actual_decorator(function)
    return actual_decorator
