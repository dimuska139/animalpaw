from django.conf.urls import url
from extusers import views

urlpatterns = [
    url(r'^profile$', views.profile, name='profile'),
    url(r'^profile/update$', views.profile_update, name='users.profile.update'),
    url(r'^profile/password/change$', views.profile_changepassword, name='users.profile.password.change'),
    url(r'^password/restore$', views.password_restore, name='users.password.restore'),  # Страница сброса пароля
    url(r'^password/reset$', views.reset_password, name='users.password.reset'),  # Сброс пароля
    # Страница с уведомлением об успешном сбросе пароля
    url(r'^password/reset/completed$', views.reset_password_completed, name='users.password.reset.completed'),
    url(r'^password/new/completed$', views.new_password_completed, name='users.password.new.completed'),
    # Страница с полем для ввода нового пароля
    url(r'^password/new/(?P<key>\w+)$', views.new_password, name='users.password.new'),
    url(r'^password/new/(?P<key>\w+)/save$', views.new_password_save, name='users.password.new.save'),
    url(r'^profile/avatar/upload$', views.avatar_upload, name='users.profile.avatar.upload'),
    url(r'^check_email$', views.check_email, name='check_email'),  #
    url(r'^registration$', views.registration, name='users.registration'),  # Регистрация
    # Уведомление об отправке email с ссылкой для подтверждения регистрации
    url(r'^registration/completion$', views.registration_completion, name='users.registration.completion'),
    # Страница подтверждения регистрации по ссылке из email
    url(r'^registration/confirmation/(?P<key>\w+)$', views.registration_confirmation, name='users.registration.confirmation'),
    # Подтверждение регистрации
    url(r'^registration/confirm/(?P<key>\w+)$', views.registration_confirm, name='users.registration.confirm'),
    # Уведомление об успешной регистрации
    url(r'^registration/completed$', views.registration_completed, name='users.registration.completed'),
    url(r'^login$', views.login, name='users.login'),  #
    url(r'^logout$', views.logout, name='logout'),  #


]
