#!/usr/bin/env bash
export DJANGO_SETTINGS_MODULE=animalpaw.settings_test_local
echo "Running tests"
rm -rf ./media_test
mkdir ./media_test
cp -r ./media_default/* ./media_test
python manage.py test
rm -rf ./media_test/*