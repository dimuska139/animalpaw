# Порода
from django.db import models
from model_utils import Choices


class Breed(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    kind = models.ForeignKey('animals.Kind', on_delete=models.SET_NULL, null=True)
    logo = models.ForeignKey('utils.Image', on_delete=models.SET_NULL, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
