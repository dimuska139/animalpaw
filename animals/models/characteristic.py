# Характеристики породы
from django.db import models
from model_utils import Choices


class Characteristic(models.Model):
    name = models.CharField(max_length=50)
    kind = models.ForeignKey('animals.Kind', on_delete=models.SET_NULL, null=True)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name
