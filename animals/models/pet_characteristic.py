# Характеристики породы (связка)
from django.db import models
from model_utils import Choices


class PetCharacteristic(models.Model):
    breed = models.ForeignKey('animals.Breed', null=True, on_delete=models.SET_NULL)
    characteristic = models.ForeignKey('animals.Characteristic', null=True, on_delete=models.SET_NULL, )
    value = models.IntegerField(choices=((i+1, i+1) for i in range(10)))
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.breed.name + ' - ' + self.characteristic.name

    class Meta:
        index_together = [['breed', 'characteristic'], ]
