# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-04-23 18:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0006_breed_logo'),
    ]

    operations = [
        migrations.CreateModel(
            name='BreedPhoto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('original', models.ImageField(upload_to='')),
                ('preview', models.ImageField(blank=True, null=True, upload_to='')),
                ('version', models.IntegerField(blank=True, default=0)),
                ('base64', models.TextField(null=True)),
                ('md5', models.CharField(max_length=32, null=True)),
                ('x_1', models.IntegerField(blank=True, null=True)),
                ('x_2', models.IntegerField(blank=True, null=True)),
                ('y_1', models.IntegerField(blank=True, null=True)),
                ('y_2', models.IntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('status', models.IntegerField(choices=[(0, 'Удалено'), (1, 'Активно')], default=1)),
            ],
        ),
        migrations.AlterField(
            model_name='breed',
            name='logo',
            field=models.ForeignKey(null=True, blank=True, on_delete=django.db.models.deletion.CASCADE, to='animals.BreedPhoto'),
        ),
    ]
