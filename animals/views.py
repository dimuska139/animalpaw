from PIL import Image as PILimage
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader
from django.conf import settings
from django.urls import reverse
from jsonview.decorators import json_view

from animals.forms import BreedForm
from animals.models import Characteristic
from animals.models import Kind
from animals.models import PetCharacteristic
from extusers.decorators import login_required_json
from extusers.decorators.permission_required import permission_required_json
from utils.forms import get_errors
from utils.models import Image
from .models import Breed


def get_tab_hash(kind):
    kind = int(kind)
    hash = ''
    if kind == 1:
        hash = '#dogs'
    if kind == 2:
        hash = '#cats'
    if kind == 3:
        hash = '#birds'
    return hash


def whatdog(request):
    characteristics = Characteristic.objects.filter(status=Characteristic.STATUSES.active).all()
    template = loader.get_template('animals/pages/whatdog.html')

    context = {
        'characteristics': characteristics
    }
    return HttpResponse(template.render(context, request))


@json_view
def whatdog_analyze(request):
    pet_characteristic = PetCharacteristic.objects.filter(status=PetCharacteristic.STATUSES.active,
                                                          breed__kind_id=1).select_related('breed')
    evaluations = []
    for ch in pet_characteristic:
        evaluations.append({
            'breed_id': ch.breed_id,
            'breed_name': ch.breed.name,
            'characteristic_id': ch.characteristic_id,
            'value': int(ch.value)
        })

    breeds = Breed.objects.filter(status=Breed.STATUSES.active, kind_id=1).all()
    breed_ids = []
    for breed in breeds:
        accept = True
        for ev in evaluations:
            if ev['breed_id'] == breed.id:
                ch_value = int(request.POST.get('characteristic_' + str(ev['characteristic_id']), 0))
                if ch_value == 1 and ev['value'] in range(3, 11):
                    accept = False
                if (ch_value == 2 and ev['value'] in range(1, 4)) or (ch_value == 2 and ev['value'] in range(7, 11)):
                    accept = False
                if ch_value == 3 and ev['value'] in range(1, 8):
                    accept = False
        if accept:
            breed_ids.append(breed.id)

    breeds = Breed.objects.filter(id__in=breed_ids)[0:10]

    context = {
        'breeds': breeds
    }

    template = loader.get_template('animals/whatdog_results.html')
    return {
        'success': True,
        'html': template.render(context, request)
    }


@login_required
@permission_required('animals.add_breed', raise_exception=True)
def breeds(request):
    dog_breeds = Breed.objects.filter(kind_id=1, status=Breed.STATUSES.active).select_related('logo')
    cat_breeds = Breed.objects.filter(kind_id=2, status=Breed.STATUSES.active).select_related('logo')
    bird_breeds = Breed.objects.filter(kind_id=3, status=Breed.STATUSES.active).select_related('logo')
    context = {
        'dog_breeds': dog_breeds,
        'cat_breeds': cat_breeds,
        'bird_breeds': bird_breeds
    }

    template = loader.get_template('animals/pages/breeds.html')
    return HttpResponse(template.render(context, request))


@login_required_json
@permission_required_json('animals.add_breed')
@json_view
def breed_logo_upload(request):
    if 'image' not in request.FILES:
        response = {
            'success': False,
            'message': 'Отсутствует файл'
        }
        return response
    if request.FILES['image'].size > settings.MAX_ARTICLE_LOGO_SIZE:
        response = {
            'success': False,
            'message': 'Максимальный размер загружаемого файла - ' + str(
                settings.MAX_ARTICLE_LOGO_SIZE / 1024) + ' кБ',
        }
        return response
    try:
        PILimage.open(request.FILES['image'])
        photo = Image(
            original=request.FILES['image'],
            user=request.user
        )
        photo.save()

        response = {
            'success': True,
            'message': 'OK',
            'id': photo.pk,
            'url': photo.original.url,
            'width': photo.original.width,
            'height': photo.original.height,
        }
        return response
    except IOError as e:
        response = {
            'success': False,
            'message': 'Недопустимый формат файла'
        }
        return response


@login_required
@permission_required('animals.add_breed', raise_exception=True)
def breed_creation_page(request):
    form = BreedForm(initial={
        'name': '',
        'kind': '',
    })
    template = loader.get_template('animals/pages/breed_creation.html')

    kinds_list = Kind.objects.filter(status=Kind.STATUSES.active)
    kinds = []
    for kind in kinds_list:
        kinds.append({
            'id': kind.id,
            'name': kind.name
        })

    context = {
        'breed_form': form,
        'kinds': kinds
    }
    return HttpResponse(template.render(context, request))


@login_required_json
@permission_required_json('animals.add_breed')
@json_view
def breed_create(request):
    form = BreedForm(request.POST)
    if not form.is_valid():
        return {
            'success': False,
            'errors': get_errors(form)
        }

    breed = form.save()

    if 'logo' in request.POST and len(request.POST['logo']) > 0:
        breed.logo.make_logo(x1=request.POST['logo_x1'],
                             y1=request.POST['logo_y1'],
                             x2=request.POST['logo_x2'],
                             y2=request.POST['logo_y2']
                             )

    template = loader.get_template('animals/messages/breed_creation_success.html')

    context = {
        'breed': breed,
    }

    messages.info(request, template.render(context, request))

    hash = get_tab_hash(request.POST['kind'])

    return {
        'success': True,
        'url': reverse('animals.breeds.index') + hash
    }


@login_required
@permission_required('animals.change_breed', raise_exception=True)
def breed_editing_page(request, id):
    breed = get_object_or_404(Breed, id=id, status=Breed.STATUSES.active)

    try:
        logo = Image.objects.get(pk=breed.logo_id)
    except ObjectDoesNotExist:
        logo = None
    breed_form = BreedForm(instance=breed)

    kinds_list = Kind.objects.filter(status=Kind.STATUSES.active)
    kinds = []
    for kind in kinds_list:
        kinds.append({
            'id': kind.id,
            'name': kind.name
        })

    template = loader.get_template('animals/pages/breed_editing.html')
    context = {
        'breed_form': breed_form,
        'breed_id': breed.id,
        'logo': logo,
        'kinds': kinds
    }
    return HttpResponse(template.render(context, request))


@login_required_json
@permission_required_json('animals.change_breed')
@json_view
def breed_edit(request, id):
    breed = Breed.objects.filter(id=id, status=Breed.STATUSES.active).first()

    if not isinstance(breed, Breed):
        return {
            'success': False,
            'errors': [
                {
                    'key': 'all',
                    'description': 'Порода не найдена'
                }
            ]
        }

    breed_form = BreedForm(request.POST, instance=breed)
    if not breed_form.is_valid():
        return {
            'success': False,
            'errors': get_errors(breed_form)
        }

    breed = breed_form.save()

    if 'logo' in request.POST and len(request.POST['logo']) > 0:
        breed.logo.make_logo(x1=request.POST['logo_x1'],
                             y1=request.POST['logo_y1'],
                             x2=request.POST['logo_x2'],
                             y2=request.POST['logo_y2']
                             )

    template = loader.get_template('animals/messages/breed_editing_success.html')

    context = {
        'breed': breed
    }
    messages.info(request, template.render(context, request))

    hash = get_tab_hash(request.POST['kind'])

    return {
        'success': True,
        'url': reverse('animals.breeds.index') + hash
    }
