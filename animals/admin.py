from django.contrib import admin

# Register your models here.
from animals.models import Breed
from animals.models import Characteristic
from animals.models import Kind
from animals.models import PetCharacteristic

admin.site.register((Breed, Characteristic, Kind, PetCharacteristic))
