from django.conf.urls import url
from animals import views


urlpatterns = [
    url(r'^whatdog$', views.whatdog, name='animals.whatdog'),
    url(r'^whatdog/analyze$', views.whatdog_analyze, name='animals.whatdog.analyze'),
    url(r'^breeds$', views.breeds, name='animals.breeds.index'),
    url(r'^breeds/creation$', views.breed_creation_page, name='animals.breeds.creation'),
    url(r'^breeds/creation/save$', views.breed_create, name='animals.breeds.creation.save'),
    url(r'^breeds/logo/upload$', views.breed_logo_upload, name='animals.breeds.logo.upload'),
    url(r'^breeds/(?P<id>\d+)/edit$', views.breed_editing_page, name='animals.breeds.editing'),
    url(r'^breeds/(?P<id>\d+)/save$', views.breed_edit, name='animals.breeds.editing.save'),
]
