__author__ = 'dimuska139'

from django.forms import ModelForm
from animals.models import Breed


class BreedForm(ModelForm):
    class Meta:
        model = Breed
        exclude = (
            'logo_url',
            'status'
        )

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = 'Название породы'
        self.fields['kind'].label = 'Вид животного'
        self.fields['logo'].label = 'Логотип'
