from django.test import TestCase
from django.urls import reverse


class UrlsTest(TestCase):
    def test_urls(self):
        self.assertEqual(reverse('animals.whatdog'), '/animals/whatdog')
        self.assertEqual(reverse('animals.whatdog.analyze'), '/animals/whatdog/analyze')
        self.assertEqual(reverse('animals.breeds.index'), '/animals/breeds')
        self.assertEqual(reverse('animals.breeds.logo.upload'), '/animals/breeds/logo/upload')
        self.assertEqual(reverse('animals.breeds.editing', args=[1]), '/animals/breeds/1/edit')
        self.assertEqual(reverse('animals.breeds.editing.save', args=[1]), '/animals/breeds/1/save')
