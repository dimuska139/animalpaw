# -*- coding: utf-8 -*-
import json

from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse

from animals.models import Breed
from extusers.models import ExtUser
from utils.models import Image


class EditingTest(TestCase):
    fixtures = ['users.json', 'initial.json']

    def test_permissions_page(self):
        user = ExtUser.objects.first()
        breed = Breed.objects.filter(status=Breed.STATUSES.active).first()

        self.assertIsInstance(breed, Breed)
        url = reverse('animals.breeds.editing', args=[
            breed.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertIsInstance(Permission.objects.get(codename='add_breed'), Permission)
        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('animals.add_breed'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='change_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('animals.change_breed'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        user.user_permissions.remove(Permission.objects.get(codename='add_breed'))
        user.user_permissions.remove(Permission.objects.get(codename='change_breed'))
        self.client.logout()

    def test_save(self):
        user = ExtUser.objects.get(id=1)

        breed = Breed.objects.filter(status=Breed.STATUSES.active).first()

        self.assertIsInstance(breed, Breed)
        url = reverse('animals.breeds.editing.save', args=[
            breed.id
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.client.login(username=user.email, password='12345')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        self.assertIsInstance(Permission.objects.get(codename='add_breed'), Permission)
        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('animals.add_breed'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

    def test_edit(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user.user_permissions.add(Permission.objects.get(codename='change_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)

        breed = Breed.objects.filter(status=Breed.STATUSES.active).first()
        self.assertIsInstance(breed, Breed)

        amount_before = Breed.objects.count()

        url = reverse('animals.breeds.editing', args=[
            breed.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        save_url = reverse('animals.breeds.editing.save', args=[
            breed.id
        ])
        new_name = 'Новое название'
        response = self.client.post(save_url, {
            'name': new_name,
            'kind': breed.kind_id,
            'logo': Image.objects.first().id,
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 10,
            'logo_y2': 10,
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))

        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertTrue(reverse('animals.breeds.index') in json_response['url'])

        self.assertEqual(amount_before, Breed.objects.count())

        # В списке пород должны появиться изменения
        breeds_page_url = reverse('animals.breeds.index')
        response = self.client.get(breeds_page_url)
        self.assertContains(response, new_name, status_code=200)

        user.user_permissions.remove(Permission.objects.get(codename='add_breed'))
        user.user_permissions.remove(Permission.objects.get(codename='change_breed'))
        self.client.logout()

    def test_edit_without_logo(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user.user_permissions.add(Permission.objects.get(codename='change_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)

        breed = Breed.objects.filter(status=Breed.STATUSES.active).first()
        self.assertIsInstance(breed, Breed)

        amount_before = Breed.objects.count()

        url = reverse('animals.breeds.editing', args=[
            breed.id
        ])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        save_url = reverse('animals.breeds.editing.save', args=[
            breed.id
        ])
        new_name = 'Новое название'
        response = self.client.post(save_url, {
            'name': new_name,
            'kind': breed.kind_id
        })
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))

        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertTrue(reverse('animals.breeds.index') in json_response['url'])

        self.assertEqual(amount_before, Breed.objects.count())

        # В списке пород должны появиться изменения
        breeds_page_url = reverse('animals.breeds.index')
        response = self.client.get(breeds_page_url)
        self.assertContains(response, new_name, status_code=200)

        user.user_permissions.remove(Permission.objects.get(codename='add_breed'))
        user.user_permissions.remove(Permission.objects.get(codename='change_breed'))
        self.client.logout()
