# -*- coding: utf-8 -*-
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse
from extusers.models import ExtUser


class BreedsTest(TestCase):
    fixtures = ['users.json', 'initial.json']

    def test_page(self):
        url = reverse('animals.breeds.index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('animals.add_breed'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

