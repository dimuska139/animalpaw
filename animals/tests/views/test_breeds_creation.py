# -*- coding: utf-8 -*-
import json

from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse

from animals.models import Breed
from animals.models import Kind
from extusers.models import ExtUser
from utils.models import Image


class CreationTest(TestCase):
    fixtures = ['users.json', 'initial.json']

    def test_page(self):
        url = reverse('animals.breeds.creation')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('animals.add_breed'))
        response = self.client.get(url)
        self.assertContains(response, reverse('animals.breeds.creation.save'), status_code=200)

    def test_action(self):
        url = reverse('animals.breeds.creation.save')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertFalse(json_response['success'])

        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)
        self.assertTrue(user.has_perm('animals.add_breed'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_creation(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)

        url = reverse('animals.breeds.creation.save')
        amount_before = Breed.objects.count()

        new_name = 'TEST_BREED'
        response = self.client.post(url, {
            'name': new_name,
            'kind': Kind.objects.first().pk,
            'logo': Image.objects.first().pk,
            'logo_x1': 0,
            'logo_y1': 0,
            'logo_x2': 100,
            'logo_y2': 100,
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertTrue(reverse('animals.breeds.index') in json_response['url'])

        self.assertNotEqual(amount_before, Breed.objects.count())

        new_breed = Breed.objects.order_by('-id').first()

        breeds_page_url = reverse('animals.breeds.index')
        response = self.client.get(breeds_page_url)
        self.assertContains(response, new_name, status_code=200)

    def test_creation_without_logo(self):
        user = ExtUser.objects.first()
        self.client.login(username=user.email, password='12345')
        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        user = get_object_or_404(ExtUser, pk=user.id)

        url = reverse('animals.breeds.creation.save')
        amount_before = Breed.objects.count()

        new_name = 'TEST_BREED'
        response = self.client.post(url, {
            'name': new_name,
            'kind': Kind.objects.first().pk
        })

        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content.decode('utf-8'))
        self.assertTrue(json_response['success'])
        self.assertTrue('url' in json_response)
        self.assertTrue(reverse('animals.breeds.index') in json_response['url'])

        self.assertNotEqual(amount_before, Breed.objects.count())

        breeds_page_url = reverse('animals.breeds.index')
        response = self.client.get(breeds_page_url)
        self.assertContains(response, new_name, status_code=200)
