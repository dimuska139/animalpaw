# coding: utf-8
from django.contrib.auth.models import Permission
from django.shortcuts import get_object_or_404
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class BreedsPageTest(TestCase):
    fixtures = ['initial.json', 'users.json',]

    def setUp(self):
        self.url = reverse('animals.breeds.index')

    def test_anon(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_auth(self):
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_superuser(self):
        user = ExtUser.objects.get(pk=1)
        user.is_superuser = True
        user.save()

        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)

        self.assertContains(response, reverse('animals.breeds.creation'), status_code=200)

    def test_with_permissions(self):
        user = ExtUser.objects.get(pk=1)
        user.user_permissions.add(Permission.objects.get(codename='add_breed'))
        self.client.login(username=user.email, password='12345')

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, reverse('animals.breeds.creation'), status_code=200)




