# coding: utf-8
from django.test import TestCase
from django.urls import reverse

from extusers.models import ExtUser


class BreedsPageTest(TestCase):
    fixtures = ['initial.json', 'users.json',]

    def setUp(self):
        self.url = reverse('animals.whatdog')

    def test_anon(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_auth(self):
        user = ExtUser.objects.get(pk=1)
        self.client.login(username=user.email, password='12345')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)




