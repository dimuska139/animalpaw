# coding: utf-8
from django.test import TestCase
from animals.forms import BreedForm
from utils.models import Image


class BreedTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_valid_form(self):
        data = {
            'name': 'Порода собаки',
            'kind': 1,
            'logo': Image.objects.first().pk
        }
        form = BreedForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        data = {
            'name': '',
            'kind': 1,
            'logo': Image.objects.first().pk
        }
        form = BreedForm(data=data)
        self.assertFalse(form.is_valid())

        data = {
            'name': 'Порода собаки',
            'kind': 100,  # Несуществующий id вида животного
            'logo': Image.objects.first().pk
        }
        form = BreedForm(data=data)
        self.assertFalse(form.is_valid())
