# -*- coding: utf-8 -*-

from django.test import TestCase

from animals.models import Breed
from animals.models import Characteristic, PetCharacteristic


class PetCharacteristicTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_creation(self):
        pet_characteristic = PetCharacteristic.objects.create(breed=Breed.objects.first(),
                                                              characteristic=Characteristic.objects.first(),
                                                              value=5)
        self.assertTrue(isinstance(pet_characteristic, PetCharacteristic))
        self.assertEqual(pet_characteristic.__str__(),
                         pet_characteristic.breed.name + ' - ' + pet_characteristic.characteristic.name)
        self.assertEqual(pet_characteristic.status, PetCharacteristic.STATUSES.active)
