# -*- coding: utf-8 -*-

from django.test import TestCase
from animals.models import Breed, Kind


class BreedTest(TestCase):
    fixtures = ['initial.json']

    def test_creation(self):
        breed = Breed.objects.create(name='Сиамский кот', kind=Kind.objects.get(pk=1))
        self.assertTrue(isinstance(breed, Breed))
        self.assertEqual(breed.__str__(), breed.name)
        self.assertEqual(breed.status, Breed.STATUSES.active)
