# -*- coding: utf-8 -*-

from django.test import TestCase

from animals.models import Characteristic, Kind


class CharacteristicTest(TestCase):
    fixtures = ['initial.json']

    def test_creation(self):
        characteristic = Characteristic.objects.create(name='Выпадение шерсти',
                                                       kind=Kind.objects.first(),
                                                       description='Сообщает Вам сколько шерсти порода оставляет. Чем выше рейтинг, тем сильнее порода линяет.')
        self.assertTrue(isinstance(characteristic, Characteristic))
        self.assertEqual(characteristic.__str__(), characteristic.name)
        self.assertEqual(characteristic.status, Characteristic.STATUSES.active)
