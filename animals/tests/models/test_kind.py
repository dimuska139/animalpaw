# -*- coding: utf-8 -*-

from django.test import TestCase
from animals.models import Kind


class KindTest(TestCase):
    def test_creation(self):
        kind = Kind.objects.create(name='Лошадь', url_alias='reg', img_url='images/img.jpg')
        self.assertTrue(isinstance(kind, Kind))
        self.assertEqual(kind.__str__(), kind.name)
        self.assertEqual(kind.status, Kind.STATUSES.active)
