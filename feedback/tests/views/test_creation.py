# -*- coding: utf-8 -*-

from django.test import TestCase
from django.urls import reverse
from feedback.models import Feedback


class CreationTest(TestCase):
    def test_creation(self):
        url = reverse('feedback.create')
        amount_before = Feedback.objects.count()
        response = self.client.post(url, {
            'name': 'Test',
            'email': 'test@test.test',
            'text': 'Test feedback',
            'hp': ''
        })
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(amount_before, Feedback.objects.count())

    def test_honeypot(self):
        url = reverse('feedback.create')
        amount_before = Feedback.objects.count()
        response = self.client.post(url, {
            'name': 'Test',
            'email': 'test@test.test',
            'text': 'Test feedback',
            'hp': 'test'
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(amount_before, Feedback.objects.count())
