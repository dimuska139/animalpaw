# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse


class FeedbackViewsTest(TestCase):
    fixtures = ['initial.json', 'users.json']

    def test_urls(self):
        self.assertEqual(reverse('feedback.create'), '/feedback/create')
