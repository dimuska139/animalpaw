# -*- coding: utf-8 -*-
from django.test import TestCase
from feedback.models import Feedback


class FeedbackTest(TestCase):
    fixtures = ['initial.json', ]

    def test_creation(self):
        feedback = Feedback.objects.create(text='Test feedback', name='Dima', email='test@test.net')
        self.assertTrue(isinstance(feedback, Feedback))
        self.assertEqual(feedback.__str__(), feedback.name)
        self.assertEqual(feedback.status, Feedback.STATUSES.active)

