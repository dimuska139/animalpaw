# -*- coding: utf-8 -*-
from django.test import TestCase
from feedback.forms import FeedbackForm


class FeedbackTest(TestCase):
    def test_valid_form(self):
        data = {
            'name': 'Alex',
            'email': 'user@test.test',
            'text': 'Отзыв',
        }
        form = FeedbackForm(data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        # Empty name
        data = {
            'name': '',
            'email': 'user@test.test',
            'text': 'Отзыв',
        }
        form = FeedbackForm(data=data)
        self.assertFalse(form.is_valid())

        # Incorrect email
        data = {
            'name': 'Alex',
            'email': 'test',
            'text': 'Отзыв',
        }
        form = FeedbackForm(data=data)
        self.assertFalse(form.is_valid())
