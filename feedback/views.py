# coding: utf-8

from django.views.decorators.http import require_POST
from honeypot.decorators import check_honeypot
from jsonview.decorators import json_view

from feedback.forms import FeedbackForm
from utils.forms import get_errors


@json_view
@require_POST
@check_honeypot(field_name='hp')
def create(request):
    feedback_form = FeedbackForm(request.POST)
    errors = get_errors(feedback_form)
    if len(errors) > 0:
        response = {
            'success': False,
            'errors': errors
        }
        return response

    feedback_form.save()

    return {
        'success': True,
        'message': 'Ваше сообщение отправлено',
    }
