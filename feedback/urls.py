from django.conf.urls import url
from feedback import views

urlpatterns = [
    url(r'^create$', views.create, name='feedback.create')
]
