from django.db import models
from model_utils import Choices


class Feedback(models.Model):
    text = models.TextField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    name = models.CharField(max_length=100)
    email = models.EmailField()

    STATUSES = Choices(
        (0, 'deleted', 'Удалено'),
        (1, 'active', 'Активно'))
    status = models.IntegerField(choices=STATUSES, default=STATUSES.active)

    def __str__(self):
        return self.name
