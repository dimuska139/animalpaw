# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-03-23 14:55
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0006_auto_20170307_2039'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='feedback',
            name='image',
        ),
        migrations.DeleteModel(
            name='Image',
        ),
    ]
