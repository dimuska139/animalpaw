# -*- coding: utf-8 -*-

__author__ = 'dimuska139'


from django.forms import ModelForm

from .models import Feedback


class FeedbackForm(ModelForm):
    class Meta:
        model = Feedback
        exclude = (
            'user',
            'status',
            'create_time',
        )

    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['text'].label = 'Сообщение'
        self.fields['name'].label = 'Ваше имя'



